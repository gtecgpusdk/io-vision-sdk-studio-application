/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Controllers.Subscribers {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import EditorPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.EditorPanel;
    import HttpManager = Com.Wui.Framework.Services.HttpProcessor.HttpManager;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BasePageController = Com.Wui.Framework.Services.HttpProcessor.Resolvers.BasePageController;
    import EditorPage = Io.VisionSDK.Studio.Gui.BaseInterface.Pages.Designer.EditorPage;
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IApplicationEnvironment = Io.VisionSDK.Studio.Services.Interfaces.IApplicationEnvironment;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import VisualGraphPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.VisualGraphPanel;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ConsolePanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ConsolePanel;
    import IContextMenuItem = Io.VisionSDK.Studio.Gui.Interfaces.IContextMenuItem;

    export class ContextMenuSubscriber extends BaseObject {
        private owner : BasePageController;
        private container : IGuiElement;
        private copyItem : IContextMenuItem;
        private cutItem : IContextMenuItem;
        private pasteItem : IContextMenuItem;
        private deleteItem : IContextMenuItem;
        private removeAllItem : IContextMenuItem;
        private cleanCmakeItem : IContextMenuItem;
        private selectAllItem : IContextMenuItem;
        private clearConsoleItem : IContextMenuItem;

        constructor($owner : BasePageController) {
            super();
            this.owner = $owner;
        }

        public Subscribe() {
            const page : EditorPage = <EditorPage>this.owner.getModel().getInstance();
            const editor : EditorPanel = page.editorPanel;
            const dao : MainPageDAO = <MainPageDAO>(<any>this.owner).getDao();

            let isMouseDown : boolean = false;
            WindowManager.getEvents().setOnMove(() : void => {
                isMouseDown = false;
            });

            WindowManager.getEvents().setOnMouseDown(($eventArgs : MouseEventArgs) : void => {
                if ($eventArgs.NativeEventArgs().which === 3 && !(<EditorPage>editor.Parent()).wizardDialog.Visible()) {
                    isMouseDown = true;
                }
            });

            WindowManager.getEvents().setOnMouseUp(($eventArgs : MouseEventArgs) : void => {
                if (isMouseDown) {
                    this.OnShowHandler($eventArgs);
                    isMouseDown = false;
                }
            });

            WindowManager.getEvents().setOnRightClick(($eventArgs : MouseEventArgs) : void => {
                $eventArgs.PreventDefault();
                $eventArgs.StopAllPropagation();
            });

            this.selectAllItem = {
                callback: () : void => {
                    editor.visualGraphPanel.SelectAll();
                },
                text    : "Select All"
            };
            this.copyItem = {
                callback: () : void => {
                    editor.visualGraphPanel.Copy();
                },
                text    : "Copy"
            };
            this.cutItem = {
                callback: () : void => {
                    editor.visualGraphPanel.Cut();
                },
                text    : "Cut"
            };
            this.pasteItem = {
                callback: () : void => {
                    editor.visualGraphPanel.Paste();
                },
                text    : "Paste"
            };

            this.deleteItem = {
                callback: () : void => {
                    editor.visualGraphPanel.RemoveSelected();
                },
                text    : "Delete"
            };

            this.removeAllItem = {
                callback: () : void => {
                    editor.visualGraphPanel.RemoveAll();
                    this.getEnvironment().notifyInfo("Map has been cleaned.");
                },
                text    : "Remove all nodes"
            };

            this.cleanCmakeItem = {
                callback: () : void => {
                    this.getEnvironment().projectManager.TaskManager().Clean(($status : boolean) : void => {
                        if ($status) {
                            this.getEnvironment().notifyInfo("CMake cache has been cleaned successfully.");
                        } else {
                            this.getEnvironment().notifyError("CMake cache clean has failed.");
                        }
                    });
                },
                text    : "Clean CMake cache"
            };

            this.clearConsoleItem = {
                callback: () : void => {
                    page.ClearConsole();
                },
                text    : "Clean console"
            };
        }

        public OnShowHandler($eventArgs : MouseEventArgs) : void {
            const page : EditorPage = <EditorPage>this.owner.getModel().getInstance();
            const visualPanel : VisualGraphPanel = page.editorPanel.visualGraphPanel;
            const items : ArrayList<IContextMenuItem> = new ArrayList<IContextMenuItem>();

            if (!page.wizardDialog.Visible()) {
                const console : ConsolePanel = <ConsolePanel>page.consoleDialog.PanelViewer().getInstance();
                if (console.IsActive()) {
                    items.Add(this.clearConsoleItem);
                } else if (visualPanel.gridManager.IsViewHovered()) {
                    if (visualPanel.getSelectedNodes().Length() > 0) {
                        items.Add(this.copyItem);
                        items.Add(this.cutItem);
                        items.Add(this.deleteItem);
                    } else if (!ObjectValidator.IsEmptyOrNull(visualPanel.connectionManager.getSelectedConnections())) {
                        items.Add(this.deleteItem);
                    }
                    if (visualPanel.clipboardNodes.Length() > 0) {
                        items.Add(this.pasteItem);
                    }
                    if (visualPanel.KernelNodes().Length() > 0) {
                        items.Add(this.selectAllItem);
                        items.Add(this.removeAllItem);
                    }
                }
                const httpManager : HttpManager = Loader.getInstance().getHttpManager();
                if (!httpManager.getRequest().IsWuiJre()) {
                    items.Add({
                        callback: () : void => {
                            this.owner.InstanceOwner().ClearCache();
                            httpManager.Refresh();
                        },
                        text    : "Clean application cache"
                    });
                }
                items.Add(this.cleanCmakeItem);

                if (items.Length() > 0) {
                    page.ShowContextMenu(items, new ElementOffset($eventArgs.NativeEventArgs().clientY,
                        $eventArgs.NativeEventArgs().clientX));
                }
            }
        }

        public getInnerHtml() : IGuiElement {
            return this.container;
        }

        private getEnvironment() : IApplicationEnvironment {
            return (<any>this.owner).appEnvironment;
        }
    }
}
