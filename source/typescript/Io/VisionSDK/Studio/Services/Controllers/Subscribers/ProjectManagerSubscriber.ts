/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Controllers.Subscribers {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import VisualGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VisualGraph;
    import ProjectEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.ProjectEventArgs;
    import APIType = Io.VisionSDK.Studio.Services.Enums.APIType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import VisualGraphDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.VisualGraphDAO;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import BuildSettingsEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.BuildSettingsEventArgs;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import IApplicationEnvironment = Io.VisionSDK.Studio.Services.Interfaces.IApplicationEnvironment;
    import ToolbarPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.ToolbarPanel;
    import EditorPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.EditorPanel;
    import EditorPage = Io.VisionSDK.Studio.Gui.BaseInterface.Pages.Designer.EditorPage;
    import VXNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXNode;
    import BasePageController = Com.Wui.Framework.Services.HttpProcessor.Resolvers.BasePageController;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import IMainPageLocalization = Io.VisionSDK.Studio.Services.Interfaces.DAO.IMainPageLocalization;
    import IMainPageNotifications = Io.VisionSDK.Studio.Services.Interfaces.DAO.IMainPageNotifications;
    import ExportPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ExportPanel;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import SettingsPanelEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.SettingsPanelEventArgs;
    import IProjectSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IProjectSettingsValue;
    import IExampleSettings = Io.VisionSDK.Studio.Gui.Interfaces.IExampleSettings;
    import IStatus = Io.VisionSDK.Studio.Services.Interfaces.IStatus;
    import StatusType = Io.VisionSDK.Studio.Services.Enums.StatusType;
    import TaskManager = Io.VisionSDK.Studio.Services.Utils.TaskManager;
    import INodeRestrictions = Io.VisionSDK.Studio.Services.Interfaces.INodeRestrictions;
    import IConnectionValidatorStatus = Io.VisionSDK.UserControls.Interfaces.IConnectionValidatorStatus;
    import InteractiveMessage = Io.VisionSDK.UserControls.Utils.InteractiveMessage;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import ISettings = Io.VisionSDK.Studio.Gui.Interfaces.ISettings;

    export class ProjectManagerSubscriber extends BaseObject {
        private owner : BasePageController;
        private env : IApplicationEnvironment;
        private loopStarted : boolean;

        constructor($owner : BasePageController, $environment : IApplicationEnvironment) {
            super();
            this.owner = $owner;
            this.env = $environment;
            this.loopStarted = false;
        }

        public Subscribe() {
            const page : EditorPage = <EditorPage>this.owner.getModel().getInstance();
            const toolbar : ToolbarPanel = page.editorPanel.toolbar;
            const panel : EditorPanel = page.editorPanel;
            const dao : MainPageDAO = <MainPageDAO>(<any>this.owner).getDao();
            const configuration : IMainPageLocalization = <IMainPageLocalization>this.owner.getPageConfiguration();
            const notifications : IMainPageNotifications = configuration.notifications;

            const onSaveRequest : any = ($saveAs : boolean, $onSaveComplete? : ($success : boolean) => void) : void => {
                $onSaveComplete = ObjectValidator.IsEmptyOrNull($onSaveComplete) ?
                    () : void => {
                        // default handler
                    }
                    : $onSaveComplete;
                const graph : VisualGraph = dao.getVisualGraph();
                if ($saveAs) {
                    this.env.showDirectoryBrowser(($status : boolean, $path : string) : void => {
                        if ($status) {
                            const descriptor : IProjectSettingsValue = {
                                description: graph.Description(),
                                name       : graph.Name(),
                                path       : StringUtils.Replace($path, "\\", "/"),
                                vxVersion  : dao.getTemplates().version
                            };
                            this.env.syncBackend();
                            this.env.projectManager.UpdateProject(descriptor,
                                ($status : IStatus) : void => {
                                    if (ObjectValidator.IsSet($status.success)) {
                                        if ($status.success) {
                                            this.env.projectManager.InitTaskManager(($status : IStatus) : void => {
                                                if (ObjectValidator.IsSet($status.success)) {
                                                    if ($status.success) {
                                                        this.env.syncFrontend();
                                                    }
                                                    $onSaveComplete($status.success);
                                                }
                                            });
                                        } else {
                                            $onSaveComplete($status.success);
                                        }
                                    }
                                    this.env.printStatus($status, "save");
                                });
                        } else {
                            $onSaveComplete($status);
                        }
                    }, configuration.directoryBrowserDialog.select_target_directory, false, true);
                } else {
                    this.env.syncBackend();
                    this.env.projectManager.SaveProject(($status : IStatus) : void => {
                        if (ObjectValidator.IsSet($status.success)) {
                            $onSaveComplete($status.success);
                        }
                        this.env.printStatus($status, "save");
                    });
                }
            };

            page.getEvents().setOnProjectCreate(($eventArgs : ProjectEventArgs) : void => {
                this.env.projectManager.CreateProject($eventArgs.Descriptor(), ($status : IStatus) : void => {
                    if ($status.success) {
                        this.env.projectManager.InitTaskManager(($status : IStatus) : void => {
                            if (ObjectValidator.IsSet($status.success)) {
                                if ($status.success) {
                                    this.env.syncFrontend(true, () : void => {
                                        this.env.printStatus($status);
                                    });
                                } else {
                                    this.env.printStatus($status);
                                }
                            } else {
                                this.env.printStatus($status);
                            }
                        });
                    } else {
                        this.env.printStatus($status);
                    }

                }, false);
            });

            page.getEvents().setOnProjectLoad(($eventArgs : ProjectEventArgs) : void => {
                this.env.projectManager.LoadExample($eventArgs.Descriptor(), ($status : IStatus) : void => {
                    if ($status.success) {
                        this.env.syncFrontend(true, () : void => {
                            this.env.printStatus($status);
                        });
                    } else {
                        this.env.printStatus($status);
                    }
                });
            });

            toolbar.AddFileMenuItem(dao.getPageConfiguration().toolBarPanel.projectMenuItems.newProject,
                () : void => {
                    const path : string = (Loader.getInstance().getHttpManager().getRequest().IsWuiJre() ?
                        this.env.projectManager.getAppFolder() :
                        StringUtils.Remove(this.env.projectManager.getAppFolder(), "/target") + "/designer_cache") +
                        "/NewVisualProject";
                    page.ShowCreateProjectDialog({
                        description: "",
                        name       : "New Visual Project",
                        path,
                        vxVersion  : APIType.OVX_1_2_0
                    });
                });

            toolbar.AddFileMenuItem(dao.getPageConfiguration().toolBarPanel.projectMenuItems.load,
                () : void => {
                    this.env.showDirectoryBrowser(($status : boolean, $path : string) : void => {
                        if ($status) {
                            $path = StringUtils.Replace($path, "\\", "/");
                            this.env.projectManager.LoadProject($path, ($status : IStatus) : void => {
                                this.env.syncFrontend(true, () : void => {
                                    this.env.printStatus($status);
                                });
                            });
                        }
                    }, dao.getPageConfiguration().directoryBrowserDialog.select_visualGraph_project, true, false);
                });

            toolbar.AddFileMenuItem(dao.getPageConfiguration().toolBarPanel.save,
                () : void => {
                    onSaveRequest(this.env.projectManager.IsExampleLoaded());
                });

            toolbar.AddFileMenuItem(dao.getPageConfiguration().toolBarPanel.projectMenuItems.saveAs,
                () : void => {
                    onSaveRequest(true);
                });

            const exportPanel : ExportPanel = (<ExportPanel>page.exportDialog.PanelViewer().getInstance());
            let exportPackageSelected : boolean = false;
            exportPanel.platformType.getEvents().setOnSelect(() : void => {
                if (!exportPackageSelected) {
                    if (exportPanel.platformType.Value() === PlatformType.LINUX) {
                        exportPanel.exportPath.Value(StringUtils.Replace(exportPanel.exportPath.Value(), ".zip", ".tar.gz"));
                    } else if (exportPanel.platformType.Value() === PlatformType.IMX) {
                        exportPanel.exportPath.Value(StringUtils.Replace(exportPanel.exportPath.Value(), ".zip", ".tar.bz2"));
                    } else {
                        let exportPath : string = exportPanel.exportPath.Value();
                        exportPath = StringUtils.Replace(exportPath, ".tar.gz", ".zip");
                        exportPath = StringUtils.Replace(exportPath, ".tar.bz2", ".zip");
                        exportPanel.exportPath.Value(exportPath);
                    }
                }
            });
            page.getEvents().setOnProjectExport(() : void => {
                this.env.notifyInfo(notifications.started_exporting_current_project);
                this.env.projectManager.ExportProject(exportPanel.platformType.Value(), [], ($status : IStatus) : void => {
                    this.env.printStatus($status);
                }, exportPanel.exportPath.Value(), true);
            });

            toolbar.AddFileMenuItem(dao.getPageConfiguration().toolBarPanel.projectMenuItems.exportProject,
                () : void => {
                    exportPanel.getEvents().setOnComplete(() : void => {
                        const projectFolder : string = this.env.projectManager.getProjectFolder();
                        const parentFolder : string = StringUtils.Substring(projectFolder, 0,
                            StringUtils.IndexOf(projectFolder, "/", false));
                        exportPanel.exportPath.Value(parentFolder + "/" + dao.getVisualGraph().Name() + ".zip");
                    });
                    page.ShowExportDialog();
                }, true);

            toolbar.AddFileMenuItem(dao.getPageConfiguration().toolBarPanel.projectMenuItems.projectSettings,
                () : void => {
                    page.ShowSettings(true);
                }, true);

            let examplesInitialized : boolean = false;
            toolbar.AddFileMenuItem(dao.getPageConfiguration().toolBarPanel.projectMenuItems.examples,
                () : void => {
                    if (!examplesInitialized) {
                        this.env.notifyInfo("Loading examples ...");
                        dao.getProjects(($projects : ArrayList<VisualGraphDAO>) : void => {
                            const examples : IExampleSettings[] = [];
                            $projects.foreach(($project : VisualGraphDAO, $path : string) : void => {
                                const graph : VisualGraph = $project.getVisualGraph();
                                examples.push({
                                    description    : graph.Description(),
                                    name           : graph.Name(),
                                    path           : $path,
                                    vxVersion      : $project.getTemplates().vxAPI.version,
                                    vxVersionPrefix: "OpenVX "
                                });
                            });
                            page.setExamples(examples, {
                                description: dao.getPageConfiguration().examplesProjectPickerDialog.defaultPageDescription,
                                header     : dao.getPageConfiguration().examplesProjectPickerDialog.defaultPageHeader
                            });
                            examplesInitialized = true;
                            page.ShowExamplesDialog();
                        });
                    } else {
                        page.ShowExamplesDialog();
                    }
                });

            toolbar.saveButton.getEvents().setOnClick(() : void => {
                onSaveRequest(this.env.projectManager.IsExampleLoaded());
            });

            const selectNode : any = ($node : IKernelNode) : void => {
                if (!ObjectValidator.IsEmptyOrNull(panel.visualGraphPanel.nodeRegistry.getNodeByUniqueId($node.UniqueId()))) {
                    if ($node.ParentNode() !== panel.visualGraphPanel.RootNode()) {
                        panel.visualGraphPanel.RootNode($node.ParentNode(), () : void => {
                            panel.visualGraphPanel.DeselectAll();
                            panel.visualGraphPanel.SelectNodeById($node.UniqueId());
                        });
                    } else {
                        panel.visualGraphPanel.DeselectAll();
                        panel.visualGraphPanel.SelectNodeById($node.UniqueId());
                    }
                }
            };

            toolbar.runButton.getEvents().setOnClick(() : void => {
                const graphRestrictions : INodeRestrictions
                    = dao.getGraphRestrictedNodes(this.env.projectManager.Settings().selectedPlatform);
                const invalidInputs : IConnectionValidatorStatus[] = dao.getInvalidNodeConnections();

                if (!ObjectValidator.IsEmptyOrNull(graphRestrictions.types)) {
                    const message : InteractiveMessage = new InteractiveMessage()
                        .Append("Restricted nodes: ");
                    graphRestrictions.nodes.forEach(($node : IKernelNode) : void => {
                        message.Append($node.Name(), () : void => {
                            selectNode($node);
                        });
                        if ($node !== graphRestrictions.nodes[graphRestrictions.nodes.length - 1]) {
                            message.Append(", ");
                        } else {
                            message.Append(".");
                        }
                    });
                    this.env.notifyError(StringUtils.Format(notifications.build_disabled___nodes_unsupported_in_platform__0__are_present,
                        this.env.projectManager.Settings().selectedPlatform));
                    this.env.printConsole("Restricted types: " + graphRestrictions.types.toString(), true);
                    this.env.printConsole(message);
                } else if (!ObjectValidator.IsEmptyOrNull(invalidInputs)) {
                    const message : InteractiveMessage = new InteractiveMessage()
                        .Append("Connection validation errors: ");
                    invalidInputs.forEach(($status : IConnectionValidatorStatus) : void => {
                        message.Append("\n" + "            ");
                        message.Append($status.sourceNode.Name() + " <Output " + $status.sourceIndex + ">",
                            () : void => {
                                selectNode($status.sourceNode);
                            });
                        message.Append(" -> ");
                        message.Append($status.targetNode.Name() + " <Input " + $status.targetIndex + ">",
                            () : void => {
                                selectNode($status.targetNode);
                            });
                        message.Append(ObjectValidator.IsEmptyOrNull($status.message) ? "" : (" (" + $status.message + ")"));
                        if ($status !== invalidInputs[invalidInputs.length - 1]) {
                            message.Append(", ");
                        } else {
                            message.Append(".");
                        }
                    });
                    this.env.notifyError(notifications.build_failed_connection_errors_found);
                    this.env.printConsole(message, true);
                } else {
                    const statusHandler : any = ($message : string, $status? : boolean) : void => {
                        if (ObjectValidator.IsSet($status)) {
                            this.env.handleButtons();
                            this.env.printStatus({
                                message: $message,
                                success: $status,
                                type   : $status ? StatusType.SUCCESS : StatusType.ERROR
                            }, "build");
                        } else {
                            this.env.printStatus({message: $message, type: StatusType.PROGRESS}, "build");
                        }
                    };
                    const stopHandler : any = ($message : string, $status : boolean) : void => {
                        if ($status && this.loopStarted) {
                            toolbar.runButton.IconName(IconType.RUN_ONCE);
                            toolbar.runButton.Title().Text(dao.getPageConfiguration().toolBarPanel.buildBar.run);
                            this.loopStarted = false;
                        }
                        statusHandler($message, $status);
                    };
                    const run : any = ($inLoop : boolean) : void => {
                        page.ShowConsole();
                        if (!$inLoop) {
                            this.env.projectManager.TaskManager().Run(false, statusHandler);
                        } else {
                            this.env.streamCounter++;
                            toolbar.runButton.Enabled(true);
                            toolbar.runButton.IconName(IconType.STOP_LOOP);
                            toolbar.runButton.Title()
                                .Text(dao.getPageConfiguration().toolBarPanel.buildBar.stop);
                            this.loopStarted = true;
                            this.env.projectManager.TaskManager().Run(true, stopHandler);
                            if (!ObjectValidator.IsEmptyOrNull(panel.visualGraphPanel.getSelectedNode())) {
                                panel.visualGraphPanel.SelectNode(panel.visualGraphPanel.getSelectedNode());
                            }
                        }
                    };

                    statusHandler(notifications.validating_graph);
                    toolbar.runButton.Enabled(false);
                    if (this.loopStarted) {
                        this.env.projectManager.TaskManager().Stop(true, stopHandler);
                    } else {
                        let inLoop : boolean = false;
                        dao.getVisualGraph().getIoComs().foreach(($ioCom : VXNode) : boolean => {
                            if ($ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.VIDEO_INPUT ||
                                $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.CAMERA) {
                                inLoop = true;
                            }
                            return !inLoop;
                        });
                        const build : any = () : void => {
                            this.env.projectManager.BuildProject(($status : IStatus) : void => {
                                this.env.printStatus($status);
                                if (ObjectValidator.IsSet($status.success)) {
                                    if ($status.success) {
                                        this.env.mapUpdated = false;
                                        run(inLoop);
                                    } else {
                                        this.env.projectManager.TaskManager().Stop(true, stopHandler);
                                    }
                                    this.env.handleButtons();
                                }
                            }, !dao.getPageConfiguration().globalSettings.buildCacheEnabled);
                        };
                        if (this.env.mapUpdated) {
                            build();
                        } else {
                            this.env.projectManager.TaskManager().ExecutableExists(($status : boolean) : void => {
                                if ($status) {
                                    run(inLoop);
                                } else {
                                    build();
                                }
                            });
                        }
                    }
                }
            });

            toolbar.openProjectFolderButton.getEvents().setOnClick(() : void => {
                this.env.projectManager.OpenProjectFolder();
            });

            const updateSettingsPanel : any = () : void => {
                if (this.env.projectManager.IsExampleLoaded()) {
                    page.settingsPanel.projectSettingsPanel.path.Enabled(false);
                } else {
                    page.settingsPanel.projectSettingsPanel.path.Enabled(true);
                }
            };

            page.settingsPanel.getEvents().setOnShow(() : void => {
                updateSettingsPanel();
            });

            page.getEvents().setOnProjectSettingsChange(($eventArgs : SettingsPanelEventArgs) : void => {
                const settings : ISettings = $eventArgs.Settings();
                const pasteSettings : any = () : void => {
                    this.env.projectManager.Settings(settings);
                    this.env.projectManager.SaveProject(($status : IStatus) : void => {
                        if ($status.success) {
                            updateSettingsPanel();
                        }
                    });
                    this.env.syncFrontend();
                };
                const updateGraph : any = ($settings : ISettings) : void => {
                    const graph : VisualGraph = dao.getVisualGraph();
                    graph.Name($settings.projectSettings.name);
                    graph.Description($settings.projectSettings.description);
                };

                if (this.env.projectManager.IsExampleLoaded()) {
                    page.settingsPanel.setDefaultSettings(this.env.projectManager.Settings());
                    onSaveRequest(true, ($success : boolean) : void => {
                        if ($success) {
                            settings.projectSettings.path = this.env.projectManager.getProjectFolder();
                            updateGraph(settings);
                            pasteSettings();
                        }
                    });
                } else {
                    pasteSettings();
                }
            });

            page.getEvents().setOnBuildPlatformChange(($eventArgs : BuildSettingsEventArgs) : void => {
                if (this.env.projectManager.IsPlatformInvalid($eventArgs.BuildPlatform().value)) {
                    const page : EditorPage = <EditorPage>this.owner.getModel().getInstance();
                    page.ShowSettings(true);
                    page.settingsPanel.ShowPanel(page.settingsPanel.toolchainSettingsPanel);

                    if (page.settingsPanel.toolchainSettingsPanel.IsCompleted()) {
                        page.settingsPanel.toolchainSettingsPanel.validateButton.Visible(true);
                    } else {
                        page.settingsPanel.toolchainSettingsPanel.getEvents().setOnComplete(() : void => {
                            page.settingsPanel.toolchainSettingsPanel.validateButton.Visible(true);
                        });
                    }
                    page.editorPanel.toolbar.setPlatforms(page.editorPanel.toolbar.getPlatforms(),
                        this.env.projectManager.Settings().selectedPlatform);
                } else {
                    this.env.projectManager.TaskManager(null);
                    this.env.handleButtons();
                    this.env.projectManager.setBuildTarget($eventArgs.BuildPlatform().value, ($status : IStatus) : void => {
                        if (ObjectValidator.IsSet($status.success)) {
                            this.env.handleButtons();
                        }
                        this.env.printStatus($status);
                    });
                }
            });

            page.settingsPanel.getEvents().setOnValidationRequest(() : void => {
                const prevManager : TaskManager = this.env.projectManager.TaskManager();
                this.env.projectManager.TaskManager(null);
                this.env.handleButtons();
                page.ShowSettings(false);
                page.settingsPanel.toolchainSettingsPanel.validateButton.Visible(false);
                this.env.projectManager.setBuildTarget(page.settingsPanel.toolchainSettingsPanel.toolchainOption.Value(),
                    ($status : IStatus) : void => {
                        if (ObjectValidator.IsSet($status.success)) {
                            if ($status.success) {
                                page.editorPanel.toolbar.setPlatforms(page.editorPanel.toolbar.getPlatforms(),
                                    page.settingsPanel.toolchainSettingsPanel.toolchainOption.Value());
                            } else {
                                page.settingsPanel.toolchainSettingsPanel.validateButton.Visible(true);
                                this.env.projectManager.TaskManager(prevManager);
                            }
                            this.env.handleButtons();
                        }
                        this.env.printStatus($status);
                    });
            });

            page.getEvents().setOnPathRequest(($eventArgs : EventArgs) : void => {
                if ($eventArgs.Owner() === exportPanel.exportPath) {
                    const filter : string[] = [
                        "Zip archive|.zip"
                    ];
                    if (exportPanel.platformType.Value() === PlatformType.LINUX || exportPanel.platformType.Value() === PlatformType.IMX) {
                        filter.push("Tar archive|.tar");
                        filter.push("Tar gz archive|.tar.gz");
                        filter.push("Tar bzip2 archive|.tar.bz2");
                    }
                    this.env.showDirectoryBrowser(($status : boolean, $path : string) : void => {
                            if ($status) {
                                exportPackageSelected = true;
                                $path = StringUtils.Replace($path, "\\", "/");
                                (<TextField>$eventArgs.Owner()).Value($path);
                            }
                        }, configuration.directoryBrowserDialog.specify_export_package_path,
                        false, false, exportPanel.exportPath.Value(), filter);
                } else {
                    this.env.showDirectoryBrowser(($status : boolean, $path : string) : void => {
                        if ($status) {
                            $path = StringUtils.Replace($path, "\\", "/");
                            (<TextField>$eventArgs.Owner()).Value($path);
                        }
                    }, configuration.directoryBrowserDialog.select_target_directory, false, true);
                }
            });

            (<any>this.owner).closeButton.getEvents().setOnClick(() : void => {
                if (this.loopStarted) {
                    this.env.projectManager.TaskManager().Stop(true, () : void => {
                        LogIt.Info("Try to kill app before app close.");
                    });
                }
                this.env.projectManager.SaveProject();
            });
        }
    }
}
