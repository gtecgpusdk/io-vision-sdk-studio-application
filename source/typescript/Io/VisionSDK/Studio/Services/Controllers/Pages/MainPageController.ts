/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Controllers.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import IMainPageLocalization = Io.VisionSDK.Studio.Services.Interfaces.DAO.IMainPageLocalization;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import EditorPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.EditorPanel;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import VXNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXNode;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import PropertyEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.PropertyEventArgs;
    import IFileSystemItemProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemItemProtocol;
    import FileSystemItemType = Com.Wui.Framework.Commons.Enums.FileSystemItemType;
    import DirectoryBrowser = Io.VisionSDK.UserControls.BaseInterface.UserControls.DirectoryBrowser;
    import DirectoryBrowserPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.DirectoryBrowserPanel;
    import FileSystemFilter = Com.Wui.Framework.Commons.Utils.FileSystemFilter;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import WindowHandlerConnector = Com.Wui.Framework.Services.Connectors.WindowHandlerConnector;
    import IWindowHandlerFileDialogSettings = Com.Wui.Framework.Services.Connectors.IWindowHandlerFileDialogSettings;
    import EditorPageViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Pages.Designer.EditorPageViewer;
    import EditorPageViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.EditorPageViewerArgs;
    import EditorPage = Io.VisionSDK.Studio.Gui.BaseInterface.Pages.Designer.EditorPage;
    import PersistenceFactory = Com.Wui.Framework.Gui.PersistenceFactory;
    import ReportServiceConnector = Com.Wui.Framework.Services.Connectors.ReportServiceConnector;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ConsolePanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ConsolePanel;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import NotificationMessageType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.NotificationMessageType;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import PropertiesPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.PropertiesPanelEventType;
    import ProjectManager = Io.VisionSDK.Studio.Services.Utils.ProjectManager;
    import IApplicationEnvironment = Io.VisionSDK.Studio.Services.Interfaces.IApplicationEnvironment;
    import IProjectEnvironment = Io.VisionSDK.Studio.Services.Interfaces.IProjectEnvironment;
    import ProjectManagerSubscriber = Io.VisionSDK.Studio.Services.Controllers.Subscribers.ProjectManagerSubscriber;
    import ICorrectAttributesStatus = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.ICorrectAttributesStatus;
    import IVXNodeAttribute = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeAttribute;
    import ContextMenuSubscriber = Io.VisionSDK.Studio.Services.Controllers.Subscribers.ContextMenuSubscriber;
    import IMainPageNotifications = Io.VisionSDK.Studio.Services.Interfaces.DAO.IMainPageNotifications;
    import AppWizardPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.AppWizardPanel;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import IAppWizardItem = Io.VisionSDK.Studio.Services.Interfaces.DAO.IAppWizardItem;
    import PropertiesPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.PropertiesPanel;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import UpdatesManagerConnector = Com.Wui.Framework.Services.Connectors.UpdatesManagerConnector;
    import HistoryManager = Io.VisionSDK.Studio.Gui.Utils.HistoryManager;
    import IStatus = Io.VisionSDK.Studio.Services.Interfaces.IStatus;
    import ISettings = Io.VisionSDK.Studio.Gui.Interfaces.ISettings;
    import IConnectionValidatorStatus = Io.VisionSDK.UserControls.Interfaces.IConnectionValidatorStatus;
    import IKernelNodeAttribute = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttribute;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import StatusType = Io.VisionSDK.Studio.Services.Enums.StatusType;
    import InteractiveMessage = Io.VisionSDK.UserControls.Utils.InteractiveMessage;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import ToolchainSettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.ToolchainSettingsPanel;
    import ConnectionSettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.ConnectionSettingsPanel;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;

    export class MainPageController extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.BasePageController {
        private static pageInstance : EditorPage;
        private static pageNotifications : IMainPageNotifications;
        private appEnvironment : IApplicationEnvironment;
        private readonly customDirectoryBrowserEnabled : boolean;
        private directoryBrowserCallback : ($status : boolean, $path) => void;
        private selectedProperty : any;

        public static AddNotification($text : string, $type : NotificationMessageType) : void {
            if (!ObjectValidator.IsEmptyOrNull(MainPageController.pageInstance)) {
                MainPageController.pageInstance.notificationPanel.AddNotification($text, $type);
                MainPageController.WriteToConsole($text);
            }
        }

        public static WriteToConsole($message : string | InteractiveMessage, $isImportant : boolean = false) : void {
            const message : InteractiveMessage = <InteractiveMessage>$message;
            if (Reflection.getInstance().IsInstanceOf(message, InteractiveMessage)) {
                LogIt.Debug(message.ToString());
            } else {
                LogIt.Debug($message);
            }

            if (!ObjectValidator.IsEmptyOrNull(MainPageController.pageInstance)) {
                (<ConsolePanel>MainPageController.pageInstance
                    .consoleDialog.PanelViewer().getInstance()).PrintMessage($message, $isImportant);
                if ($isImportant) {
                    MainPageController.pageInstance.ShowConsole();
                }
            }
        }

        public static NotifyConnectionLost() : void {
            if (!ObjectValidator.IsEmptyOrNull(MainPageController.pageInstance)) {
                MainPageController.AddNotification(MainPageController.pageNotifications.connection_has_been_lost,
                    NotificationMessageType.GENERAL_WARNING);
            }
        }

        constructor() {
            super();
            this.customDirectoryBrowserEnabled = false;
            this.setDao(new MainPageDAO());
            this.setModelClassName(EditorPageViewer);
        }

        public getModel() : EditorPageViewer {
            return <EditorPageViewer>super.getModel();
        }

        public getModelArgs() : EditorPageViewerArgs {
            return this.getDao().getModelArgs();
        }

        public getPageConfiguration() : IMainPageLocalization {
            return <IMainPageLocalization>super.getPageConfiguration();
        }

        protected browserValidator() : boolean {
            return this.getRequest().IsWuiJre() ||
                this.getRequest().getBrowserType() === BrowserType.GOOGLE_CHROME ||
                this.getRequest().getBrowserType() === BrowserType.OPERA ||
                this.getRequest().getBrowserType() === BrowserType.SAFARI;
        }

        protected stop() : void {
            if (this.browserValidator()) {
                super.stop();
            }
        }

        protected getDao() : MainPageDAO {
            return <MainPageDAO>super.getDao();
        }

        protected beforeLoad($instance : EditorPage) : void {
            ElementManager.Show("AppContentLoader");
            ElementManager.setOpacity($instance, 0);
            let allowShowApp : boolean = true;
            let showProcess : number;
            const showApp : any = () : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    if (allowShowApp) {
                        if ($instance.editorPanel.IsCompleted()) {
                            showProcess = this.getEventsManager().FireAsynchronousMethod(() : void => {
                                ElementManager.Hide("AppContentLoader");
                                ElementManager.setCssProperty(document.body, "background-color", "#ECEAEB");
                                ElementManager.setOpacity($instance, 100);
                            }, 100);
                        } else {
                            showApp();
                        }
                    }
                }, 100);
            };
            showApp();

            if (this.getRequest().IsWuiJre()) {
                const reportConnector : ReportServiceConnector = new ReportServiceConnector(10);
                const updatesConnector : UpdatesManagerConnector = new UpdatesManagerConnector(10,
                    this.getDao().getPageConfiguration().globalSettings.hubUrl + "/connector.config.jsonp");
                const environment : EnvironmentArgs = this.getEnvironmentArgs();
                reportConnector.LogIt(StringUtils.Format("Started {0}[{2}] {1}",
                    environment.getProjectName(), environment.getProjectVersion(), environment.getReleaseName()));

                updatesConnector.UpdateExists(
                    environment.getProjectName(),
                    environment.getReleaseName(),
                    environment.getPlatform(),
                    environment.getProjectVersion(),
                    new Date(environment.getBuildTime()).getTime())
                    .OnError(() : void => {
                        if (!ObjectValidator.IsEmptyOrNull(showProcess)) {
                            clearTimeout(showProcess);
                        }
                        showApp();
                    })
                    .Then(($updateAvailable : boolean) : void => {
                        const fileSystem : FileSystemHandlerConnector = new FileSystemHandlerConnector();
                        fileSystem.getTempPath().Then(($tmpPath : string) : void => {
                            const updaterName : string = "vision-sdk-updater-" + environment.getReleaseName() + ".exe";
                            const updaterPath : string = $tmpPath + "/" + updaterName;
                            fileSystem.Exists(updaterPath).Then(($updaterExists : boolean) : void => {
                                if ($updaterExists && $updateAvailable) {
                                    allowShowApp = false;
                                    if (!ObjectValidator.IsEmptyOrNull(showProcess)) {
                                        clearTimeout(showProcess);
                                    }
                                    ElementManager.Show("AppContentLoader");
                                    ElementManager.setOpacity($instance, 0);
                                    this.loaderText.Text(this.getPageConfiguration().notifications
                                        .updating_application__please_wait);
                                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                                        const detached : any = () : void => {
                                            new WindowHandlerConnector().Close();
                                        };
                                        const detachDetect : any = this.getEventsManager().FireAsynchronousMethod(detached, 2500);
                                        const terminal : TerminalConnector = new TerminalConnector();
                                        terminal
                                            .Spawn(updaterName, [], $tmpPath)
                                            .Then(($exitCode : number, $stdout : string, $stderr : string) : void => {
                                                clearTimeout(detachDetect);
                                                if ($exitCode !== 0) {
                                                    LogIt.Error("Unable to launch updater: " + $stdout + $stderr);
                                                    showApp();
                                                } else {
                                                    detached();
                                                }
                                            });
                                    }, 100);
                                } else {
                                    fileSystem.Delete(updaterPath).Then(($status : boolean) : void => {
                                        if ($status) {
                                            if ($updateAvailable) {
                                                LogIt.Info("Downloading updater ...");
                                                const updaterLink : string =
                                                    this.getPageConfiguration().updaters[environment.getReleaseName()];
                                                if (ObjectValidator.IsEmptyOrNull(updaterLink)) {
                                                    LogIt.Error("Unable to find updater for current release: " +
                                                        environment.getReleaseName());
                                                } else {
                                                    const onDownload : any = ($status : boolean) : void => {
                                                        if ($status) {
                                                            MainPageController.AddNotification(this.getPageConfiguration().notifications
                                                                    .application_update_is_available_and_will_be_executed_on_next_launch,
                                                                NotificationMessageType.GENERAL_INFO);
                                                            this.getEventsManager().FireAsynchronousMethod(() : void => {
                                                                $instance.editorPanel.toolbar.updateButton.Visible(true);
                                                            }, 250);
                                                        } else {
                                                            LogIt.Error("Unable to copy application updater to: " + updaterPath);
                                                        }
                                                    };
                                                    fileSystem
                                                        .Download(updaterLink)
                                                        .Then(($path : string) : void => {
                                                            fileSystem.Copy($path, updaterPath).Then(($status : boolean) : void => {
                                                                if ($status) {
                                                                    fileSystem.Delete($path);
                                                                }
                                                                onDownload($status);
                                                            });
                                                        });
                                                }
                                            }
                                        } else if ($updaterExists) {
                                            LogIt.Error("Unable to delete application updater at: " + updaterPath);
                                        }
                                    });
                                }
                            });
                        });
                    });
            }
        }

        protected resolver() : void {
            this.getEventsManager().setEvent(this.getClassName(), EventType.ON_COMPLETE, () : void => {
                const appFolder : string = this.getRequest().IsWuiHost() ?
                    "resource/data/projects/" + PersistenceFactory.getHttpSessionId() :
                    StringUtils.Remove(ObjectDecoder.Url(this.getRequest().getHostUrl()),
                        "file:///", "file://", "/target/index.html") + "/target";

                const model : EditorPageViewer = this.getModel();
                const instance : EditorPage = model.getInstance();
                const editorInstance : EditorPanel = instance.editorPanel;
                const dao : MainPageDAO = this.getDao();
                const notifications : IMainPageNotifications = this.getPageConfiguration().notifications;
                MainPageController.pageInstance = instance;
                MainPageController.pageNotifications = notifications;
                const propertiesPanel : PropertiesPanel = editorInstance.detailsPanel.accordion.getPanel(PropertiesPanel);
                const projectEnv : IProjectEnvironment = {
                    dao        : this.getDao(),
                    environment: this.getEnvironmentArgs(),
                    fileSystem : new FileSystemHandlerConnector(true),
                    terminal   : new TerminalConnector(true)
                };

                const projectManager : ProjectManager = new ProjectManager(appFolder, projectEnv);
                projectManager.setOnPrint(MainPageController.WriteToConsole);
                projectManager.setOnError(($message : string) : void => {
                    if (env.projectManager.IsInitialized()) {
                        editorInstance.toolbar.runButton.Enabled(true);
                        instance.ShowConsole();
                        editorInstance.setProgressStatus("");
                    }
                    MainPageController.WriteToConsole($message);
                });
                projectManager.setOnErrorNotification(($message : string) : void => {
                    env.notifyError(ObjectValidator.IsSet($message) ?
                        $message : notifications.error_occurred__see_console_output_for_further_details);
                    instance.ShowConsole();
                });
                projectManager.getHistoryPersistence().HistoryManager(editorInstance.historyManager);

                const statusQueue : ArrayList<any> = new ArrayList<any>();
                const env : IApplicationEnvironment = {
                    handleButtons       : () : void => {
                        editorInstance.toolbar.fileMenu
                            .Enabled(!ObjectValidator.IsEmptyOrNull(editorInstance.visualGraphPanel.RootNode()));
                        editorInstance.toolbar.saveButton
                            .Enabled(!ObjectValidator.IsEmptyOrNull(editorInstance.visualGraphPanel.RootNode()));
                        editorInstance.toolbar.helpButton
                            .Enabled(!ObjectValidator.IsEmptyOrNull(editorInstance.visualGraphPanel.RootNode()));
                        editorInstance.toolbar.runButton.Enabled(env.projectManager.IsInitialized());
                        editorInstance.toolbar.openProjectFolderButton.Enabled(true);
                        const historyManager : HistoryManager = projectManager.getHistoryPersistence().HistoryManager();
                        editorInstance.toolbar.undoButton.Enabled(historyManager.IsUndoable());
                        editorInstance.toolbar.redoButton.Enabled(historyManager.IsRedoable());
                    },
                    mapUpdated          : false,
                    notifyError         : ($message : string) : void => {
                        MainPageController.AddNotification($message, NotificationMessageType.GENERAL_ERROR);
                    },
                    notifyInfo          : ($message : string) : void => {
                        MainPageController.AddNotification($message, NotificationMessageType.GENERAL_INFO);
                    },
                    notifySuccess       : ($message : string) : void => {
                        MainPageController.AddNotification($message, NotificationMessageType.GENERAL_SUCCESS);
                    },
                    notifyWarning       : ($message : string) : void => {
                        MainPageController.AddNotification($message, NotificationMessageType.GENERAL_WARNING);
                    },
                    printConsole        : ($message : string | InteractiveMessage, $isImportant? : boolean) : void => {
                        MainPageController.WriteToConsole($message, $isImportant);
                    },
                    printStatus         : ($status : IStatus, $owner? : string) : void => {
                        const owner : string = ObjectValidator.IsSet($owner) ? $owner : "general";
                        if (ObjectValidator.IsSet($status.success)) {
                            const status : any = statusQueue.getItem(owner);
                            if (!ObjectValidator.IsEmptyOrNull(status)) {
                                if (statusQueue.getLast() === status) {
                                    statusQueue.RemoveLast();
                                    const next : any = statusQueue.getLast();
                                    if (!ObjectValidator.IsEmptyOrNull(next)) {
                                        editorInstance.setProgressStatus(next.message);
                                    } else {
                                        editorInstance.setProgressStatus("");
                                    }
                                } else {
                                    statusQueue.RemoveAt(statusQueue.IndexOf(status));
                                }
                            }
                            if (!$status.success) {
                                instance.ShowConsole();
                            }
                        }
                        if (ObjectValidator.IsSet($status.message)) {
                            switch ($status.type) {
                            case StatusType.SUCCESS:
                                env.notifySuccess($status.message);
                                break;
                            case StatusType.ERROR:
                                env.notifyError($status.message);
                                break;
                            case StatusType.WARNING:
                                env.notifyWarning($status.message);
                                break;
                            case StatusType.INFO:
                                env.notifyInfo($status.message);
                                break;
                            case StatusType.PROGRESS:
                                statusQueue.Add({message: $status.message}, owner);
                                editorInstance.setProgressStatus($status.message);
                                break;
                            case StatusType.LOG:
                                MainPageController.WriteToConsole($status.message);
                                break;
                            }
                        }
                    },
                    projectManager,
                    showDirectoryBrowser: ($callback : ($status : boolean, $path) => void, $description : string,
                                           $openOnly? : boolean, $folderOnly? : boolean, $initPath? : string,
                                           $filter? : string[]) : void => {
                        let path : string;
                        if (!ObjectValidator.IsEmptyOrNull($initPath)) {
                            path = $initPath;
                        } else if (projectManager.IsExampleLoaded()) {
                            path = projectManager.getAppFolder();
                        } else {
                            path = projectManager.getProjectFolder();
                        }
                        let filter : string[] = [
                            "VisualGraph project files|.jsonp"
                        ];
                        if (!ObjectValidator.IsEmptyOrNull($filter)) {
                            filter = $filter;
                        }
                        if (this.getRequest().IsWuiJre()) {
                            const fileDialogSettings : IWindowHandlerFileDialogSettings = <IWindowHandlerFileDialogSettings>{
                                filter,
                                folderOnly: $folderOnly,
                                openOnly  : $openOnly,
                                path,
                                title     : $description
                            };
                            // todo : how to get cancel result?
                            new WindowHandlerConnector().ShowFileDialog(fileDialogSettings).Then($callback);
                        } else if (this.customDirectoryBrowserEnabled) {
                            const directoryBrowserPanel : DirectoryBrowserPanel =
                                (<DirectoryBrowserPanel>instance.directoryBrowserDialog.PanelViewer().getInstance());
                            const directoryBrowser : DirectoryBrowser = directoryBrowserPanel.browser;
                            this.directoryBrowserCallback = $callback;
                            if (directoryBrowserPanel.pathValue.IsCompleted()) {
                                directoryBrowserPanel.pathValue.Value(path);
                            } else {
                                directoryBrowserPanel.getEvents().setOnComplete(() : void => {
                                    directoryBrowserPanel.pathValue.Value(path);
                                });
                            }
                            instance.directoryBrowserDialog.Visible(false);
                            instance.directoryBrowserDialog.Visible(true);

                            (<any>DirectoryBrowser).showLoader(directoryBrowser, true);
                            editorInstance.getEvents().FireAsynchronousMethod(() : void => {
                                directoryBrowser.Value(path);
                                editorInstance.getEvents().FireAsynchronousMethod(() : void => {
                                    directoryBrowser.Filter("*.*");
                                });
                            }, 150);
                        } else {
                            let newPath : string = prompt($description, path);
                            if (!ObjectValidator.IsEmptyOrNull(newPath)) {
                                newPath = StringUtils.Replace(newPath, "\\", "/");
                                if (!ObjectValidator.IsEmptyOrNull(editorInstance.visualGraphPanel.getSelectedNode())) {
                                    newPath = StringUtils.Remove(newPath, projectManager.getProjectFolder() + "/");
                                }
                                $callback(true, newPath);
                            }
                        }
                    },
                    streamCounter       : 0,
                    syncBackend         : (() : void => {
                        dao.FromGraphRoot(editorInstance.visualGraphPanel.GraphRoot());
                    }),
                    syncFrontend        : (($updateGraph? : boolean, $callback? : () => void) : void => {
                        if (editorInstance.visualGraphPanel.GraphRoot().Name() !== dao.getVisualGraph().Name()) {
                            editorInstance.visualGraphPanel.GraphRoot().Name(dao.getVisualGraph().Name());
                            editorInstance.visualGraphPanel.UpdateBreadcrumb();
                        }
                        const settings : ISettings = projectManager.Settings();
                        instance.settingsPanel.ApplySettings(settings);
                        instance.editorPanel.toolbar.setPlatforms(instance.editorPanel.toolbar.getPlatforms(), settings.selectedPlatform);

                        env.handleButtons();

                        editorInstance.visualGraphPanel.GraphRoot().getAttributes().getItem("path").value
                            = projectManager.IsExampleLoaded() ? "" : projectManager.getProjectFolder();

                        propertiesPanel.WorkingDirectory(projectManager.getProjectFolder());
                        editorInstance.Value(dao.getModelArgs().EditorPanelViewerArgs());
                        const showSelectedProperties : any = () : void => {
                            const selectedNodes : ArrayList<IKernelNode> = editorInstance.visualGraphPanel.getSelectedNodes();
                            if (!ObjectValidator.IsEmptyOrNull(editorInstance.visualGraphPanel.RootNode())
                                && selectedNodes.Length() !== 1) {
                                propertiesPanel.ShowProperties(editorInstance.visualGraphPanel.RootNode());
                            }
                            if (ObjectValidator.IsFunction($callback)) {
                                $callback();
                            }
                        };
                        // todo : merge logic
                        if ($updateGraph) {
                            editorInstance.visualGraphPanel
                                .GraphRoot(dao.getModelArgs().EditorPanelViewerArgs().VisualGraphPanelArgs().GraphRoot());
                            editorInstance.visualGraphPanel.GraphRoot().Name(dao.getVisualGraph().Name());
                            editorInstance.visualGraphPanel.RootNode(
                                editorInstance.visualGraphPanel
                                    .SearchRootNode(env.projectManager.getHistoryPersistence().HistoryManager().getCurrentRoot()),
                                () : void => {
                                    showSelectedProperties();
                                });
                        } else {
                            showSelectedProperties();
                        }
                    })
                };
                this.appEnvironment = env;

                new ContextMenuSubscriber(this).Subscribe();
                new ProjectManagerSubscriber(this, env).Subscribe();
                this.initWizard();
                this.initVisualGraph();
                this.initPropertiesPanel();
                if (this.customDirectoryBrowserEnabled) {
                    this.initDirectoryBrowser();
                }
                editorInstance.kernelNodePickerPanel.EnableDrag(false);
                editorInstance.visualGraphPanel.EnableGrouping(false);
                instance.settingsPanel.EnableBuildSettings(false);
                const toolchain : ToolchainSettingsPanel = instance.settingsPanel.toolchainSettingsPanel;
                const connection : ConnectionSettingsPanel = instance.settingsPanel.connectionSettingsPanel;
                [
                    toolchain.linkerLabel, toolchain.linker,
                    connection.usernameLabel, connection.username,
                    connection.passwordLabel, connection.password
                ].forEach(($element : GuiCommons) : void => {
                    $element.Visible(false);
                });
                env.handleButtons();
                projectManager.CreateInitialWorkspace(($status : IStatus) : void => {
                    if (ObjectValidator.IsSet($status.success)) {
                        const finishLoading : any = () : void => {
                            editorInstance.visualGraphPanel.ApplicationDirectory(env.projectManager.getAppFolder());
                            env.syncFrontend(true, () : void => {
                                editorInstance.kernelNodePickerPanel.EnableDrag(true);
                                env.handleButtons();
                                if ($status.success) {
                                    projectManager.InitTaskManager(($status : IStatus) : void => {
                                        if (ObjectValidator.IsSet($status.success)) {
                                            env.handleButtons();
                                        }
                                        env.printStatus($status, "init-task");
                                    });
                                }
                            });
                        };
                        if (editorInstance.IsCompleted()) {
                            finishLoading();
                        } else {
                            editorInstance.getEvents().setOnComplete(() : void => {
                                finishLoading();
                            });
                        }
                    }
                    env.printStatus($status, "create-workspace");
                });

                editorInstance.toolbar.consoleButton.getEvents().setOnClick(() : void => {
                    instance.ToggleConsole();
                });

                editorInstance.toolbar.statusBar.getEvents().setOnClick(() : void => {
                    instance.ShowConsole();
                });

                editorInstance.toolbar.updateButton.getEvents().setOnClick(() : void => {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        editorInstance.toolbar.updateButton.Visible(false);
                        projectEnv.fileSystem.getTempPath().Then(($tmpPath : string) : void => {
                            const updaterName : string = "vision-sdk-updater-" + this.getEnvironmentArgs().getReleaseName() + ".exe";
                            const updaterPath : string = $tmpPath + "/" + updaterName;
                            projectEnv.fileSystem.Exists(updaterPath).Then(($updaterExists : boolean) : void => {
                                if ($updaterExists) {
                                    env.handleButtons();
                                    env.notifyInfo(notifications.updating_application__please_wait);
                                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                                        const detached : any = () : void => {
                                            new WindowHandlerConnector().Close();
                                        };
                                        const detachDetect : any = this.getEventsManager().FireAsynchronousMethod(detached, 2500);
                                        const terminal : TerminalConnector = new TerminalConnector();
                                        terminal
                                            .Spawn(updaterName, [], $tmpPath)
                                            .Then(($exitCode : number, $stdout : string, $stderr : string) : void => {
                                                clearTimeout(detachDetect);
                                                if ($exitCode !== 0) {
                                                    env.handleButtons();
                                                    env.notifyError(notifications.failed_to_launch_application_update);
                                                    LogIt.Error("Unable to launch updater: " + $stdout + $stderr);
                                                } else {
                                                    detached();
                                                }
                                            });
                                    }, 300);
                                } else {
                                    env.notifyError(notifications.failed_to_launch_application_update);
                                }
                            });
                        });
                    }, true, 100);
                });
            });
            super.resolver();
        }

        protected contextMenu() : string {
            return "";
        }

        private initDirectoryBrowser() : void {
            const page : EditorPage = this.getModel().getInstance();
            const panel : EditorPanel = page.editorPanel;
            const env : IApplicationEnvironment = this.appEnvironment;

            const directoryBrowserPanel : DirectoryBrowserPanel =
                (<DirectoryBrowserPanel>page.directoryBrowserDialog.PanelViewer().getInstance());
            const directoryBrowser : DirectoryBrowser = directoryBrowserPanel.browser;

            const defaultStructure : IFileSystemItemProtocol[] = <IFileSystemItemProtocol[]>[
                {
                    map : <IFileSystemItemProtocol[]>[],
                    name: "Recent",
                    type: FileSystemItemType.RECENT
                },
                {
                    map : <IFileSystemItemProtocol[]>[],
                    name: "Favorites",
                    type: FileSystemItemType.FAVORITES
                },
                {
                    map : <IFileSystemItemProtocol[]>[],
                    name: "Computer",
                    type: FileSystemItemType.MY_COMPUTER
                }
            ];

            directoryBrowser.setStructure(defaultStructure);

            directoryBrowser.getEvents().setOnPathRequest(() : void => {
                env.projectManager.getEnvironment().fileSystem.getPathMap(directoryBrowser.Value())
                    .Then(($map : IFileSystemItemProtocol[]) : void => {
                        $map.forEach(($item : IFileSystemItemProtocol) : void => {
                            switch ($item.type) {
                            case FileSystemItemType.RECENT:
                                defaultStructure[0] = $item;
                                break;
                            case FileSystemItemType.FAVORITES:
                                (<IFileSystemItemProtocol[]>$item.map).forEach(($record : IFileSystemItemProtocol) : void => {
                                    (<IFileSystemItemProtocol[]>defaultStructure[1].map).push($record);
                                });
                                break;
                            case FileSystemItemType.MY_COMPUTER:
                                defaultStructure[2] = $item;
                                break;
                            default:
                                break;
                            }
                        });
                        directoryBrowser.setStructure(defaultStructure);
                    });
            });

            directoryBrowser.getEvents().setOnChange(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                if (page.directoryBrowserDialog.Visible() &&
                    !$manager.IsActive(directoryBrowserPanel.pathValue)) {
                    directoryBrowserPanel.pathValue.Value(directoryBrowser.Value());
                }
            });

            directoryBrowserPanel.pathValue.getEvents().setOnChange(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                let path : string = directoryBrowserPanel.pathValue.Value();

                if (page.directoryBrowserDialog.Visible() && $manager.IsActive(directoryBrowser)) {
                    if (!StringUtils.Contains(path, ":/") &&
                        !StringUtils.StartsWith(path, "/")) {
                        path = env.projectManager.getProjectFolder() + "/" + path;
                    }
                    if (directoryBrowser.Value() !== path) {
                        directoryBrowser.Value(directoryBrowserPanel.pathValue.Value());
                    }
                }
            });

            directoryBrowserPanel.saveButton.getEvents().setOnClick(() : void => {
                let path : string = directoryBrowserPanel.pathValue.Value();
                if (ObjectValidator.IsFunction(this.directoryBrowserCallback)) {
                    this.directoryBrowserCallback(true, path);
                    this.directoryBrowserCallback = null;
                } else {
                    if (!ObjectValidator.IsEmptyOrNull(panel.visualGraphPanel.getSelectedNode())) {
                        path = StringUtils.Remove(path, env.projectManager.getProjectFolder() + "/");
                    }
                    this.selectedProperty.Value(path);
                    this.selectedProperty = null;
                }
            });

            const getDirectory : any = () : void => {
                env.projectManager.getEnvironment().fileSystem.getDirectoryContent(directoryBrowserPanel.pathValue.Value())
                    .Then(($map : IFileSystemItemProtocol[]) : void => {
                        directoryBrowser.setStructure($map, directoryBrowserPanel.pathValue.Value());
                    });
            };

            directoryBrowser.getEvents().setOnDirectoryRequest(getDirectory);
            directoryBrowser.getEvents().setOnCreateDirectoryRequest(getDirectory);
            directoryBrowser.getEvents().setOnDeleteRequest(getDirectory);
            directoryBrowser.getEvents().setOnRenameRequest(getDirectory);
        }

        private initVisualGraph() : void {
            const panel : EditorPanel = this.getModel().getInstance().editorPanel;
            const dao : MainPageDAO = this.getDao();
            const env : IApplicationEnvironment = this.appEnvironment;
            const propertiesPanel : PropertiesPanel = panel.detailsPanel.accordion.getPanel(PropertiesPanel);

            dao.getVisualGraphEvents().setOnError(($eventArgs : ErrorEventArgs) : void => {
                env.notifyError($eventArgs.Message());
                LogIt.Error($eventArgs.Exception().Stack());
            });

            panel.getEvents().setOnStateChange(() : void => {
                dao.FromGraphRoot(panel.visualGraphPanel.GraphRoot());
                env.mapUpdated = true;
                panel.getEvents().FireAsynchronousMethod(() : void => {
                    env.handleButtons();
                }, 10);
            });

            panel.visualGraphPanel.getEvents().setOnRootChangeComplete(() : void => {
                dao.FromGraphRoot(panel.visualGraphPanel.GraphRoot());
            });

            panel.visualGraphPanel.Validator(($node1 : KernelNode, $node2 : KernelNode,
                                              $outputIndex : number, $inputIndex : number,
                                              $correctAttributes? : boolean,
                                              $applyChange? : boolean) : IConnectionValidatorStatus => {
                return dao.Validator($node1, $node2, $outputIndex, $inputIndex, $correctAttributes, $applyChange);
            });

            panel.visualGraphPanel.getEvents().setOnNodeSelect(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(env.projectManager.TaskManager())) {
                    const node : IKernelNode = panel.visualGraphPanel.getSelectedNode();
                    if (!ObjectValidator.IsEmptyOrNull(node)) {
                        if (/* node.Type() === "VIDEO_OUTPUT" || */
                            node.Type() === "DISPLAY") {
                            const visualNode : VXNode = dao.getVxNodeNode(node);
                            if (!ObjectValidator.IsEmptyOrNull(visualNode) &&
                                !ObjectValidator.IsEmptyOrNull(visualNode.getInputs().getItem(0))) {
                                node.setAttribute(<IKernelNodeAttribute>{
                                    format: {
                                        hidden: true
                                    },
                                    name  : "stream",
                                    value : "http://localhost:61616?width=300&height=300&version=" + env.streamCounter
                                });
                                propertiesPanel.ShowProperties(node);
                                env.projectManager.TaskManager()
                                    .SelectDisplay(visualNode.getInputs().getItem(0).UniqueId(), ($status : boolean) : void => {
                                        if (!$status) {
                                            env.notifyError(dao.getPageConfiguration().notifications.unable_to_save_runtime_config);
                                        }
                                    });
                            }
                        }
                    }
                }
            });
        }

        private initPropertiesPanel() : void {
            const page : EditorPage = this.getModel().getInstance();
            const panel : EditorPanel = page.editorPanel;
            const dao : MainPageDAO = this.getDao();
            const env : IApplicationEnvironment = this.appEnvironment;
            const propertiesPanel : PropertiesPanel = panel.detailsPanel.accordion.getPanel(PropertiesPanel);

            panel.getEvents().setOnPropertyChange(($eventArgs : PropertyEventArgs) : void => {
                const selectedNode : IKernelNode = panel.visualGraphPanel.getSelectedNode();
                if (!ObjectValidator.IsEmptyOrNull(selectedNode)) {
                    if ($eventArgs.Name() === "name") {
                        dao.FromGraphRoot(panel.visualGraphPanel.GraphRoot());
                        env.handleButtons();
                    } else {
                        const kernelAttribute : IKernelNodeAttribute = selectedNode.getAttributes().getItem($eventArgs.Name());
                        const nodeAttribute : IVXNodeAttribute =
                            dao.getVxNodeNode(selectedNode).getAttributes().getItem($eventArgs.Name());
                        if (!ObjectValidator.IsEmptyOrNull(kernelAttribute) &&
                            (kernelAttribute.value !== $eventArgs.Value() || nodeAttribute.value !== $eventArgs.Value())) {
                            env.mapUpdated = true;
                            kernelAttribute.value = $eventArgs.Value();
                            nodeAttribute.value = $eventArgs.Value();
                            const result : ICorrectAttributesStatus = dao.CorrectAttributes(selectedNode, kernelAttribute);
                            if (result.corrected.length > 0) {
                                propertiesPanel.ShowProperties(selectedNode);
                                propertiesPanel.MarkCorrections(result.corrected);
                            }
                            panel.visualGraphPanel.connectionManager.ValidateOutputs(selectedNode);
                            panel.visualGraphPanel.connectionManager.ValidateOutputs(panel.visualGraphPanel.gridManager.getNodes());

                            dao.FromGraphRoot(panel.visualGraphPanel.GraphRoot());
                            env.handleButtons();
                        }
                    }
                } else if (!ObjectValidator.IsEmptyOrNull($eventArgs.Value())) {
                    if ($eventArgs.Name() === "name") {
                        if (dao.getVisualGraph().Name() !== $eventArgs.Value()) {
                            dao.getVisualGraph().Name($eventArgs.Value());
                            env.syncFrontend();
                        }
                    }
                }
            });

            propertiesPanel.getEvents().setOnPropertySelect(($eventArgs : PropertyEventArgs) : void => {
                if ($eventArgs.PropertyType() === "Directory" ||
                    $eventArgs.PropertyType() === "ImageFile" ||
                    $eventArgs.PropertyType() === "VideoFile" ||
                    $eventArgs.PropertyType() === "DataFile") {
                    let path : string = $eventArgs.Value();
                    if (this.getRequest().IsWuiJre()) {
                        if (!StringUtils.Contains(path, ":/") &&
                            !StringUtils.StartsWith(path, "/")) {
                            path = env.projectManager.getProjectFolder() + "/" + path;
                        }
                        const fileDialogSettings : IWindowHandlerFileDialogSettings = <IWindowHandlerFileDialogSettings>{
                            path
                        };
                        if ($eventArgs.PropertyType() === "ImageFile") {
                            fileDialogSettings.filter = [
                                "Image Files|.bmp;.jpg;.jpeg;.png",
                                "BMP Files|.bmp",
                                "JPEG Files|.jpg;.jpeg",
                                "PNG Files|.png"
                            ];
                            fileDialogSettings.filterIndex = 3;
                        } else if ($eventArgs.PropertyType() === "VideoFile") {
                            fileDialogSettings.filter = [
                                "MPEG4 Files|.mp4",
                                "AVI Files|.avi"
                            ];
                            fileDialogSettings.filterIndex = 0;
                        } else if ($eventArgs.PropertyType() === "DataFile") {
                            fileDialogSettings.filter = [
                                "DATA Files|.dat;.data",
                                "TEXT Files|.txt;.csv",
                                "JSON Files|.json"
                            ];
                        }
                        if ($eventArgs.PropertyType() === "Directory") {
                            fileDialogSettings.title = this.getPageConfiguration().directoryBrowserDialog.select_target_directory;
                            fileDialogSettings.initialDirectory = env.projectManager.getProjectFolder() + "/";
                        } else if (
                            $eventArgs.PropertyType() === "ImageFile" ||
                            $eventArgs.PropertyType() === "VideoFile" ||
                            $eventArgs.PropertyType() === "DataFile") {
                            fileDialogSettings.title = this.getPageConfiguration().directoryBrowserDialog.select_input_file;
                            fileDialogSettings.openOnly = true;
                        }

                        new WindowHandlerConnector().ShowFileDialog(fileDialogSettings).Then(($status : boolean, $path : string) : void => {
                            if ($status) {
                                $path = StringUtils.Replace($path, "\\", "/");
                                if (!ObjectValidator.IsEmptyOrNull(panel.visualGraphPanel.getSelectedNode())) {
                                    $path = StringUtils.Remove($path, env.projectManager.getProjectFolder() + "/");
                                }
                                $eventArgs.Value($path);
                                this.getEventsManager()
                                    .FireEvent(propertiesPanel, PropertiesPanelEventType.ON_PROPERTY_CHANGE, $eventArgs);
                            }
                        });
                    } else if (this.customDirectoryBrowserEnabled) {
                        const directoryBrowserPanel : DirectoryBrowserPanel =
                            (<DirectoryBrowserPanel>page.directoryBrowserDialog.PanelViewer().getInstance());
                        const directoryBrowser : DirectoryBrowser = directoryBrowserPanel.browser;

                        if (directoryBrowserPanel.pathValue.IsCompleted()) {
                            directoryBrowserPanel.pathValue.Value(path);
                        } else {
                            directoryBrowserPanel.getEvents().setOnComplete(() : void => {
                                directoryBrowserPanel.pathValue.Value(path);
                            });
                        }
                        page.directoryBrowserDialog.Visible(true);
                        if (directoryBrowser.Value() !== $eventArgs.Value()) {
                            this.selectedProperty = $eventArgs.Owner();
                            if (!StringUtils.Contains($eventArgs.Value(), ":/") &&
                                !StringUtils.StartsWith($eventArgs.Value(), "/")) {
                                path = env.projectManager.getProjectFolder() + "/" + path;
                            }
                            (<any>DirectoryBrowser).showLoader(directoryBrowser, true);
                            panel.getEvents().FireAsynchronousMethod(() : void => {
                                directoryBrowser.Value(path);
                                panel.getEvents().FireAsynchronousMethod(() : void => {
                                    if ($eventArgs.PropertyType() === "ImageFile" ||
                                        $eventArgs.PropertyType() === "VideoFile" ||
                                        $eventArgs.PropertyType() === "DataFile") {
                                        directoryBrowser.Filter(FileSystemFilter.ALL);
                                    } else {
                                        directoryBrowser.Filter(FileSystemFilter.HIDDEN_DIRECTORIES);
                                    }
                                });
                            }, 150);
                        }
                    } else {
                        let title : string = "Specify directory";
                        if ($eventArgs.PropertyType() === "Directory") {
                            title = "Specify output file";
                        } else if (
                            $eventArgs.PropertyType() === "ImageFile" ||
                            $eventArgs.PropertyType() === "VideoFile" ||
                            $eventArgs.PropertyType() === "DataFile") {
                            title = this.getPageConfiguration().directoryBrowserDialog.select_input_file;
                        }
                        let newPath : string = prompt(title, path);
                        if (!ObjectValidator.IsEmptyOrNull(newPath)) {
                            newPath = StringUtils.Replace(newPath, "\\", "/");
                            if (!ObjectValidator.IsEmptyOrNull(panel.visualGraphPanel.getSelectedNode())) {
                                newPath = StringUtils.Remove(newPath, env.projectManager.getProjectFolder() + "/");
                            }
                            $eventArgs.Value(newPath);
                            this.getEventsManager()
                                .FireEvent(propertiesPanel, PropertiesPanelEventType.ON_PROPERTY_CHANGE, $eventArgs);
                        }
                    }
                }
            });
        }

        private initWizard() : void {
            const page : EditorPage = this.getModel().getInstance();
            const panel : EditorPanel = page.editorPanel;
            const dao : MainPageDAO = this.getDao();
            const env : IApplicationEnvironment = this.appEnvironment;

            const appPersistence : IPersistenceHandler = PersistenceFactory.getPersistence(this.getClassName());
            let wizardIndex : number = 0;
            let wizardItem : IAppWizardItem;
            page.wizardDialog.closeButton.Visible(false);
            dao.getAppWizardDao().mouseClick = ($x : number, $y : number, $tooltip : string, $callback? : () => void) : void => {
                (<AppWizardPanel>page.wizardDialog.PanelViewer().getInstance()).MouseClick($x, $y, $tooltip, $callback);
            };
            dao.getAppWizardDao().mouseDoubleClick = ($x : number, $y : number, $tooltip : string, $callback? : () => void) : void => {
                (<AppWizardPanel>page.wizardDialog.PanelViewer().getInstance()).MouseDoubleClick($x, $y, $tooltip, $callback);
            };
            dao.getAppWizardDao().mouseDrag = ($x : number, $y : number, $tooltip : string, $callback? : () => void) : void => {
                (<AppWizardPanel>page.wizardDialog.PanelViewer().getInstance()).MouseDrag($x, $y, $tooltip, $callback);
            };
            dao.getAppWizardDao().mouseMoveTo = ($dx : number, $dy : number, $animationMath? : ($current : number, $end : number) => number,
                                                 $callback? : () => void) : void => {
                (<AppWizardPanel>page.wizardDialog.PanelViewer().getInstance()).MouseMoveTo($dx, $dy, $animationMath, $callback);
            };
            dao.getAppWizardDao().mouseHide = () : void => {
                (<AppWizardPanel>page.wizardDialog.PanelViewer().getInstance()).MouseHide();
            };
            dao.getAppWizardDao().mouseHide();

            let isWizardComplete : boolean = false;
            page.editorPanel.visualGraphPanel.getEvents().setOnRootChangeComplete(() : void => {
                if (!isWizardComplete && !PersistenceFactory.getPersistence(this.getClassName()).Exists("CompletedWizard") ||
                    dao.getPageConfiguration().globalSettings.forceWizardShow) {
                    dao.getAppWizardDao().beforeOpen.apply(dao.getAppWizardDao(), [
                        page, dao, () : void => {
                            getNextWizardItem();
                        }
                    ]);
                }
                isWizardComplete = true;
            });

            let isItemComplete : boolean = false;
            let onItemCompleteHandler : any = null;

            const handleItemCompletion : any = () : void => {
                isItemComplete = true;
                if (ObjectValidator.IsFunction(onItemCompleteHandler)) {
                    const handler = onItemCompleteHandler;
                    onItemCompleteHandler = null;
                    handler();
                }
            };

            const getNextWizardItem : any = () : void => {
                isItemComplete = false;
                (<AppWizardPanel>page.wizardDialog.PanelViewer().getInstance()).ReleaseEvents();
                wizardItem = dao.getWizardItem(wizardIndex);
                if (!ObjectValidator.IsEmptyOrNull(wizardItem)) {
                    (<AppWizardPanel>page.wizardDialog.PanelViewer().getInstance()).ReleaseEvents();
                    wizardItem.before.apply(dao.getAppWizardDao(), [
                        page, dao, ($instance : IGuiCommons) : void => {
                            page.wizardDialog.PanelViewer().ViewerArgs(this.getModelArgs().AppWizardPanelViewerArgs());
                            page.ShowWizard($instance, wizardItem.dimensions);
                            handleItemCompletion();
                        }
                    ]);
                } else {
                    dao.getAppWizardDao().beforeClose.apply(dao.getAppWizardDao(), [
                        page, dao, () : void => {
                            page.HideWizard();
                            appPersistence.Variable("CompletedWizard", this.getEnvironmentArgs().getProjectVersion());
                            handleItemCompletion();
                            env.handleButtons();
                        }
                    ]);
                }
            };

            const wizardForwardHandler : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(wizardItem)) {
                    if (isItemComplete) {
                        isItemComplete = false;
                        wizardIndex++;
                        wizardItem.after.apply(dao.getAppWizardDao(), [
                            page, dao, () : void => {
                                getNextWizardItem();
                            }
                        ]);
                    } else if (ObjectValidator.IsEmptyOrNull(onItemCompleteHandler)) {
                        onItemCompleteHandler = wizardForwardHandler;
                    }
                }
            };

            const wizardBackHandler : any = () : void => {
                if (isItemComplete) {
                    isItemComplete = false;
                    if (wizardIndex > 0) {
                        wizardIndex--;
                        wizardItem.after.apply(dao.getAppWizardDao(), [
                            page, dao, () : void => {
                                (<AppWizardPanel>page.wizardDialog.PanelViewer().getInstance()).ReleaseEvents();
                                wizardItem = dao.getWizardItem(wizardIndex);
                                wizardItem.before.apply(dao.getAppWizardDao(), [
                                    page, dao, ($instance : IGuiCommons) : void => {
                                        page.wizardDialog.PanelViewer().ViewerArgs(this.getModelArgs().AppWizardPanelViewerArgs());
                                        page.ShowWizard($instance, wizardItem.dimensions);
                                        handleItemCompletion();
                                    }
                                ]);
                            }
                        ]);
                    } else {
                        handleItemCompletion();
                    }
                } else if (ObjectValidator.IsEmptyOrNull(onItemCompleteHandler)) {
                    onItemCompleteHandler = wizardBackHandler;
                }
            };

            page.getEvents().setOnWizardForward(wizardForwardHandler);
            page.getEvents().setOnWizardBack(wizardBackHandler);
            page.getEvents().setOnWizardClose(() : void => {
                wizardIndex = 0;
            });

            panel.toolbar.helpButton.getEvents().setOnClick(() : void => {
                wizardIndex = 1;
                dao.getAppWizardDao().beforeOpen.apply(dao.getAppWizardDao(), [
                    page, dao, () : void => {
                        getNextWizardItem();
                    }
                ]);
            });
        }
    }
}
