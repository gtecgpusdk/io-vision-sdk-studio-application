/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.CodeGenerator.Core {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import VisualGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VisualGraph;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ICodeGeneratorTemplates = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.ICodeGeneratorTemplates;
    import IVisualGraphTemplates = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVisualGraphTemplates;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import VXNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXNode;
    import VXContext = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXContext;
    import VXGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXGraph;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import VisualGraphDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.VisualGraphDAO;
    import IVXNodeCode = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeCode;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import IScriptTemplates = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IScriptTemplates;
    import Linter = Com.Wui.Framework.XCppLint.Core.Linter;
    import LintError = Com.Wui.Framework.XCppLint.Core.Model.LintError;
    import Severity = Com.Wui.Framework.XCppLint.Enums.Severity;

    export class CppCodeGenerator extends BaseObject {
        private fileSystem : FileSystemHandlerConnector;
        private projectDirectory : string;
        private readonly projectNamespace : string;
        private visualGraph : VisualGraph;
        private readonly templates : ICodeGeneratorTemplates;
        private codeCache : any[];
        private linter : Linter;
        private readonly linterConfig : any;
        private readonly platform : PlatformType;

        constructor($graph : VisualGraph, $dao : VisualGraphDAO, $platform : PlatformType) {
            super();

            this.fileSystem = new FileSystemHandlerConnector();
            this.projectDirectory = "";
            this.visualGraph = $graph;
            this.projectNamespace = this.normalizeChars(this.visualGraph.Name(), /[^a-zA-Z0-9_]/g);
            this.templates = $dao.getTemplates();
            this.platform = Property.EnumType(this.platform, $platform, PlatformType, PlatformType.WIN_GCC);
            this.codeCache = [];
            this.linter = new Linter();
            this.linterConfig = {
                extensions: [
                    "cpp",
                    "hpp"
                ],
                fix       : [
                    "errors-whitespace-newline-missingAfterOpenBrace",
                    "errors-other-fileNewLine",
                    "errors-whitespace-newline-multipleCommands",
                    "errors-whitespace-newline-missingBeforeCloseBrace",
                    "errors-whitespace-newline-missingAfterCloseBrace",
                    "errors-whitespace-newline-missingAfterVisibilitySpec",
                    "errors-whitespace-newline-missingAfterComment",
                    "errors-whitespace-newline-missingBeforeComment",
                    "errors-whitespace-newline-missingBeforeInitList",
                    "errors-whitespace-indent-invalidIndent",
                    "errors-whitespace-blankLine-preceded",
                    "errors-whitespace-newline-missingAfterTemplate",
                    "errors-whitespace-newline-missingBeforeDecDef",
                    "errors-readability-wui-methodOrder",
                    "errors-readability-wui-classOrder",
                    "errors-readability-wui-lineEndings"
                ],
                ignore    : [],
                lineEnding: (this.platform === PlatformType.WIN_GCC || this.platform === PlatformType.MSVC) ? "<CRLF>" : "<LF>",
                maxLineLen: 140,
                spaces    : 4
            };
        }

        public ProjectDirectory($value? : string) : string {
            return this.projectDirectory = Property.String(this.projectDirectory, $value);
        }

        public GenerateCode($callback : ($status : boolean, $filesMap? : string[]) => void) : void {
            this.linter.CleanErrors();
            this.codeCache = [];
            const contextsCount : number = this.visualGraph.getVxContexts().Length();
            let graphsCount : number;
            const contexts : string[] = [];
            const graphs : string[] = [];
            let displayOutput : boolean = false;
            let contextsConstruction : string = "";
            let contextsCreate : string = "";
            let contextsValidate : string = "";
            let contextsBeforeProcess : string = "";
            let contextsProcess : string = "";
            let contextsAfterProcess : string = "";

            const getNextGraph : any = ($context : VXContext, $index : number, $callback : () => void) : void => {
                if ($index < graphsCount) {
                    const graph : VXGraph = $context.getVxGraphs().getItem($index);
                    graphs.push(graph.UniqueId());
                    let hppProperties : string = "";
                    const cppConstruction : string = "";
                    let cppCreate : string = "";
                    let cppValidate : string = "";
                    let cppBeforeProcess : string = "";
                    let cppProcess : string = "";
                    let cppAfterProcess : string = "";

                    const dataLen : number = graph.getVxData().Length();
                    const sortedData : ArrayList<VXNode> = new ArrayList<VXNode>();

                    cppValidate += this.templates.cpp.vxGraph.validate.apply(this.templates, [graph]);
                    cppProcess += this.templates.cpp.vxGraph.process.apply(this.templates, [graph]);

                    while (sortedData.Length() < dataLen) {
                        graph.getVxData().foreach(($vxData1 : VXNode) : boolean => {
                            let isDependant : boolean = false;
                            graph.getVxData().foreach(($vxData2 : VXNode) : boolean => {
                                if ($vxData1 !== $vxData2 && $vxData1.getParams().Contains($vxData2)) {
                                    isDependant = true;
                                    return false;
                                }
                            });
                            if (!isDependant) {
                                sortedData.Add($vxData1);
                                graph.getVxData().RemoveAt(graph.getVxData().IndexOf($vxData1));
                                return false;
                            }
                        });
                    }

                    sortedData.foreach(($vxData : VXNode) : void => {
                        graph.getVxData().Add($vxData);
                    });

                    this.visualGraph.getIoComs().foreach(($ioCom : VXNode) : void => {
                        if ($ioCom.getOwners().Contains(graph)) {
                            const code : IVXNodeCode = $ioCom.ToCode();

                            if ($ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.IMAGE_INPUT ||
                                $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.VIDEO_INPUT ||
                                $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.DATA_INPUT ||
                                $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.CAMERA) {
                                cppBeforeProcess += code.process("            ");
                            } else {
                                cppAfterProcess += code.process("            ");
                            }
                        }
                    });

                    graph.getVxData().foreach(($node : VXNode) : void => {
                        const code : IVXNodeCode = $node.ToCode();
                        hppProperties += code.property("        ");
                        cppCreate += code.create("            ");
                    });
                    if (!ObjectValidator.IsEmptyOrNull(cppCreate) && !graph.getVxNodes().IsEmpty()) {
                        cppCreate += "\r\n";
                    }

                    const nodeLen : number = graph.getVxNodes().Length();
                    const sortedNodes : ArrayList<VXNode> = new ArrayList<VXNode>();

                    while (sortedNodes.Length() < nodeLen) {
                        graph.getVxNodes().foreach(($vxNode1 : VXNode) : boolean => {
                            let isDependant : boolean = false;
                            graph.getVxNodes().foreach(($vxNode2 : VXNode) : boolean => {
                                $vxNode1.getInputs().foreach(($input : VXNode) : boolean => {
                                    if ($vxNode2.getOutputs().Contains($input)) {
                                        isDependant = true;
                                        return false;
                                    }
                                });
                                if (isDependant) {
                                    return false;
                                }
                            });
                            if (!isDependant) {
                                sortedNodes.Add($vxNode1);
                                graph.getVxNodes().RemoveAt(graph.getVxNodes().IndexOf($vxNode1));
                                return false;
                            }
                        });
                    }

                    sortedNodes.foreach(($vxNode : VXNode) : void => {
                        graph.getVxNodes().Add($vxNode);
                    });

                    graph.getVxNodes().foreach(($node : VXNode) : void => {
                        const code : IVXNodeCode = $node.ToCode();
                        hppProperties += code.property("        ");
                        cppCreate += code.create("            ");
                    });

                    this.generateHppFile(this.templates.cpp.vxGraph, graph.UniqueId(), [], hppProperties);
                    this.generateCppFile(this.templates.cpp.vxGraph, graph.UniqueId(), [],
                        cppConstruction, cppCreate, cppValidate, cppBeforeProcess + cppProcess + cppAfterProcess);
                    getNextGraph($context, $index + 1, $callback);
                } else {
                    $callback();
                }
            };
            const getNextContext : any = ($index : number, $callback : () => void) : void => {
                if ($index < contextsCount) {
                    const context : VXContext = this.visualGraph.getVxContexts().getItem($index);
                    contexts.push(context.UniqueId());
                    contextsConstruction += this.templates.cpp.vxContext.construction.apply(this.templates, [context]);
                    contextsCreate += this.templates.cpp.vxContext.create.apply(this.templates, [context]);
                    contextsValidate += this.templates.cpp.vxContext.validate.apply(this.templates, [context]);
                    contextsBeforeProcess += this.templates.cpp.vxContext.beforeProcess.apply(this.templates, [context]);
                    contextsProcess += this.templates.cpp.vxContext.process.apply(this.templates, [context]);
                    contextsAfterProcess += this.templates.cpp.vxContext.afterProcess.apply(this.templates, [context]);

                    const graphs : string[] = [];
                    let cppConstruction : string = "";
                    let cppCreate : string = "";
                    let cppBeforeProcess : string = "";
                    let cppValidate : string = "";
                    context.getVxGraphs().foreach(($graph : VXGraph) : void => {
                        graphs.push($graph.UniqueId());
                        contextsBeforeProcess += this.templates.cpp.vxGraph.beforeProcess.apply(this.templates, [$graph]);
                        cppConstruction += this.templates.cpp.vxGraph.construction.apply(this.templates, [$graph]);
                        cppCreate += this.templates.cpp.vxGraph.create.apply(this.templates, [$graph]);
                        cppBeforeProcess += this.templates.cpp.vxGraph.afterProcess.apply(this.templates, [$graph]);
                        cppValidate += this.templates.cpp.vxGraph.validate.apply(this.templates, [$graph]);
                    });

                    this.generateHppFile(this.templates.cpp.vxContext, context.UniqueId(), [], "");
                    this.generateCppFile(this.templates.cpp.vxContext, context.UniqueId(), graphs,
                        cppConstruction, cppCreate, cppValidate, cppBeforeProcess);
                    graphsCount = context.getVxGraphs().Length();
                    getNextGraph(context, 0, () : void => {
                        getNextContext($index + 1, $callback);
                    });
                } else {
                    $callback();
                }
            };
            getNextContext(0, () : void => {
                let hppProperties : string = "";
                let cppCreate : string = "";
                let cppBeforeProcess : string = "";
                const outputFolders : string[] = [];

                this.visualGraph.getIoComs().foreach(($ioCom : VXNode) : void => {
                    const code : IVXNodeCode = $ioCom.ToCode();
                    hppProperties += code.property("        ");
                    cppBeforeProcess += code.create("        ");

                    if ($ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.DISPLAY) {
                        displayOutput = true;
                    }

                    if (!ObjectValidator.IsEmptyOrNull($ioCom.getAttributeValue("path")) &&
                        ($ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.IMAGE_OUTPUT ||
                            $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.VIDEO_OUTPUT ||
                            $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.DATA_OUTPUT)) {
                        let path : string = StringUtils.Replace($ioCom.getAttributeValue("path"), "\\", "/");
                        path = StringUtils.Replace(path, "//", "/");
                        if (!StringUtils.Contains(path, ":/")) {
                            path = this.projectDirectory + "/" + path;
                        }
                        path = StringUtils.Substring(path, 0, StringUtils.IndexOf(path, "/", false));
                        if (outputFolders.indexOf(path) === -1) {
                            outputFolders.push(path);
                        }
                    }
                });

                this.generateHppFile(this.templates.cpp.visualGraph, "VisualGraph", [], hppProperties);
                this.generateBaseClass();
                if (!ObjectValidator.IsEmptyOrNull(cppCreate)) {
                    cppCreate += "\r\n";
                }
                this.generateCppFile(this.templates.cpp.visualGraph,
                    "VisualGraph", contexts,
                    contextsConstruction,
                    cppCreate + contextsCreate,
                    contextsValidate,
                    contextsBeforeProcess + cppBeforeProcess + contextsProcess + contextsAfterProcess);
                this.generateReferenceFile(contexts.concat(graphs));
                this.generateMainFile();
                this.generateCmakeFile(contexts.concat(graphs));
                this.generateLicenseFile();
                const prepareOutputs : any = ($index : number) : void => {
                    if ($index < outputFolders.length) {
                        this.codeCache.push({
                            path: outputFolders[$index],
                            task: "CreateDirectory"
                        });
                        prepareOutputs($index + 1);
                    } else {
                        this.linter.getErrors().forEach(($error : LintError) : void => {
                            if ($error.getSeverity() === Severity.ERROR) {
                                LogIt.Warning("LINTER: " + $error.ToString());
                            }
                        });
                        this.saveFiles($callback);
                    }
                };
                prepareOutputs(0);
            });
        }

        public GenerateScripts($callback : ($status : boolean, $filesMap? : string[]) => void) : void {
            let templates : IScriptTemplates;
            let extension : string;
            if (this.platform === PlatformType.WIN_GCC) {
                templates = this.templates.scripts.winGCC;
                extension = "cmd";
            } else if (this.platform === PlatformType.IMX) {
                templates = this.templates.scripts.imx;
                extension = "sh";
            } else {
                templates = this.templates.scripts.linux;
                extension = "sh";
            }
            const executableName : string = (<any>this.visualGraph.Name()).replace(new RegExp(/[^a-zA-Z0-9_\-.]/g), "_");
            let inLoop : boolean = false;
            this.visualGraph.getIoComs().foreach(($ioCom : VXNode) : boolean => {
                if ($ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.VIDEO_INPUT ||
                    $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.CAMERA) {
                    inLoop = true;
                }
                return !inLoop;
            });
            this.codeCache.push({
                content: templates.build(executableName),
                path   : this.projectDirectory + "/build." + extension,
                task   : "Write"
            });
            this.codeCache.push({
                content: templates.run(executableName, inLoop),
                path   : this.projectDirectory + "/run." + extension,
                task   : "Write"
            });
            this.codeCache.push({
                content: templates.clean(executableName),
                path   : this.projectDirectory + "/clean." + extension,
                task   : "Write"
            });
            this.codeCache.push({
                content: templates.readMe(this.visualGraph.Name(), this.visualGraph.Description()),
                path   : this.projectDirectory + "/README.txt",
                task   : "Write"
            });
            this.saveFiles($callback);
        }

        private saveFiles($callback : ($status : boolean, $filesMap? : string[]) => void) : void {
            const filesMap : string[] = [];
            const processNextFileSystemTask : any = ($index : number) : void => {
                if ($index < this.codeCache.length) {
                    const task : any = this.codeCache[$index];
                    filesMap.push(task.path);
                    switch (task.task) {
                    case "CreateDirectory":
                        this.fileSystem
                            .Exists(task.path)
                            .Then(($status : boolean) : void => {
                                if (!$status) {
                                    LogIt.Debug("Create directory: {0}", task.path);
                                    this.fileSystem
                                        .CreateDirectory(task.path)
                                        .Then(($status : boolean) : void => {
                                            if ($status) {
                                                processNextFileSystemTask($index + 1);
                                            } else {
                                                $callback(false);
                                            }
                                        });
                                } else {
                                    processNextFileSystemTask($index + 1);
                                }
                            });
                        break;
                    case "Write":
                        const write : any = () : void => {
                            LogIt.Debug("Create/Update file: {0}", task.path);
                            this.fileSystem.Write(task.path, task.content)
                                .Then(($status : boolean) : void => {
                                    if ($status) {
                                        processNextFileSystemTask($index + 1);
                                    } else {
                                        $callback(false);
                                    }
                                });
                        };
                        this.fileSystem
                            .Exists(task.path)
                            .Then(($status : boolean) : void => {
                                if (!$status) {
                                    write();
                                } else {
                                    this.fileSystem
                                        .Read(task.path)
                                        .Then(($content : string) : void => {
                                            if (task.content !== $content) {
                                                write();
                                            } else {
                                                processNextFileSystemTask($index + 1);
                                            }
                                        });
                                }
                            });
                        break;
                    default:
                        processNextFileSystemTask($index + 1);
                        break;
                    }
                } else {
                    $callback(true, filesMap);
                }
            };
            processNextFileSystemTask(0);
        }

        private checkDirectory($dirPath : string) : void {
            this.codeCache.push({
                path: $dirPath,
                task: "CreateDirectory"
            });
        }

        private generateHppFile($template : IVisualGraphTemplates,
                                $className : string, $includes : string[], $properties : string) : void {
            this.checkDirectory(this.projectDirectory + "/include");
            const path : string = this.projectDirectory + "/include/" + $className + ".hpp";
            this.codeCache.push({
                content: this.lintFile($template.header.apply(this.templates, [
                    this.projectNamespace,
                    $className,
                    $includes,
                    $properties
                ]), path),
                path,
                task   : "Write"
            });
        }

        private generateCppFile($template : IVisualGraphTemplates, $className : string, $includes : string[],
                                $construction : string, $create : string, $validate : string, $process : string) : void {
            this.checkDirectory(this.projectDirectory + "/source");
            const path : string = this.projectDirectory + "/source/" + $className + ".cpp";
            this.codeCache.push({
                content: this.lintFile($template.source.apply(this.templates, [
                    this.projectNamespace,
                    $className,
                    $includes,
                    $construction,
                    $create,
                    $validate,
                    $process
                ]), path),
                path,
                task   : "Write"
            });
        }

        private generateReferenceFile($classes : string[]) : void {
            const path : string = this.projectDirectory + "/include/Reference.hpp";
            this.codeCache.push({
                content: this.lintFile(this.templates.cpp.reference.apply(this.templates, [
                    this.projectNamespace, $classes
                ]), path),
                path,
                task   : "Write"
            });
        }

        private generateBaseClass() : void {
            this.generateHppFile(this.templates.cpp.manager, "Manager", [], "");
            this.generateCppFile(this.templates.cpp.manager, "Manager", [], "", "", "", "");
        }

        private generateMainFile() : void {
            const path : string = this.projectDirectory + "/source/main.cpp";
            this.codeCache.push({
                content: this.lintFile(this.templates.cpp.main.apply(this.templates, [
                    this.projectNamespace, this.visualGraph.Name()
                ]), path),
                path,
                task   : "Write"
            });
        }

        private generateCmakeFile($includes : string[]) : void {
            const includes : string[] = [];
            const sources : string[] = [];
            $includes.forEach(($className : string) : void => {
                includes.push($className + ".hpp");
                sources.push($className + ".cpp");
            });
            this.codeCache.push({
                content: this.templates.cmake(
                    this.normalizeChars(this.visualGraph.Name(), /[^a-zA-Z0-9_\-.]/g),
                    includes.concat([
                        "VisualGraph.hpp",
                        "Reference.hpp",
                        "Manager.hpp"
                    ]),
                    sources.concat([
                        "VisualGraph.cpp",
                        "Manager.cpp"
                    ])),
                path   : this.projectDirectory + "/CMakeLists.txt",
                task   : "Write"
            });
        }

        private generateLicenseFile() : void {
            this.codeCache.push({
                content: this.templates.cpp.license(),
                path   : this.projectDirectory + "/LICENSE.txt",
                task   : "Write"
            });
        }

        private normalizeChars($input : string, $regex : RegExp) : string {
            return (<any>$input).replace($regex, "_");
        }

        private lintFile($content : string, $path : string) : string {
            this.linter.Run($content, $path, this.linterConfig, ($fixedFileData : string) : void => {
                $content = $fixedFileData;
            });
            return $content;
        }
    }
}
