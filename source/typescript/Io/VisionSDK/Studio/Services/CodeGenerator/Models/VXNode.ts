/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.CodeGenerator.Models {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import IVXNode = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNode;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IVXNodeTemplate = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeTemplate;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IVisualGraph = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVisualGraph;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IVXNodeAttribute = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeAttribute;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IVXNodeModel = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeModel;
    import IVXNodeCode = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeCode;
    import IVXNodeJson = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeJson;
    import IVXDataJson = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXDataJson;
    import ICorrectAttributesStatus = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.ICorrectAttributesStatus;
    import IKernelNodeAttribute = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttribute;
    import IConnectionValidatorStatus = Io.VisionSDK.UserControls.Interfaces.IConnectionValidatorStatus;
    import IKernelNodeHelp = Io.VisionSDK.UserControls.Interfaces.IKernelNodeHelp;

    export class VXNode extends VisualGraphNode {
        private icon : IconType;
        private readonly attributes : ArrayList<IVXNodeAttribute>;
        private readonly params : ArrayList<any>;
        private readonly inputs : ArrayList<VXNode>;
        private readonly outputs : ArrayList<VXNode>;
        private template : IVXNodeTemplate;
        private help : IKernelNodeHelp;
        private readonly owners : ArrayList<VisualGraphNode>;

        constructor() {
            super();
            this.attributes = new ArrayList<IVXNodeAttribute>();
            this.params = new ArrayList<any>();
            this.inputs = new ArrayList<VXNode>();
            this.outputs = new ArrayList<VXNode>();
            this.help = {};
            this.owners = new ArrayList<VisualGraphNode>();
        }

        public Parent($value? : VisualGraph | VXGraph) : VisualGraph | VXGraph {
            return <VisualGraph | VXGraph>super.Parent($value);
        }

        public Name($value? : string) : string {
            let name : string = super.Name($value);
            if (!ObjectValidator.IsSet($value) && ObjectValidator.IsEmptyOrNull(name) && !ObjectValidator.IsEmptyOrNull(this.template)) {
                name = this.getVarName();
                super.Name(name);
            }
            return name;
        }

        public Icon($value? : IconType) : IconType {
            if (ObjectValidator.IsSet($value)) {
                this.icon = $value;
            }
            return this.icon;
        }

        public getAttributes() : ArrayList<IVXNodeAttribute> {
            return this.attributes;
        }

        public getAttribute($name : string) : IVXNodeAttribute {
            const attribute : IVXNodeAttribute = this.attributes.getItem($name);
            if (!ObjectValidator.IsEmptyOrNull(attribute)) {
                return attribute;
            }
            return null;
        }

        public setAttribute($value : IVXNodeAttribute) : void {
            return this.attributes.Add($value, $value.varName);
        }

        public getAttributeValue($name : string) : any {
            const attribute : IVXNodeAttribute = this.attributes.getItem($name);
            if (!ObjectValidator.IsEmptyOrNull(attribute)) {
                return attribute.value;
            }
            return null;
        }

        public getHelp() : IKernelNodeHelp {
            return this.help;
        }

        public getParams() : ArrayList<any> {
            return this.params;
        }

        public getInputs() : ArrayList<VXNode> {
            return this.inputs;
        }

        public getOutputs() : ArrayList<VXNode> {
            return this.outputs;
        }

        public getOwners() : ArrayList<VisualGraphNode> {
            return this.owners;
        }

        public ToKernelNode() : KernelNode {
            const node : KernelNode = new KernelNode(this.Icon(), this.Row(), this.Column());
            node.Type(this.Type());
            node.Name(this.Name());
            node.UniqueId(this.UniqueId());
            const model : IVXNodeModel = this.ToModel();
            model.outputs().forEach(($node : IVXNode) => {
                node.AddOutput((<any>$node).Type());
            });
            model.inputs().forEach(($node : IVXNode) => {
                node.AddInput((<any>$node).Type());
            });
            this.getAttributes().foreach(($attribute : IVXNodeAttribute) : void => {
                node.setAttribute(<IKernelNodeAttribute>{
                    format: $attribute.format,
                    items : $attribute.options,
                    name  : $attribute.varName,
                    text  : $attribute.name,
                    type  : $attribute.type,
                    value : $attribute.value
                });
            });
            this.getAttributes().foreach(($attribute : IVXNodeAttribute) : void => {
                model.correctAttributes($attribute, true);
            });
            node.Help(this.help);
            return node;
        }

        public Configuration($value? : IVisualGraph) : IVisualGraph {
            const configuration : IVisualGraph = super.Configuration($value);
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.loadTemplate();
            }
            return configuration;
        }

        public FromKernelNode($node : KernelNode) : void {
            super.FromKernelNode($node);
            this.loadTemplate();
            this.Icon($node.IconType());
            $node.getAttributes().foreach(($attribute : IKernelNodeAttribute) : void => {
                const property : IVXNodeAttribute = this.getAttributes().getItem($attribute.name);
                if (!ObjectValidator.IsEmptyOrNull(property)) {
                    property.value = $attribute.value;
                }
            });
        }

        public FromNodeConfiguration($node : IVXNode) : void {
            super.FromNodeConfiguration($node);
            this.loadTemplate();
            if (!ObjectValidator.IsEmptyOrNull(this.template)) {
                this.Icon(this.template.icon);
                this.ToModel().attributes($node);
            }
        }

        public ToCode() : IVXNodeCode {
            if (ObjectValidator.IsEmptyOrNull(this.template) || ObjectValidator.IsEmptyOrNull(this.template.cpp)) {
                return {
                    property($prefix : string = "    ") : string {
                        return "";
                    },
                    create($prefix : string = "    ") : string {
                        return "";
                    },
                    process($prefix : string = "    ") : string {
                        return "";
                    }
                };
            }
            const params : any[] = [this];
            this.getParams().foreach(($param : any) : void => {
                params.push($param);
            });
            return {
                create  : ($prefix : string = "    ") : string => {
                    let output : string = "";
                    if (!ObjectValidator.IsEmptyOrNull(this.template.cpp.create)) {
                        this.errorHandler("cpp.create", () : void => {
                            output = this.template.cpp.create.apply(this.Configuration(), [$prefix].concat(params));
                        });
                    }
                    return output;
                },
                process : ($prefix : string = "    ") : string => {
                    let output : string = "";
                    if (!ObjectValidator.IsEmptyOrNull(this.template.cpp.process)) {
                        this.errorHandler("cpp.process", () : void => {
                            output = this.template.cpp.process.apply(this.Configuration(), [$prefix].concat(params));
                        });
                    }
                    return output;
                },
                property: ($prefix : string = "    ") : string => {
                    let output : string = "";
                    if (!ObjectValidator.IsEmptyOrNull(this.template.cpp.property)) {
                        this.errorHandler("cpp.property", () : void => {
                            output = this.template.cpp.property.apply(this.Configuration(), [$prefix].concat(params));
                        });
                    }
                    return output;
                }
            };
        }

        public ToJsonp() : IVXNodeJson {
            if (ObjectValidator.IsEmptyOrNull(this.template) || ObjectValidator.IsEmptyOrNull(this.template.jsonp)) {
                return {
                    data($prefix : string = "    ") : IVXDataJson[] {
                        return [];
                    },
                    node($prefix : string = "    ") : string {
                        return "";
                    }
                };
            }
            return {
                data: ($prefix : string = "    ") : IVXDataJson[] => {
                    let output : IVXDataJson[] = [];
                    if (!ObjectValidator.IsEmptyOrNull(this.template.jsonp.data)) {
                        this.errorHandler("jsonp.data", () : void => {
                            output = this.template.jsonp.data.apply(this.Configuration(), [$prefix, this]);
                        });
                    }
                    return output;
                },
                node: ($prefix : string = "    ") : string => {
                    let output : string = "";
                    if (!ObjectValidator.IsEmptyOrNull(this.template.jsonp.node)) {
                        this.errorHandler("jsonp.node", () : void => {
                            output = this.template.jsonp.node.apply(this.Configuration(), [$prefix, this]);
                        });
                    }
                    return output;
                }
            };
        }

        public ToModel() : IVXNodeModel {
            if (ObjectValidator.IsEmptyOrNull(this.template)
                || ObjectValidator.IsEmptyOrNull(this.template.model)) {
                return {
                    attributes($config? : IVXNode) : IVXNodeAttribute[] {
                        return [];
                    },
                    inputs() : IVXNode[] {
                        return [];
                    },
                    outputs() : IVXNode[] {
                        return [];
                    },
                    params() : IVXNode[] {
                        return [];
                    },
                    validator($target : IVXNode, $outputIndex : number, $inputIndex : number, $correctAttributes? : boolean,
                              $applyChanges? : boolean) : IConnectionValidatorStatus {
                        return <IConnectionValidatorStatus>{
                            message: "",
                            status : true
                        };
                    },
                    validateAttribute($item : IVXNode, $sourceAttribute : IVXNodeAttribute | IVXNodeAttribute[], $inputIndex : number,
                                      $correctAttributes? : boolean, $applyChanges? : boolean) : IConnectionValidatorStatus {
                        return <IConnectionValidatorStatus>{
                            message: "",
                            status : true
                        };
                    },
                    correctAttributes($changedAttribute : IVXNodeAttribute, $applyChanges? : boolean) : ICorrectAttributesStatus {
                        return <ICorrectAttributesStatus>{
                            corrected: []
                        };
                    }
                };
            }
            return {
                attributes       : ($config? : IVXNode) : IVXNodeAttribute[] => {
                    let attributes : IVXNodeAttribute[] = [];
                    if (!ObjectValidator.IsEmptyOrNull(this.template.model.attributes)) {
                        this.errorHandler("model.attributes", () : void => {
                            attributes = this.template.model.attributes.apply(this.Configuration(), [this, $config]);
                        });
                    }
                    return attributes;
                },
                correctAttributes: ($changedAttribute : IVXNodeAttribute, $applyChanges? : boolean) : ICorrectAttributesStatus => {
                    let output : ICorrectAttributesStatus = <ICorrectAttributesStatus>{
                        corrected: []
                    };
                    if (!ObjectValidator.IsEmptyOrNull(this.template.model.correctAttributes)) {
                        this.errorHandler("model.correctAttributes", () : void => {
                            output = this.template.model.correctAttributes.apply(this.Configuration(), [
                                this, $changedAttribute.varName, $changedAttribute.value, $applyChanges
                            ]);
                        });
                    }
                    return output;
                },
                inputs           : () : IVXNode[] => {
                    let types : string[] = [];
                    if (!ObjectValidator.IsEmptyOrNull(this.template.model.inputs)) {
                        this.errorHandler("model.inputs", () : void => {
                            types = this.template.model.inputs.apply(this.Configuration());
                        });
                    }
                    const output : IVXNode[] = [];
                    types.forEach(($type : string) : void => {
                        const node : VXNode = new VXNode();
                        node.Type($type);
                        node.Parent(this.Parent());
                        node.Configuration(this.Configuration());
                        node.getOutputs().Add(this);
                        output.push(<any>node);
                    });
                    return output;
                },
                outputs          : () : IVXNode[] => {
                    let types : string[] = [];
                    if (!ObjectValidator.IsEmptyOrNull(this.template.model.outputs)) {
                        this.errorHandler("model.outputs", () : void => {
                            types = this.template.model.outputs.apply(this.Configuration());
                        });
                    }
                    const output : IVXNode[] = [];
                    types.forEach(($type : string) : void => {
                        const node : VXNode = new VXNode();
                        node.Type($type);
                        node.Parent(this.Parent());
                        node.Configuration(this.Configuration());
                        node.getInputs().Add(this);
                        output.push(<any>node);
                    });
                    return output;
                },
                params           : () : IVXNode[] => {
                    let output : IVXNode[] = [];
                    if (!ObjectValidator.IsEmptyOrNull(this.template.model.params)) {
                        this.errorHandler("model.params", () : void => {
                            output = this.template.model.params.apply(this.Configuration(), [this]);
                        });
                    }
                    return output;
                },
                validator        : ($target : IVXNode, $outputIndex : number, $inputIndex : number,
                                    $correctAttributes? : boolean, $applyChanges? : boolean) : IConnectionValidatorStatus => {
                    let output : IConnectionValidatorStatus = <IConnectionValidatorStatus>{
                        message: "",
                        status : true
                    };
                    if (!ObjectValidator.IsEmptyOrNull(this.template.model.validator)) {
                        this.errorHandler("model.validator", () : void => {
                            output = this.template.model.validator.apply(this.Configuration(), [
                                this, $target, $outputIndex, $inputIndex, $correctAttributes, $applyChanges
                            ]);
                        });
                    }
                    return output;
                },
                validateAttribute($item : IVXNode, $sourceAttribute : IVXNodeAttribute | IVXNodeAttribute[], $inputIndex : number,
                                  $correctAttributes? : boolean, $applyChanges? : boolean) : IConnectionValidatorStatus {
                    let output : IConnectionValidatorStatus = <IConnectionValidatorStatus>{
                        message: "",
                        status : true
                    };
                    if (!ObjectValidator.IsEmptyOrNull(this.template.model.validateAttribute)) {
                        this.errorHandler("model.validateAttribute", () : void => {
                            output = this.template.model.validateAttribute.apply(
                                this.Configuration(),
                                [$item, $sourceAttribute, $inputIndex, $correctAttributes, $applyChanges]
                            );
                        });
                    }
                    return output;
                }
            };
        }

        protected getVarId() : string {
            const index : string = this.Index() !== -1 ? this.Index().toString() : "";
            if (this.Configuration().vxAPI.vxIoComs.hasOwnProperty(this.Type())) {
                return "ioCom" + index;
            } else if (this.Configuration().vxAPI.vxNodes.hasOwnProperty(this.Type())) {
                return "vxNode" + index;
            } else {
                return "vxData" + index;
            }
        }

        private loadTemplate() : void {
            const configuration : IVisualGraph = this.Configuration();
            if (!ObjectValidator.IsEmptyOrNull(configuration) && !ObjectValidator.IsEmptyOrNull(this.Type())) {
                if (configuration.vxAPI.vxIoComs.hasOwnProperty(this.Type())) {
                    this.template = configuration.vxAPI.vxIoComs[this.Type()];
                } else if (configuration.vxAPI.vxNodes.hasOwnProperty(this.Type())) {
                    this.template = configuration.vxAPI.vxNodes[this.Type()];
                } else {
                    this.template = configuration.vxAPI.vxData[this.Type()];
                }
                if (!ObjectValidator.IsEmptyOrNull(this.template.help)) {
                    this.help = JSON.parse(JSON.stringify(this.template.help));
                }
            }
        }

        private getVarName() : string {
            const index : string = this.Index() !== -1 ? this.Index().toString() : "";
            if (!ObjectValidator.IsEmptyOrNull(this.template.varName)) {
                return this.template.varName + index;
            } else {
                if (this.Configuration().vxAPI.vxNodes.hasOwnProperty(this.Type())) {
                    return "vxNode" + index;
                }
                return null;
            }
        }

        private errorHandler($dataType : string, $execution : () => void) : void {
            try {
                $execution();
            } catch (ex) {
                ex.message = "Unable to process " + $dataType + " template for node" + StringUtils.NewLine(false) +
                    this.ToString("", false) + StringUtils.NewLine(false) +
                    ex.message;
                Loader.getInstance().getHttpResolver().getEvents()
                    .FireEvent(VisualGraphNode.ClassName(), EventType.ON_ERROR, new ErrorEventArgs(ex));
            }
        }
    }
}
