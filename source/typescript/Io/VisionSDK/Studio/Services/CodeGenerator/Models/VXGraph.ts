/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.CodeGenerator.Models {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IVXGraph = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXGraph;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class VXGraph extends VisualGraphNode {
        private readonly vxNodes : ArrayList<VXNode>;
        private readonly vxData : ArrayList<VXNode>;
        private varName : string;

        constructor() {
            super();

            this.vxNodes = new ArrayList<VXNode>();
            this.vxData = new ArrayList<VXNode>();
        }

        public Parent($value? : VXContext) : VXContext {
            return <VXContext>super.Parent($value);
        }

        public getVxNodes() : ArrayList<VXNode> {
            return this.vxNodes;
        }

        public getVxData() : ArrayList<VXNode> {
            return this.vxData;
        }

        public VarName($value? : string) : string {
            this.varName = Property.String(this.varName, $value);
            if (ObjectValidator.IsEmptyOrNull(this.varName)) {
                this.varName = "graph" + this.Index();
            }
            return this.varName;
        }

        public getReference() : string {
            return "this.vxContexts[" + this.Parent().Index() + "].vxGraphs[" + this.Index() + "]";
        }

        public FromNodeConfiguration($node : IVXGraph) : void {
            super.FromNodeConfiguration($node);
        }

        public ToString($prefix? : string, $htmlTag? : boolean) : string {
            return "" +
                $prefix + "VXNodes: " + this.getVxNodes().ToString($prefix + StringUtils.Tab(1, $htmlTag), $htmlTag) +
                StringUtils.NewLine($htmlTag) +
                $prefix + "VXData: " + this.getVxData().ToString($prefix + StringUtils.Tab(1, $htmlTag), $htmlTag);
        }

        protected getVarId() : string {
            const index : string = this.Index() !== -1 ? this.Index().toString() : "";
            return "Graph" + index;
        }
    }
}
