/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectDecoderValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import TaskManager = Io.VisionSDK.Studio.Services.Utils.TaskManager;
    import RemoteTargetManager = Io.VisionSDK.Studio.Services.Utils.RemoteTargetManager;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import IMainPageNotifications = Io.VisionSDK.Studio.Services.Interfaces.DAO.IMainPageNotifications;
    import IAgentInfo = Com.Wui.Framework.Services.Connectors.IAgentInfo;
    import AgentsRegisterConnector = Com.Wui.Framework.Services.Connectors.AgentsRegisterConnector;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import APIType = Io.VisionSDK.Studio.Services.Enums.APIType;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;

    export class AgentsTest extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RuntimeTestRunner {
        private connection : RemoteTargetManager;
        private taskManager : TaskManager;
        private agentsList : HTMLSelectElement;
        private hubsList : HTMLSelectElement;
        private notifications : IMainPageNotifications;
        private readonly hubs : string[];
        private agentsInfo : IAgentInfo[];
        private readonly toolchains : any;
        private sdkVersionList : HTMLSelectElement;
        private testCaseList : HTMLSelectElement;
        private readonly testConfig : ITestConfig[];
        private readonly runtimeVersion : string;
        private testRoot : string;

        constructor() {
            super();
            this.setMethodFilter("CheckAgentsConnectionTest", "AgentOperationTest");
            this.hubs = [
                "https://hub.visionstudio.io",
                "https://hub.dev.visionstudio.io",
                "https://hub.eap.visionstudio.io"
            ];
            this.testConfig = [
                {
                    resourceRoot: "/test/resource/data/Io/VisionSDK/Studio/Services/CodeGenerator/OpenVX_1_1",
                    sdkVersion  : APIType.OVX_1_1_0,
                    testCases   : [
                        "testFunctional",
                        "testAllNodes"
                    ]
                },
                {
                    resourceRoot: "/test/resource/data/Io/VisionSDK/Studio/Services/CodeGenerator/OpenVX_1_2",
                    sdkVersion  : APIType.OVX_1_2_0,
                    testCases   : [
                        "testFunctional",
                        "testXorNN"
                    ]
                }
            ];

            this.toolchains = {
                Linux : {
                    cmake   : "/var/lib/com-wui-framework-builder/appdata/external_modules/cmake/bin",
                    compiler: null
                },
                WinGCC: {
                    cmake   : "c:/wui/com-wui-framework-builder/appdata/cmake/bin",
                    compiler: "c:/wui/com-wui-framework-builder/appdata/msys2/mingw64/bin"
                },
                iMX   : {
                    cmake   : null,
                    compiler: null
                }
            };

            this.runtimeVersion = "2019.1.0";
        }

        public CheckAgentsConnectionTest() : void {
            this.addButton("Check agent - run before test", () : void => {
                this.initConnection(() : void => {
                    // dummy
                });
            });
        }

        public AgentOperationTest() : void {
            this.addButton("Clean up", () : void => {
                this.initTaskManager(() : void => {
                    this.taskManager.Clean(($status : boolean) : void => {
                        if ($status === true) {
                            Echo.Println("Clean up successful");
                        } else {
                            Echo.Println("Clean up failed");
                        }
                    });
                });
            });

            this.addButton("Remove SDK", () : void => {
                this.initTaskManager(() : void => {
                    this.taskManager.RemoveSDK(($status : boolean) : void => {
                        if ($status === true) {
                            Echo.Println("SDK removed");
                        } else {
                            Echo.Println("Failed to remove SDK");
                        }
                    });
                });
            });

            this.addButton("Synchronize", () : void => {
                this.initTaskManager(() : void => {
                    this.taskManager.Synchronize(($message : string, $status : boolean) : void => {
                        if ($status === true) {
                            Echo.Println("OK: " + $message);
                        } else {
                            Echo.Println("Fail: " + $message);
                        }
                    });
                });
            });

            this.addButton("Build", () : void => {
                this.initTaskManager(() : void => {
                    this.taskManager.Build(($message : string, $status? : boolean) : void => {
                        Echo.Println($message);
                        if (!ObjectDecoderValidator.IsEmptyOrNull($status)) {
                            if ($status === true) {
                                Echo.Println("build successful");
                            } else {
                                Echo.Println("build failed");
                            }
                        }
                    });
                });
            });

            this.addButton("Run", () : void => {
                this.initTaskManager(() : void => {
                    this.taskManager.Run(false, ($message : string, $status : boolean) : void => {
                        if ($status === true) {
                            Echo.Println("run successful");
                        } else {
                            Echo.Println("run failed");
                        }
                    });
                });
            });
        }

        protected before() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                const dao : MainPageDAO = new MainPageDAO();
                dao.Load(LanguageType.EN, () : void => {
                    this.notifications = dao.getPageConfiguration().notifications;
                    const value : string = this.hubsList.value;
                    LogIt.Debug("before: {0}", value);
                    this.updateAgents(value, () : void => {
                        $done();
                    });
                });
            };
        }

        protected resolver() : void {
            super.resolver();

            Echo.Print(
                "<style>.TextField input{z-index: 0 !important;}</style>"
            );

            const hubListId : string = this.getClassNameWithoutNamespace() + "_hubList";
            const agentListId : string = this.getClassNameWithoutNamespace() + "_agentList";
            const sdkListId : string = this.getClassNameWithoutNamespace() + "_sdkList";
            const testCasesListId : string = this.getClassNameWithoutNamespace() + "_testCasesList";

            Echo.Print(
                "<h2>Target configuration</h2>" +
                "Hub<br>" +
                "<select id=\"" + hubListId + "\"></select>" +
                "<br>" +
                "Agent<br>" +
                "<select id=\"" + agentListId + "\"></select>" +
                "<br>" +
                "SDK<br>" +
                "<select id=\"" + sdkListId + "\"></select>" +
                "<br>" +
                "TestCase<br>" +
                "<select id=\"" + testCasesListId + "\"></select>" +
                "<br>"
            );

            this.agentsList = <HTMLSelectElement>document.getElementById(agentListId);
            this.hubsList = <HTMLSelectElement>document.getElementById(hubListId);
            this.sdkVersionList = <HTMLSelectElement>document.getElementById(sdkListId);
            this.testCaseList = <HTMLSelectElement>document.getElementById(testCasesListId);

            this.hubs.forEach(($hub : string) : void => {
                this.insertItem(this.hubsList, $hub);
            });
            this.testConfig.forEach((cfg : ITestConfig) : void => {
                this.insertItem(this.sdkVersionList, cfg.sdkVersion.toString());
            });
            this.updateTestCases();
            this.insertItem(this.agentsList, "not-available");

            this.agentsList.onchange = () : void => {
                LogIt.Debug("Agent changed to: {0}", this.agentsList.value);
            };
            this.hubsList.onchange = () : void => {
                LogIt.Debug("Hub changed to: {0}", this.hubsList.value);
                this.updateAgents(this.hubsList.value);
            };
            this.sdkVersionList.onchange = () : void => {
                const sdkVersion : string = this.sdkVersionList.value;
                LogIt.Debug("SDK version changed to: {0}", sdkVersion);
                this.updateTestCases();
            };
            this.testCaseList.onchange = () : void => {
                const testCase : string = this.testCaseList.value;
                LogIt.Debug("TestCase changed to: {0}", testCase);
            };
        }

        private updateTestCases() : void {
            const currentSdk : string = this.sdkVersionList.value;
            this.testConfig.forEach((cfg : ITestConfig) : void => {
                if (currentSdk === cfg.sdkVersion.toString()) {
                    for (let i = this.testCaseList.options.length - 1; i >= 0; i--) {
                        this.testCaseList.remove(i);
                    }
                    this.testRoot = cfg.resourceRoot;
                    cfg.testCases.forEach(($case : string) : void => {
                        this.insertItem(this.testCaseList, $case);
                    });
                }
            });
        }

        private insertItem($element : HTMLSelectElement, $value : string) : void {
            if (!ObjectValidator.IsEmptyOrNull($element)) {
                const option : HTMLOptionElement = document.createElement("option");
                option.value = $value;
                option.text = $value;
                $element.appendChild(option);
            }
        }

        private initConnection($callback : ($changed : boolean) => void) : void {
            const agentsListValue : string = this.agentsList.value;
            const hubsListValue : string = this.hubsList.value;
            if (ObjectValidator.IsEmptyOrNull(this.connection) ||
                this.connection.getAgentName() !== agentsListValue ||
                !StringUtils.Contains(this.connection.getServerConfiguration(), hubsListValue)) {
                LogIt.Debug("Init connection for {0} over {1}", agentsListValue, hubsListValue);
                this.connection = new RemoteTargetManager({
                    agentName: agentsListValue,
                    host     : "",
                    pass     : "",
                    user     : ""
                }, {
                    homePath : this.getAbsoluteRoot() + "/..",
                    msysPath : "",
                    serverUrl: hubsListValue,
                    sshPort  : 0
                });
                (<any>this.connection).isAgent = true;
                this.connection.setOnError(($message : string) : void => {
                    Echo.Printf($message);
                });
                this.connection.setOnPrint(($message : string) : void => {
                    Echo.PrintCode($message);
                    WindowManager.ScrollToBottom();
                });
                this.connection.Start(($status : boolean) : void => {
                    if (!$status) {
                        LogIt.Error("Unable to start communication with agent.");
                    } else {
                        $callback(true);
                    }
                });
            } else {
                LogIt.Debug("Connection for this configuration has been established already.");
                $callback(false);
            }
        }

        private initTaskManager($callback? : () => void) : void {
            const getPlatform : any = ($info : IAgentInfo) : PlatformType => {
                let type : PlatformType;
                if (StringUtils.ContainsIgnoreCase($info.platform, "win")) {
                    type = PlatformType.WIN_GCC;
                } else {
                    type = PlatformType.LINUX;
                    if (StringUtils.ContainsIgnoreCase($info.platform, "arm")) {
                        type = PlatformType.IMX;
                    }
                }
                return type;
            };
            this.initConnection(($changed : boolean) : void => {
                if (ObjectValidator.IsEmptyOrNull(this.taskManager) || $changed ||
                    (this.sdkVersionList.value !== (<any>this.taskManager).environment.sdkVersion) ||
                    (this.testCaseList.value.replace(
                        new RegExp(/[^a-zA-Z0-9_\-.]/g), "_") !== (<any>this.taskManager).targetName)) {
                    let info : IAgentInfo = null;
                    this.agentsInfo.forEach(($info : IAgentInfo) : void => {
                        if ($info.name === this.agentsList.value) {
                            info = $info;
                        }
                    });
                    if (ObjectValidator.IsEmptyOrNull(info)) {
                        LogIt.Error("Agent info not found");
                        this.taskManager = null;
                        $callback();
                    } else {
                        const agentPlatform : string = getPlatform(info).toString();
                        const agentToolchain : IAgentToolchain = this.toolchains[agentPlatform.toString()];
                        this.taskManager = new TaskManager(this.getAbsoluteRoot() + this.testRoot + "/" +
                            this.testCaseList.value, this.testCaseList.value,
                            {
                                agentName     : info.name,
                                cmakePath     : agentToolchain.cmake,
                                compilerPath  : agentToolchain.compiler,
                                notifications : this.notifications,
                                platform      : agentPlatform,
                                runtimeVersion: (ObjectValidator.IsEmptyOrNull(this.runtimeVersion) ? "2019.0.1" : this.runtimeVersion),
                                sdkVersion    : this.sdkVersionList.value,
                                serverUrl     : this.hubsList.value
                            },
                            this.connection);
                        this.taskManager.setOnPrint(($message : string) : void => {
                            if (StringUtils.StartsWith($message, "> sync")) {
                                LogIt.Debug($message);
                            } else {
                                Echo.Println($message);
                            }
                        });
                        this.taskManager.Init(() : void => {
                            $callback();
                        });
                    }
                } else {
                    $callback();
                }
            });
        }

        private updateAgents($hub : string, $callback? : () => void) : void {
            let callback : any = () : void => {
                // dummy
            };
            if (!ObjectValidator.IsEmptyOrNull($callback)) {
                callback = $callback;
            }
            LogIt.Debug("Update agents for {0}", $hub);
            if (!ObjectValidator.IsEmptyOrNull($hub) && StringUtils.StartsWith($hub, "http")) {
                const agentsRegister : AgentsRegisterConnector = new AgentsRegisterConnector(true, $hub + "/connector.config.jsonp");
                agentsRegister.getAgentsList()
                    .Then(($agentsList : IAgentInfo[]) : void => {
                        for (let i = this.agentsList.options.length - 1; i >= 0; i--) {
                            this.agentsList.remove(i);
                        }
                        this.agentsInfo = $agentsList;
                        if ($agentsList.length > 0) {
                            $agentsList.forEach(($agentInfo : IAgentInfo) : void => {
                                this.insertItem(this.agentsList, $agentInfo.name);
                            });
                        } else {
                            this.insertItem(this.agentsList, "not-available");
                        }
                        this.agentsList.selectedIndex = 0;

                        callback();
                    });
            } else {
                callback();
            }
        }
    }

    class IAgentToolchain {
        public cmake : string;
        public compiler : string;
    }

    class ITestConfig {
        public sdkVersion : APIType;
        public resourceRoot : string;
        public testCases : string[];
    }
}
/* dev:end */
