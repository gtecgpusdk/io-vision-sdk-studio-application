/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import VisualGraphDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.VisualGraphDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;

    export class NamespaceFallbackTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testLoad() : IRuntimeTestPromise {
            const dao : VisualGraphDAO = new VisualGraphDAO();
            dao.setConfigurationPath("test/resource/data/Io/VisionSDK/Studio/Services/DAO/CodeGenerator/OpenVX_1_0_1/" +
                "NamespaceFallbackTest.jsonp");
            return ($done : any) : void => {
                dao.Load(LanguageType.EN, () : void => {
                    $done();
                });
            };
        }
    }
}
/* dev:end */
