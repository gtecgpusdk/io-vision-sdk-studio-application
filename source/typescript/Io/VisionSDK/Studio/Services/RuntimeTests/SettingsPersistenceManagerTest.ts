/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import SettingsDAO = Io.VisionSDK.Studio.Services.DAO.Pages.SettingsDAO;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import SettingsPersistenceManager = Io.VisionSDK.Studio.Services.Utils.SettingsPersistenceManager;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import ToolchainManager = Io.VisionSDK.Studio.Services.Utils.ToolchainManager;
    import ISettings = Io.VisionSDK.Studio.Gui.Interfaces.ISettings;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class SettingsPersistenceManagerTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {
        private readonly fileSystem : FileSystemHandlerConnector;
        private readonly terminal : TerminalConnector;
        private manager : SettingsPersistenceManager;
        private dao : SettingsDAO;
        private localSettingsPath : string;
        private globalSettingsPath : string;
        private appDataPath : string;
        private projectId : string;
        private testPath : string;

        constructor() {
            super();
            this.fileSystem = new FileSystemHandlerConnector();
            this.terminal = new TerminalConnector();
        }

        public TestSettings() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.initEnv(() : void => {
                    const settings : ISettings = this.getSettingsMock();
                    this.manager.Settings(settings);
                    const processed : ISettings = this.manager.Settings();
                    this.assertEquals(processed.selectedPlatform, settings.selectedPlatform,
                        "Platform equals");
                    this.assertEquals(JSON.stringify(processed.projectSettings), JSON.stringify(settings.projectSettings),
                        "Project settings equals");
                    this.assertEquals(JSON.stringify(processed.toolchainSettings), JSON.stringify(settings.toolchainSettings),
                        "Toolchain settings equals");
                    this.assertEquals(JSON.stringify(processed.connectionSettings), JSON.stringify(settings.connectionSettings),
                        "Connection settings equals");
                    $done();
                });
            };
        }

        public TestInitDefaultSettings() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.initEnv(() : void => {
                    this.manager.Init(this.projectId, this.localSettingsPath, () : void => {
                        const settings : ISettings = this.manager.Settings();
                        this.assertEquals(ObjectValidator.IsEmptyOrNull(settings.projectSettings), true,
                            "Project settings is empty");
                        this.assertEquals(ObjectValidator.IsEmptyOrNull(settings.selectedPlatform), false,
                            "Selected platform exists");
                        this.assertEquals(ObjectValidator.IsEmptyOrNull(settings.connectionSettings.options), false,
                            "Connection settings exists");
                        this.assertEquals(ObjectValidator.IsEmptyOrNull(settings.toolchainSettings.options), false,
                            "Toolchain settings exists");
                        $done();
                    });
                });
            };
        }

        public TestSave() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.initEnv(() : void => {
                    this.manager.Settings(this.getSettingsMock());
                    this.manager.Save(this.projectId, this.localSettingsPath, ($status : boolean) : void => {
                        this.assertEquals($status, true, "Save status : " + $status);
                        $done();
                    });
                });
            };
        }

        public TestSaveGlobal() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.initEnv(() : void => {
                    this.manager.Settings(this.getSettingsMock());
                    this.manager.SaveGlobal(($status : boolean) : void => {
                        this.assertEquals($status, true, "Save global status : " + $status);
                        $done();
                    });
                });
            };
        }

        public TestInitSavedSettings() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.initEnv(() : void => {
                    const savedSettings : ISettings = this.getSettingsMock();
                    this.manager.Settings(savedSettings);
                    this.manager.Save(this.projectId, this.localSettingsPath, ($status : boolean) : void => {
                        this.assertEquals($status, true, "Manager save success");
                        if ($status) {
                            this.manager
                                = new SettingsPersistenceManager(this.globalSettingsPath, this.dao, this.fileSystem, this.terminal);
                            this.manager.Init(this.projectId, this.localSettingsPath, () : void => {
                                const settings : ISettings = this.manager.Settings();
                                this.assertEquals(settings.projectSettings.name, savedSettings.projectSettings.name,
                                    "Name equals");
                                this.assertEquals(settings.selectedPlatform, savedSettings.selectedPlatform,
                                    "Selected platform equals");
                                this.assertEquals(settings.connectionSettings.options[0].name,
                                    savedSettings.connectionSettings.options[0].name,
                                    "Connection settings equals");
                                this.assertEquals(settings.toolchainSettings.options[0].name,
                                    savedSettings.toolchainSettings.options[0].name,
                                    "Toolchain settings equals");
                                $done();
                            });
                        } else {
                            $done();
                        }
                    });
                });
            };
        }

        public TestInitDifferentIdSamePath() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.initEnv(() : void => {
                    const savedSettings : ISettings = this.getSettingsMock();
                    this.manager.Settings(savedSettings);
                    this.manager.Save(this.projectId, this.localSettingsPath, ($status : boolean) : void => {
                        this.assertEquals($status, true, "Manager save success");
                        if ($status) {
                            this.manager.Init(this.projectId + this.projectId, this.localSettingsPath, () : void => {
                                const settings : ISettings = this.manager.Settings();
                                this.assertEquals(ObjectValidator.IsEmptyOrNull(settings.projectSettings), true,
                                    "Project settings is empty");
                                this.assertEquals(settings.selectedPlatform, savedSettings.selectedPlatform,
                                    "Selected platform equals");
                                $done();
                            });
                        } else {
                            $done();
                        }
                    });
                });
            };
        }

        protected setUp() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.testPath = this.getAbsoluteRoot() + "/target/test/settings-persistence-manager-test";
                this.localSettingsPath = this.testPath + "/LOCAL-SETTINGS";
                this.globalSettingsPath = this.testPath + "/GLOBAL-SETTINGS";
                this.projectId = "TEST-PROJECT-ID";
                ToolchainManager.getLocalAppDataPath(this.terminal, ($appDataPath : string) : void => {
                    this.appDataPath = $appDataPath;
                    $done();
                });
            };
        }

        protected tearDown() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Delete(this.testPath)
                    .Then(() : void => {
                        $done();
                    });
            };
        }

        private initEnv($callback : () => void) : void {
            this.dao = new SettingsDAO();
            this.manager = new SettingsPersistenceManager(this.globalSettingsPath, this.dao, this.fileSystem, this.terminal);
            this.fileSystem.Delete(this.testPath)
                .Then(() : void => {
                    $callback();
                });
        }

        private getSettingsMock() : ISettings {
            /* tslint:disable: object-literal-sort-keys */
            return {
                selectedPlatform  : "Test Local 2",
                projectSettings   : {
                    description: "Test description",
                    name       : "Test name",
                    path       : "Test path",
                    vxVersion  : "Test vxVersion"
                },
                toolchainSettings : {
                    options: [
                        {
                            cmake   : "%localappdata%/VisionSDK/ExternalModules/cmake/bin",
                            compiler: "%localappdata%/VisionSDK/ExternalModules/msys2/mingw64/bin",
                            linker  : "",
                            name    : "Test Local 1",
                            platform: "WinGCC"
                        },
                        {
                            cmake   : "%localappdata%/VisionSDK/ExternalModules/cmake/bin",
                            compiler: "C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/MSBuild/15.0/Bin",
                            linker  : "",
                            name    : "Test Local 2",
                            platform: "MSVC"
                        }
                    ]
                },
                connectionSettings: {
                    options: [
                        {
                            address : "https://hub.visionstudio.io",
                            cmake   : "%localappdata%/remote/cmake",
                            name    : "Test Connection 1",
                            platform: "Linux"
                        },
                        {
                            address : "https://hub.visionstudio.io",
                            cmake   : "%localappdata%/remote/cmake",
                            name    : "Test Connection 2",
                            platform: "Windows"
                        }
                    ]
                }
            };
            /* tslint:enable */
        }
    }
}
/* dev:end */
