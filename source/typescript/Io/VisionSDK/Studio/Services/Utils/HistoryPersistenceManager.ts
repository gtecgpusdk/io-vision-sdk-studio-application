/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Utils {
    "use strict";
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import HistoryManager = Io.VisionSDK.Studio.Gui.Utils.HistoryManager;
    import IHistoryCache = Io.VisionSDK.Studio.Gui.Interfaces.IHistoryCache;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class HistoryPersistenceManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private fileSystem : FileSystemHandlerConnector;
        private historyPath : string;
        private historyManager : HistoryManager;

        constructor($fileSystem : FileSystemHandlerConnector) {
            super();
            this.fileSystem = $fileSystem;
        }

        public HistoryManager($value? : HistoryManager) : HistoryManager {
            return this.historyManager = ObjectValidator.IsSet($value) ? $value : this.historyManager;
        }

        public Save($id : string, $path : string, $callback : ($status : boolean) => void) : void {
            this.setProjectInfo($id, $path);
            if (!ObjectValidator.IsEmptyOrNull(this.historyManager)) {
                try {
                    this.fileSystem.Write(this.historyPath, JSON.stringify(this.historyManager.HistoryCache())).Then($callback);
                } catch (ex) {
                    LogIt.Error("Unable to store project history.", ex);
                    $callback(false);
                }
            } else {
                LogIt.Warning("Running without front-end so project history has been ignored.");
                $callback(true);
            }
        }

        public Init($id : string, $path : string, $loadFromDisk : boolean, $callback? : () => void) : void {
            if ($loadFromDisk && !ObjectValidator.IsEmptyOrNull(this.historyManager) && $id !== this.historyManager.HistoryCache().id) {
                this.setProjectInfo($id, $path);
                const setCache : any = ($value? : IHistoryCache) : void => {
                    this.historyManager.HistoryCache(ObjectValidator.IsSet($value) ? $value : new IHistoryCache($id));
                    if (ObjectValidator.IsFunction($callback)) {
                        $callback();
                    }
                };
                const identifyCache : any = ($historyCache : IHistoryCache) : void => {
                    if ($historyCache.id === this.historyManager.HistoryCache().id) {
                        if (!ObjectValidator.IsSet($historyCache.graphChanges)) {
                            $historyCache.graphChanges = [];
                        }
                        setCache($historyCache);
                    } else {
                        setCache();
                    }
                };
                const loadCache : any = () : void => {
                    this.fileSystem.Read(this.historyPath).Then(($data : string) => {
                        if (!ObjectValidator.IsEmptyOrNull($data)) {
                            try {
                                try {
                                    identifyCache($data);
                                } catch (ex) {
                                    ObjectDecoder.Unserialize($data, identifyCache);
                                }
                            } catch (ex) {
                                LogIt.Error("Unable to load project history.", ex);
                                setCache();
                            }
                        } else {
                            setCache();
                        }
                    });
                };
                Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
                    this.fileSystem.Exists(this.historyPath).Then(($success : boolean) => {
                        if ($success) {
                            loadCache();
                        } else {
                            const fallbackPath : string = StringUtils.Remove(this.historyPath, ".obj");
                            this.fileSystem.Exists(fallbackPath).Then(($success : boolean) => {
                                if ($success) {
                                    this.fileSystem.Rename(fallbackPath, this.historyPath).Then(($success : boolean) : void => {
                                        if ($success) {
                                            loadCache();
                                        } else {
                                            setCache();
                                        }
                                    });
                                } else {
                                    setCache();
                                }
                            });
                        }
                    });
                }, 150);
            } else {
                this.setProjectInfo($id, $path);
                if (ObjectValidator.IsFunction($callback)) {
                    $callback();
                }
            }
        }

        private setProjectInfo($id : string, $path : string) : void {
            if (!ObjectValidator.IsEmptyOrNull(this.historyManager)) {
                if ($id !== this.historyManager.HistoryCache().id) {
                    this.historyManager.HistoryCache(new IHistoryCache($id));
                }
            }
            this.historyPath = $path + "/.visionsdk/history.obj";
        }
    }
}
