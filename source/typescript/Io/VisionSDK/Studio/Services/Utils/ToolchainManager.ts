/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Utils {
    "use strict";
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import InstallationRecipeDAO = Com.Wui.Framework.Services.DAO.InstallationRecipeDAO;
    import InstallationProgressEventArgs = Com.Wui.Framework.Services.Events.Args.InstallationProgressEventArgs;
    import InstallationProgressCode = Com.Wui.Framework.Services.Enums.InstallationProgressCode;
    import InstallationProtocolType = Com.Wui.Framework.Services.Enums.InstallationProtocolType;
    import IInstallationUtilValuePromise = Com.Wui.Framework.Services.Interfaces.DAO.IInstallationUtilValuePromise;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IFileSystemDownloadPromise = Com.Wui.Framework.Services.Connectors.IFileSystemDownloadPromise;
    import FileSystemDownloadOptions = Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;

    export class ToolchainManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private readonly fileSystem : FileSystemHandlerConnector;
        private readonly terminal : TerminalConnector;
        private printHandler : ($message : string) => void;

        public static getLocalAppDataPath($terminal : TerminalConnector, $callback : ($path : string) => void) : void {
            $terminal.Execute("cmd", ["/c", "echo", "%USERPROFILE%"])
                .Then(($exitCode : number, $stdOut : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($exitCode)) {
                        if ($exitCode === 0) {
                            let appDataPath : string = StringUtils.Remove($stdOut, "\r\n", "\n", "\\\"").trim() + "/AppData/Local";
                            appDataPath = StringUtils.Replace(appDataPath, "\\", "/");
                            appDataPath = StringUtils.Replace(appDataPath, "//", "/");
                            $callback(appDataPath);
                        } else {
                            $callback("");
                        }
                    }
                });
        }

        public static DownloadSDK($fileSystem : FileSystemHandlerConnector,
                                  $serverUrl : string, $sdkType : string, $platform : PlatformType,
                                  $libsVersion : string) : IFileSystemDownloadPromise {
            let release : string = StringUtils.Replace($sdkType, ".", "-") + "-" + $platform + "/";

            if ($platform === PlatformType.WIN_GCC) {
                release += "static-win-gcc";
            } else if ($platform === PlatformType.LINUX) {
                release += "static-linux-gcc";
            } else if ($platform === PlatformType.IMX) {
                release += "static-linux-arm";
            }

            LogIt.Debug("Download SDK from {0}", $serverUrl + "/Update/io-vision-sdk-studio-libs/" + release + "/" + $libsVersion);
            return $fileSystem.Download(<FileSystemDownloadOptions>{
                url: $serverUrl + "/Update/io-vision-sdk-studio-libs/" + release + "/" + $libsVersion
            });
        }

        constructor() {
            super();
            this.fileSystem = new FileSystemHandlerConnector(true);
            this.terminal = new TerminalConnector(true);
            this.printHandler = ($message : string) : void => {
                Echo.Println($message);
            };
        }

        public setOnError($callback : ($message : string) => void) : ToolchainManager {
            [this.fileSystem, this.terminal].forEach(($connector : BaseConnector) : void => {
                $connector.ErrorPropagation(true);
                $connector.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                    $callback($eventArgs.Message());
                });
            });
            return this;
        }

        public setOnPrint($callback : ($message : string) => void) : ToolchainManager {
            this.printHandler = $callback;
            return this;
        }

        public InstallBuildDependencies($installDao : InstallationRecipeDAO, $msysPath : string, $cmakePath : string,
                                        $callback : ($message : string, $status? : boolean) => void) : void {
            $cmakePath = StringUtils.Remove($cmakePath, "/bin");
            let currentProduct : string;
            let isValidated : boolean = true;
            $installDao.getEvents().setOnChange(($eventArgs : InstallationProgressEventArgs) : void => {
                switch ($eventArgs.ProgressCode()) {
                case InstallationProgressCode.CHAIN_START:
                    currentProduct = StringUtils.Split($eventArgs.Message(), " (")[0];
                    break;
                case InstallationProgressCode.STEP_START:
                    if ($eventArgs.Message() === InstallationProtocolType.INSTALL) {
                        $callback("Installing " + currentProduct);
                        isValidated = false;
                    } else if ($eventArgs.Message() === InstallationProtocolType.UNINSTALL) {
                        $callback("Uninstalling " + currentProduct);
                        isValidated = false;
                    }
                    break;
                case InstallationProgressCode.DOWNLOAD_START:
                    $callback("Downloading " + currentProduct);
                    isValidated = false;
                    break;
                default:
                    if ($eventArgs.CurrentValue() === -1) {
                        this.printHandler($eventArgs.Message());
                    }
                    if (!$eventArgs.Status()) {
                        isValidated = true;
                        $callback("Dependencies install has failed.", false);
                    }
                    break;
                }
            });

            $installDao.getEvents().setOnComplete(() : void => {
                currentProduct = null;
                Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
                    if (currentProduct === null) {
                        if (!isValidated) {
                            $callback("Dependencies installed successfully.", true);
                        } else {
                            $callback("Build toolchain is up to date.", true);
                        }
                    }
                }, 300);
            });

            $installDao.getEvents().setOnError(($eventArgs : ErrorEventArgs) : void => {
                $callback($eventArgs.Message(), false);
            });

            $installDao.getConfigurationInstance().utils.getMsysPath = ($done : IInstallationUtilValuePromise) : void => {
                $done($msysPath);
            };
            $installDao.getConfigurationInstance().utils.getCmakePath = ($done : IInstallationUtilValuePromise) : void => {
                $done($cmakePath);
            };
            $installDao.RunInstallationChain(InstallationProtocolType.INSTALL);
        }
    }
}
