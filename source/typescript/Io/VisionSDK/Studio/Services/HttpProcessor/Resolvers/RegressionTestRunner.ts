/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers {
    "use strict";
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import CppCodeGenerator = Io.VisionSDK.Studio.Services.CodeGenerator.Core.CppCodeGenerator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import VisualGraphDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.VisualGraphDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import TaskManager = Io.VisionSDK.Studio.Services.Utils.TaskManager;
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import EditorPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.EditorPanelViewerArgs;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import VisualGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VisualGraph;
    import APIType = Io.VisionSDK.Studio.Services.Enums.APIType;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;

    export class RegressionTestRunner extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RuntimeTestRunner {
        private fileSystem : FileSystemHandlerConnector;
        private terminal : TerminalConnector;
        private testCaseSourceMap : string;
        private testCaseName : string;
        private goldenData : string[];
        private withCppBuild : boolean;
        private withCppBuildCache : boolean;
        private disableRun : boolean;
        private compilerPath : string;
        private cmakePath : string;
        private sdkPath : string;
        private sdkVersion : string;
        private runtimeVersion : string;
        private platform : PlatformType;
        private agentName : string;
        private hubUrl : string;

        constructor() {
            super();
            this.fileSystem = new FileSystemHandlerConnector();
            this.terminal = new TerminalConnector();
            this.compilerPath = "C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/MSBuild/15.0/Bin";
        }

        public setUseCaseScope($testCaseName : string, $goldenDataFiles : string[], $sourceMap? : string) : void {
            this.testCaseName = Property.String(this.testCaseName, $testCaseName);
            if (ObjectValidator.IsArray($goldenDataFiles)) {
                this.goldenData = $goldenDataFiles;
            }
            this.testCaseSourceMap = Property.String(this.testCaseSourceMap, $sourceMap);
        }

        public setCompilerPath($value : string) : void {
            this.compilerPath = Property.String(this.compilerPath, $value);
        }

        public setCMakePath($value : string) : void {
            this.cmakePath = Property.String(this.cmakePath, $value);
        }

        public setSDKVersion($value : string) : void {
            this.sdkVersion = Property.String(this.sdkVersion, $value);
        }

        public setRuntimeVersion($value : string) : void {
            this.runtimeVersion = Property.String(this.runtimeVersion, $value);
        }

        public setSDKPath($value : string) : void {
            this.sdkPath = Property.String(this.sdkPath, $value);
        }

        public setPlatform($value : PlatformType, $agentName? : string, $hubUrl? : string) : void {
            this.platform = Property.EnumType(this.platform, $value, PlatformType, PlatformType.WIN_GCC);
            this.agentName = Property.String(this.agentName, $agentName);
            this.hubUrl = Property.String(this.hubUrl, $hubUrl);
        }

        public setHubLocation($value : string) : void {
            this.hubUrl = Property.String(this.hubUrl, $value);
        }

        public EnableCppBuild() : void {
            this.withCppBuild = true;
        }

        public DisableCppBuildCache() : void {
            this.withCppBuildCache = false;
        }

        public DisableRun() : void {
            this.disableRun = true;
        }

        protected before() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                if (ObjectValidator.IsEmptyOrNull(this.compilerPath) ||
                    ObjectValidator.IsEmptyOrNull(this.cmakePath)) {
                    this.terminal.Execute("wui", ["--path"]).Then(($exitCode : number, $stdout : string) : void => {
                        if ($exitCode === 0) {
                            const modulesPath : string = StringUtils.Replace((<any>($stdout + "")).trim() + "/external_modules", "\\", "/");
                            if (ObjectValidator.IsEmptyOrNull(this.compilerPath)) {
                                this.compilerPath = modulesPath + "/msys2/mingw64/bin";
                                Echo.Printf("Using detected mingw: {0}", this.compilerPath);
                            }
                            if (ObjectValidator.IsEmptyOrNull(this.cmakePath)) {
                                this.cmakePath = modulesPath + "/cmake/bin";
                                Echo.Printf("Using detected CMake: {0}", this.cmakePath);
                            }
                            $done();
                        } else {
                            Echo.Printf("Unable to find WUI Builder external_modules path used for detection of " +
                                "mingw and CMake!");
                        }
                    });
                } else {
                    $done();
                }
            };
        }

        protected setUp() : void {
            this.goldenData = [];
            this.withCppBuild = false;
            this.withCppBuildCache = true;
            this.sdkPath = "";
            this.sdkVersion = APIType.OVX_1_1_0;
            this.runtimeVersion = "2019.0.1";
            this.platform = PlatformType.WIN_GCC;
            this.agentName = "";
            this.hubUrl = "https://hub.dev.visionstudio.io";
        }

        protected tearDown() : IRuntimeTestPromise {
            if (this.withCppBuild) {
                this.timeoutLimit(-1);
            }
            return ($done : () => void) : void => {
                let targetRoot : string = this.getAbsoluteRoot();
                const testRoot : string = targetRoot + "/test/resource/data/Io/VisionSDK/Studio/Services/CodeGenerator";
                if (this.withCppBuild) {
                    targetRoot = StringUtils.Remove(targetRoot, "/target") + "/runtimeTest_cache";
                }

                const compareNextFile : any = ($index : number) : void => {
                    if ($index < this.goldenData.length) {
                        const file : string = this.goldenData[$index];
                        this.fileSystem.Exists(targetRoot + file).Then(($status : boolean) : void => {
                            if ($status) {
                                this.fileSystem.Exists(testRoot + file).Then(($status : boolean) : void => {
                                    if ($status) {
                                        this.fileSystem.Read(targetRoot + file).Then(($actual : string) : void => {
                                            this.fileSystem.Read(testRoot + file).Then(($expected : string) : void => {
                                                if (!ObjectValidator.IsEmptyOrNull($actual) && !ObjectValidator.IsEmptyOrNull($expected)) {
                                                    this.assertEquals(
                                                        JSON.stringify($actual), JSON.stringify($expected), targetRoot + file);
                                                } else {
                                                    this.assertEquals(
                                                        "", "Not empty file content", targetRoot + file);
                                                }
                                                WindowManager.ScrollToBottom();
                                                compareNextFile($index + 1);
                                            });
                                        });
                                    } else {
                                        this.assertEquals("", testRoot + file, "test data not found");
                                        compareNextFile($index + 1);
                                    }
                                });
                            } else {
                                this.assertEquals("", targetRoot + file, "golden data not found");
                                compareNextFile($index + 1);
                            }
                        });
                    } else {
                        this.testCaseSourceMap = "";
                        $done();
                    }
                };
                const buildGeneratedCode : any = ($dao : MainPageDAO, $targetName : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull(this.sdkPath)) {
                        Echo.Printf("Using test case specified SDK path: {0}", this.sdkPath);
                    }
                    const taskManager : TaskManager = new TaskManager(targetRoot + "/" + this.testCaseName, $targetName, {
                        agentName     : this.agentName,
                        cmakePath     : this.cmakePath,
                        compilerPath  : this.compilerPath,
                        notifications : $dao.getPageConfiguration().notifications,
                        platform      : this.platform,
                        runtimeVersion: this.runtimeVersion,
                        sdkPath       : this.sdkPath,
                        sdkVersion    : this.sdkVersion,
                        serverUrl     : this.hubUrl
                    });

                    const build : any = () : void => {
                        LogIt.Debug("Running build from test runner");
                        taskManager.Build(($message : string, $status : boolean) : void => {
                            if (ObjectValidator.IsSet($status)) {
                                if ($status) {
                                    if (!this.disableRun) {
                                        taskManager.Run(false, ($message : string, $status : boolean) : void => {
                                            if ($status) {
                                                compareNextFile(0);
                                            } else {
                                                Echo.Printf($message);
                                            }
                                        });
                                    } else {
                                        compareNextFile(0);
                                    }
                                } else {
                                    Echo.Printf($message);
                                }
                            } else {
                                Echo.Printf($message);
                            }
                            WindowManager.ScrollToBottom();
                        });
                    };
                    taskManager.Init(() : void => {
                        if (this.platform === PlatformType.LINUX || this.platform === PlatformType.IMX) {
                            taskManager.Synchronize(($message : string, $status : boolean) : void => {
                                if ($status) {
                                    if (!this.withCppBuildCache) {
                                        taskManager.Clean(($status : boolean) : void => {
                                            if ($status) {
                                                build();
                                            } else {
                                                Echo.Printf("Unable to cleanup test case cache!");
                                            }
                                        });
                                    } else {
                                        build();
                                    }
                                } else {
                                    Echo.Printf($message);
                                }
                            });
                        } else {
                            build();
                        }
                    });
                };
                const generateCode : any = ($dao : MainPageDAO, $graph : VisualGraph, $graphDao : VisualGraphDAO) : void => {
                    const generator : CppCodeGenerator = new CppCodeGenerator($graph, $graphDao, this.platform);
                    generator.ProjectDirectory(targetRoot + "/" + this.testCaseName);
                    generator.GenerateCode(($status : boolean) : void => {
                        if ($status) {
                            this.fileSystem.Copy(
                                testRoot + "/" + this.testCaseName + "/data/input",
                                targetRoot + "/" + this.testCaseName + "/data/input")
                                .Then(($status : boolean) : void => {
                                    if ($status) {
                                        Echo.Printf("Source code generated successfully.");
                                        if (this.withCppBuild) {
                                            Echo.Printf("Starting build...");
                                            buildGeneratedCode($dao, $graph.Name());
                                        } else {
                                            compareNextFile(0);
                                        }
                                    } else {
                                        Echo.Printf("Unable to copy test case input data!");
                                    }
                                });
                        } else {
                            Echo.Printf("Unable to generate files!");
                        }
                    });
                };
                const runTestCase : any = () : void => {
                    if (ObjectValidator.IsEmptyOrNull(this.testCaseSourceMap)) {
                        Echo.Printf("Test case source map must be defined!", this.testCaseSourceMap);
                    } else {
                        const validateData : any = ($dao : MainPageDAO) : void => {
                            const graphDao : VisualGraphDAO = $dao.getVisualGraphDAO();
                            this.fileSystem
                                .Write(targetRoot + "/" + this.testCaseName + "/" + this.testCaseSourceMap, graphDao.ToJsonp())
                                .Then(($status : boolean) : void => {
                                    if ($status) {
                                        generateCode($dao, $dao.getVisualGraph(), graphDao);
                                    } else {
                                        Echo.Printf("Unable to reconstruct {0} file!", this.testCaseSourceMap);
                                    }
                                });
                        };
                        const errorHandler : any = ($eventArgs : ErrorEventArgs) : void => {
                            Echo.Println($eventArgs.Exception().Stack() + StringUtils.NewLine());
                            LogIt.Error($eventArgs.Exception().Stack());
                        };
                        const dao : MainPageDAO = new MainPageDAO();
                        dao.getVisualGraphEvents().setOnError(errorHandler);
                        dao.Load(LanguageType.EN, () : void => {
                            dao.LoadProject("file:///" + testRoot + "/" + this.testCaseName + "/" + this.testCaseSourceMap,
                                () : void => {
                                    const args : EditorPanelViewerArgs = dao.getModelArgs().EditorPanelViewerArgs();
                                    const root : IKernelNode = args.VisualGraphPanelArgs().GraphRoot();
                                    dao.FromGraphRoot(root);
                                    validateData(dao);
                                });
                        }, true);
                    }
                };
                if (this.withCppBuild) {
                    if (!this.withCppBuildCache && this.platform !== PlatformType.LINUX && this.platform !== PlatformType.IMX) {
                        this.fileSystem.Delete(targetRoot + "/" + this.testCaseName)
                            .Then(($status : boolean) : void => {
                                if ($status) {
                                    runTestCase();
                                } else {
                                    Echo.Printf("Unable to cleanup test case cache!");
                                }
                            });
                    } else {
                        runTestCase();
                    }
                } else {
                    runTestCase();
                }
            };
        }
    }
}
/* dev:end */
