/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers {
    "use strict";
    import ReportServiceConnector = Com.Wui.Framework.Services.Connectors.ReportServiceConnector;
    import IReportProtocol = Com.Wui.Framework.Services.Connectors.IReportProtocol;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import NotificationMessageType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.NotificationMessageType;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import MainPageController = Io.VisionSDK.Studio.Services.Controllers.Pages.MainPageController;
    import AsyncHttpResolver = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.AsyncHttpResolver;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;

    export class ErrorResolver extends AsyncHttpResolver {
        private static reportTimeout : any;
        private static exceptionNum : number = 0;

        constructor() {
            super();
            ErrorResolver.exceptionNum++;
        }

        public resolver() : void {
            if (ObjectValidator.IsEmptyOrNull(ErrorResolver.reportTimeout)) {
                MainPageController.AddNotification("Error occurred - reporting issue ...", NotificationMessageType.GENERAL_ERROR);
            }

            const stopTimeout : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(ErrorResolver.reportTimeout)) {
                    clearTimeout(ErrorResolver.reportTimeout);
                }
            };

            const resetTimeout : any = () : void => {
                stopTimeout();
                ErrorResolver.reportTimeout = setTimeout(() : void => {
                    sendReport();
                }, 3000);
            };

            let trace : string = "";
            if (this.RequestArgs().POST().KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
                const exceptions : ArrayList<Exception> =
                    this.RequestArgs().POST().getItem(HttpRequestConstants.EXCEPTIONS_LIST);
                try {
                    trace = exceptions.getFirst().ToString("", false);
                } catch (ex) {
                    // ignore processing of stack trace, if convert to string is not possible
                }
            }
            const environment : EnvironmentArgs = this.getEnvironmentArgs();
            const data : IReportProtocol = {
                appId      : StringUtils.getSha1(environment.getAppName() + environment.getProjectVersion()),
                appName    : environment.getAppName(),
                appVersion : environment.getProjectVersion(),
                log        : Loader.getInstance().ApplicationLog(),
                printScreen: "",
                timeStamp  : new Date().getTime(),
                trace
            };

            const notifyReportSendFailure : any = () : any => {
                MainPageController.AddNotification("Error report has failed to send.", NotificationMessageType.GENERAL_ERROR);
                ErrorResolver.reportTimeout = null;
                ErrorResolver.exceptionNum = 0;
            };

            const detectFatalError : any = () : void => {
                stopTimeout();
                ErrorResolver.reportTimeout = null;
                if (ErrorResolver.exceptionNum >= 10) {
                    MainPageController.AddNotification("Fatal error detected - processing application restore.",
                        NotificationMessageType.GENERAL_WARNING);
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.getHttpManager().Reload();
                    }, 2000);
                } else {
                    ErrorResolver.exceptionNum = 0;
                    if (!environment.IsProductionMode()) {
                        MainPageController.AddNotification("Test environment detected - report sending skipped.",
                            NotificationMessageType.GENERAL_INFO);
                        MainPageController.WriteToConsole(trace, true);
                    }
                }
            };

            const sendReport : any = () : void => {
                if (environment.IsProductionMode()) {
                    if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().ApplicationLog())) {
                        this.getHttpManager().IsOnline(($status : boolean) : void => {
                            if ($status) {
                                const connector : ReportServiceConnector = new ReportServiceConnector();
                                connector.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                                    try {
                                        LogIt.Error($eventArgs.ToString("", false));
                                    } catch (ex) {
                                        LogIt.Debug("Unable to consume Report service connector error.", ex);
                                    }
                                    notifyReportSendFailure();
                                });
                                connector.getEvents().OnTimeout(() : void => {
                                    LogIt.Error("Report service connector is unreachable.");
                                    notifyReportSendFailure();
                                });
                                connector
                                    .CreateReport(data)
                                    .Then(($success : boolean) : void => {
                                        if ($success) {
                                            Loader.getInstance().ApplicationLog("");
                                            MainPageController.AddNotification("Error report has been sent successfully.",
                                                NotificationMessageType.GENERAL_SUCCESS);
                                            detectFatalError();
                                        } else {
                                            notifyReportSendFailure();
                                        }
                                    });
                            } else {
                                notifyReportSendFailure();
                            }
                        });
                    } else {
                        detectFatalError();
                    }
                } else {
                    detectFatalError();
                }
            };

            if (ErrorResolver.exceptionNum < 10) {
                const fileSystem : FileSystemHandlerConnector = new FileSystemHandlerConnector();
                fileSystem.getEvents().OnError(sendReport);
                fileSystem.getEvents().OnTimeout(sendReport);

                const today : Date = new Date();
                const year : string = today.getFullYear() + "";
                const monthNumber : number = today.getMonth() + 1;
                const month : string = monthNumber < 10 ? "0" + monthNumber : monthNumber + "";
                const day : string = today.getDate() < 10 ? "0" + today.getDate() : today.getDate() + "";
                const logFile : string = "log/" + year + "/" + month + "/" + day + "_" + month + "_" + year + ".txt";
                fileSystem.getWuiHostLocation().Then(($path : string) : void => {
                    fileSystem.Read($path + "/" + logFile).Then(($data : string) : void => {
                        const currentLength : number = StringUtils.Length($data);
                        if (currentLength > 0) {
                            const maxLength : number = 1024 * 10;
                            if (currentLength > maxLength) {
                                $data = StringUtils.Substring($data, currentLength - maxLength);
                            }
                            data.log += "\r\n-------------------- WUI Connector --------------------\r\n\r\n" + $data;
                        }
                        resetTimeout();
                    });
                });
            } else {
                sendReport();
            }
        }
    }
}
