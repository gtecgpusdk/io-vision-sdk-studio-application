/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BasePageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.BasePageDAO;
    import Icon = Io.VisionSDK.UserControls.BaseInterface.UserControls.Icon;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import ImageButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.ImageButton;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import DragBar = Com.Wui.Framework.UserControls.BaseInterface.Components.DragBar;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;

    /**
     * BasePageController class provides handling of requests for controller.
     */
    export class BasePageController extends Com.Wui.Framework.Services.HttpProcessor.Resolvers.BasePageController {

        constructor() {
            super();

            this.setPageTitle("Vision SDK Studio v" + this.getEnvironmentArgs().getProjectVersion());
            this.setPageIconSource("resource/graphics/VisionSdk.ico");
            this.loaderIcon.IconType(IconType.SPINNER_TRANSPARENT_BIG);
            this.appTitle.Visible(false);
            this.minimizeButton.GuiType(ImageButtonType.APP_MINIMIZE);
            this.maximizeButton.GuiType(ImageButtonType.APP_MAXIMIZE);
            this.closeButton.GuiType(ImageButtonType.APP_CLOSE);
            this.setResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
        }

        protected setDao($value : BasePageDAO) : void {
            super.setDao($value);
        }

        protected getDao() : BasePageDAO {
            return <BasePageDAO>super.getDao();
        }

        protected getCssInterfaceName() : string {
            return StringUtils.Remove(BasePageController.ClassName(), ".") + " " +
                StringUtils.Remove(Com.Wui.Framework.Services.HttpProcessor.Resolvers.BasePageController.ClassName(), ".");
        }

        protected getLoaderIconClass() : any {
            return Icon;
        }

        protected getLoaderTextClass() : any {
            return Label;
        }

        protected getAppImageButtonClass() : any {
            return ImageButton;
        }

        protected getAppIconClass() : any {
            return Icon;
        }

        protected getAppTitleTextClass() : any {
            return Label;
        }

        protected getAppResizeBarClass() : any {
            return ResizeBar;
        }

        protected getAppDragBarClass() : any {
            return DragBar;
        }

        protected getEventsManager() : EventsManager {
            return <EventsManager>super.getEventsManager();
        }
    }
}
