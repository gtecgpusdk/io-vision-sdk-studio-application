/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers {
    "use strict";
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;

    /**
     * @typedef {Object} RuntimeTestRunner
     */

    /**
     * @class RuntimeTestRunner
     * @classdesc RuntimeTestRunner class provides handling of unit tests at runtime.
     * @memberOf Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers
     */
    export abstract class RuntimeTestRunner extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        constructor() {
            super();
            StaticPageContentManager.FaviconSource("resource/graphics/Io/VisionSDK/Studio/Services/Generator.ico");
            StaticPageContentManager.Title("VisionSDK - Unit Test");
        }

        protected resolver() : void {
            super.resolver();
            StaticPageContentManager.Title("VisionSDK - Unit Test");
        }
    }
}
