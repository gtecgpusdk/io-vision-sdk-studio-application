/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import StaticPageContentManger = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;

    export class AboutPackage extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            this.registerResolver("/about/Wui/Commons", Com.Wui.Framework.Commons.Index);
            this.registerResolver("/about/Wui/Gui", Com.Wui.Framework.Gui.Index);
            this.registerResolver("/about/Wui/Services", Com.Wui.Framework.Services.Index);

            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>GPU SDK Services Library</h1>" +
                "<div class=\"Index\">";

            output +=
                "<H3>Library dependencies</H3>" +
                "<a href=\"#" + this.createLink("/about/Wui/Commons") + "\">WUI Commons library</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + this.createLink("/about/Wui/Gui") + "\">WUI Gui library</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + this.createLink("/about/Wui/Services") + "\">WUI Services library</a>" +
                "<H3>Runtime tests</H3>" +
                StringUtils.NewLine() +
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + StringUtils.NewLine(false) +
                "<div class=\"Logo\">" + StringUtils.NewLine(false) +
                "   <div class=\"WUI\"></div>" + StringUtils.NewLine(false) +
                "</div>";

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("Vision SDK Studio - Package Info");
            StaticPageContentManger.BodyAppend(output);
            StaticPageContentManger.Draw();
        }
    }
}
