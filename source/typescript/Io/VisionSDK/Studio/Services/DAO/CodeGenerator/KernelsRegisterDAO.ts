/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.DAO.CodeGenerator {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IKernelsRegister = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IKernelsRegister;
    import IKernelCodeAttributes = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IKernelCodeAttributes;
    import IKernelsCodeProject = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IKernelsCodeProject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class KernelsRegisterDAO extends BaseGeneratorDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/VisionSDK/Studio/Services/Configuration/KernelsRegister.jsonp";
        private printHandler : ($message : string) => void;

        constructor() {
            super();
            this.printHandler = ($message : string) : void => {
                Echo.Println($message);
            };
        }

        public setPrintHandler($handler : ($message : string) => void) : void {
            if (ObjectValidator.IsFunction($handler)) {
                this.printHandler = $handler;
            }
        }

        public getStaticConfiguration() : IKernelsRegister {
            return <IKernelsRegister>super.getStaticConfiguration();
        }

        public getCompanies() : string[] {
            const companies : string[] = [];
            this.getStaticConfiguration().projects.forEach(($project : IKernelsCodeProject) : void => {
                if (companies.indexOf($project.companyId) === -1) {
                    companies.push($project.companyId);
                }
            });
            return companies;
        }

        public getProjects($companyId : string) : ArrayList<IKernelsCodeProject> {
            const projects : ArrayList<IKernelsCodeProject> = new ArrayList<IKernelsCodeProject>();
            let index : number = 0;
            this.getStaticConfiguration().projects.forEach(($project : IKernelsCodeProject) : void => {
                if ($project.companyId === $companyId) {
                    $project.index = index;
                    projects.Add($project, $project.name);
                    index++;
                }
            });
            return projects;
        }

        public getKernels($companyId : string, $projectName : string) : ArrayList<IKernelCodeAttributes> {
            const kernels : ArrayList<IKernelCodeAttributes> = new ArrayList<IKernelCodeAttributes>();
            this.getProjects($companyId).foreach(($project : IKernelsCodeProject) : void => {
                if ($project.name === $projectName) {
                    $project.kernels.forEach(($kernel : IKernelCodeAttributes) : void => {
                        kernels.Add($kernel, $kernel.name);
                    });
                }
            });
            return kernels;
        }

        public AddKernels($companyId : string, $projectName : string, $kernels : IKernelCodeAttributes[]) : void {
            const register : ArrayList<IKernelCodeAttributes> = this.getKernels($companyId, $projectName);
            $kernels.forEach(($kernel : IKernelCodeAttributes) : void => {
                if (!ObjectValidator.IsEmptyOrNull($kernel.name) && !ObjectValidator.IsEmptyOrNull($kernel.namespace)) {
                    $kernel.companyId = $companyId;
                    $kernel.target = $projectName;
                    if (!ObjectValidator.IsSet($kernel.description)) {
                        $kernel.description = "";
                    }
                    if (!ObjectValidator.IsSet($kernel.params)) {
                        $kernel.params = [];
                    }
                    register.Add($kernel, $kernel.name);
                }
            });
            this.getProjects($companyId).getItem($projectName).kernels = register.getAll();
        }

        public ValidateKernel($kernel : IKernelCodeAttributes) : boolean {
            const projects : ArrayList<IKernelsCodeProject> = this.getProjects($kernel.companyId);
            if (!ObjectValidator.IsEmptyOrNull(projects)) {
                if (!projects.KeyExists($kernel.target)) {
                    this.printHandler(StringUtils.Format("Project {0}[{1}] has not been found in register.",
                        $kernel.target, $kernel.companyId));
                    return false;
                } else {
                    if (projects.Length() + 1 > 255) {
                        this.printHandler("Maximal count of projects for single company has been reached.");
                        return false;
                    } else if (this.getKernels($kernel.companyId, $kernel.target).Length() + 1 > 4096) {
                        this.printHandler("Maximal count of kernels for single project has been reached.");
                        return false;
                    } else if (StringUtils.ContainsIgnoreCase($kernel.name, "Kernel", "Node")) {
                        this.printHandler("Reserved identifier \"Kernel\" or \"Node\" has been found in kernel name.");
                        return false;
                    }
                }
            } else {
                this.printHandler(StringUtils.Format("CompanyID {0} has not been found in register.", $kernel.companyId));
                return false;
            }
            return true;
        }
    }
}
