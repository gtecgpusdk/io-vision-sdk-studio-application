/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.DAO.Pages {
    "use strict";
    import BaseDAO = Com.Wui.Framework.Services.DAO.BaseDAO;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IAppWizardLocalization = Io.VisionSDK.Studio.Services.Interfaces.DAO.IAppWizardLocalization;

    export class AppWizardDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/VisionSDK/Studio/Services/Localization/AppWizardLocalization.jsonp";

        protected static getDaoInterfaceClassName($interfaceName : string) : any {
            switch ($interfaceName) {
            case "IAppWizardConfiguration":
                return AppWizardDAO;
            default:
                break;
            }
            return BaseDAO;
        }

        public getStaticConfiguration() : IAppWizardLocalization {
            return <IAppWizardLocalization>super.getStaticConfiguration();
        }
    }
}
