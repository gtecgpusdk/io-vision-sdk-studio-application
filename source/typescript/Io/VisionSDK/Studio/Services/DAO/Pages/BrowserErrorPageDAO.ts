/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.DAO.Pages {
    "use strict";
    import IBrowserErrorPageLocalization = Io.VisionSDK.Studio.Services.Interfaces.DAO.IBrowserErrorPageLocalization;

    export class BrowserErrorPageDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/VisionSDK/Studio/Services/Localization/BrowserErrorPageLocalization.jsonp";

        public getStaticConfiguration() : IBrowserErrorPageLocalization {
            return <IBrowserErrorPageLocalization>super.getStaticConfiguration();
        }
    }
}
