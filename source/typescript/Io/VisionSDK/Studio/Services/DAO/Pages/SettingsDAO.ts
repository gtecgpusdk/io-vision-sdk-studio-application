/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.DAO.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import SettingsPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.SettingsPanelViewerArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ToolchainSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ToolchainSettingsPanelViewerArgs;
    import AboutSettingsPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.AboutSettingsPanelViewerArgs;
    import ISettingLocalization = Io.VisionSDK.Studio.Services.Interfaces.DAO.ISettingLocalization;
    import ConnectionSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ConnectionSettingsPanelViewerArgs;
    import ProjectSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ProjectSettingsPanelViewerArgs;

    export class SettingsDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/VisionSDK/Studio/Services/Localization/SettingsLocalization.jsonp";
        protected modelArgs : SettingsPanelViewerArgs;

        constructor() {
            super();
        }

        public getPageConfiguration() : ISettingLocalization {
            return <ISettingLocalization>super.getStaticConfiguration();
        }

        public getModelArgs() : SettingsPanelViewerArgs {
            if (!ObjectValidator.IsSet(this.modelArgs)) {
                const args : SettingsPanelViewerArgs = new SettingsPanelViewerArgs();
                const pageConfiguration : ISettingLocalization = this.getPageConfiguration();
                args.BackText(pageConfiguration.back);
                args.MenuHeaderText(pageConfiguration.menuHeader);
                args.DescriptionHeaderText(pageConfiguration.descriptionHeader);
                args.SaveText(pageConfiguration.save);
                args.CancelText(pageConfiguration.cancel);
                args.AboutText(pageConfiguration.about);
                args.ConnectionText(pageConfiguration.connection);
                args.ProjectText(pageConfiguration.project);
                args.ToolchainText(pageConfiguration.toolchain);

                const projectArgs : ProjectSettingsPanelViewerArgs = args.ProjectSettingsPanelArgs();
                projectArgs.HeaderText(pageConfiguration.projectSettingsPanel.header);
                projectArgs.VxVersionText(pageConfiguration.projectSettingsPanel.vxVersion);
                projectArgs.DescriptionText(pageConfiguration.projectSettingsPanel.description);
                projectArgs.PathText(pageConfiguration.projectSettingsPanel.path);
                projectArgs.NameText(pageConfiguration.projectSettingsPanel.name);
                projectArgs.PanelDescription(pageConfiguration.projectSettingsPanel.panelDescription);
                pageConfiguration.projectSettingsPanel.vxVersionOptions.forEach(($version : string) : void => {
                    projectArgs.AddVxVersion($version);
                });

                const connectionArgs : ConnectionSettingsPanelViewerArgs = args.ConnectionSettingsPanelArgs();
                connectionArgs.HeaderText(pageConfiguration.connectionSettingsPanel.header);
                connectionArgs.NameText(pageConfiguration.connectionSettingsPanel.name);
                connectionArgs.AddressText(pageConfiguration.connectionSettingsPanel.address);
                connectionArgs.UsernameText(pageConfiguration.connectionSettingsPanel.username);
                connectionArgs.PasswordText(pageConfiguration.connectionSettingsPanel.password);
                connectionArgs.PlatformText(pageConfiguration.connectionSettingsPanel.platform);
                connectionArgs.AgentNameText(pageConfiguration.connectionSettingsPanel.agentName);
                connectionArgs.PanelDescription(pageConfiguration.connectionSettingsPanel.panelDescription);
                connectionArgs.Value(pageConfiguration.values.connectionSettings);

                const toolchainArgs : ToolchainSettingsPanelViewerArgs = args.ToolchainSettingsPanelArgs();
                toolchainArgs.HeaderText(pageConfiguration.toolchainSettingsPanel.header);
                toolchainArgs.NameText(pageConfiguration.toolchainSettingsPanel.name);
                toolchainArgs.CmakeText(pageConfiguration.toolchainSettingsPanel.cmakePath);
                toolchainArgs.CppText(pageConfiguration.toolchainSettingsPanel.cppPath);
                toolchainArgs.LinkerText(pageConfiguration.toolchainSettingsPanel.linker);
                toolchainArgs.ValidateButtontext(pageConfiguration.toolchainSettingsPanel.validateButton);
                toolchainArgs.PanelDescription(pageConfiguration.toolchainSettingsPanel.panelDescription);
                toolchainArgs.Value(pageConfiguration.values.toolchainSettings);

                const aboutArgs : AboutSettingsPanelViewerArgs = args.AboutPanelArgs();
                aboutArgs.HeaderText(pageConfiguration.aboutPanel.header);
                aboutArgs.VersionText(pageConfiguration.aboutPanel.versionLabel);
                aboutArgs.VersionContentsText(pageConfiguration.aboutPanel.versionValue);
                aboutArgs.TextContentsText(pageConfiguration.aboutPanel.aboutText);
                aboutArgs.PanelDescription(pageConfiguration.aboutPanel.panelDescription);

                return this.modelArgs = args;
            }
            return <SettingsPanelViewerArgs>this.modelArgs;
        }
    }
}
