/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.DAO.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IMainPageLocalization = Io.VisionSDK.Studio.Services.Interfaces.DAO.IMainPageLocalization;
    import EditorPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.EditorPanelViewerArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import VisualGraphPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.VisualGraphPanelViewerArgs;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import VisualGraphDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.VisualGraphDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import VisualGraphNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VisualGraphNode;
    import VisualGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VisualGraph;
    import PropertiesPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.PropertiesPanelViewerArgs;
    import VXContext = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXContext;
    import VXGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXGraph;
    import VXNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXNode;
    import IVisualGraph = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVisualGraph;
    import IVXNodeTemplates = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeTemplates;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IVXNodeTemplate = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeTemplate;
    import IVisualGraphDAOEvents = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.IVisualGraphDAOEvents;
    import IVXNodeAttribute = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeAttribute;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import DirectoryBrowserPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.DirectoryBrowserPanelViewerArgs;
    import KernelNodeHelpPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.KernelNodeHelpPanelViewerArgs;
    import EditorPageViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.EditorPageViewerArgs;
    import AppWizardPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.AppWizardPanelViewerArgs;
    import IAppWizardLocalization = Io.VisionSDK.Studio.Services.Interfaces.DAO.IAppWizardLocalization;
    import IAppWizardItem = Io.VisionSDK.Studio.Services.Interfaces.DAO.IAppWizardItem;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ToolbarPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.ToolbarPanelViewerArgs;
    import KernelNodesPickerPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.KernelNodesPickerPanelViewerArgs;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import PersistenceFactory = Com.Wui.Framework.Gui.PersistenceFactory;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import InstallationRecipeDAO = Com.Wui.Framework.Services.DAO.InstallationRecipeDAO;
    import ProjectPickerPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ProjectPickerPanelViewerArgs;
    import IDefaultPageDescriptor = Io.VisionSDK.Studio.Gui.Interfaces.IDefaultPageDescriptor;
    import APIType = Io.VisionSDK.Studio.Services.Enums.APIType;
    import IVXNodeModel = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeModel;
    import IVXNode = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNode;
    import ICorrectAttributesStatus = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.ICorrectAttributesStatus;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import ExportPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ExportPanelViewerArgs;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import IPropagableNumberValue = Com.Wui.Framework.Gui.Structures.IPropagableNumberValue;
    import VxNodesGroupType = Io.VisionSDK.Studio.Services.Enums.VxNodesGroupType;
    import ProjectCreationPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ProjectCreationPanelViewerArgs;
    import ProjectSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ProjectSettingsPanelViewerArgs;
    import IConnectionSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IConnectionSettingsValue;
    import IBuildPlatform = Io.VisionSDK.Studio.Gui.Interfaces.IBuildPlatform;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import Output = Io.VisionSDK.UserControls.Interfaces.Output;
    import Input = Io.VisionSDK.UserControls.Interfaces.Input;
    import IKernelNodeAttribute = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttribute;
    import IConnectionValidatorStatus = Io.VisionSDK.UserControls.Interfaces.IConnectionValidatorStatus;
    import IBoundingRectangle = Io.VisionSDK.Studio.Gui.Interfaces.IBoundingRectangle;
    import INodeRestrictions = Io.VisionSDK.Studio.Services.Interfaces.INodeRestrictions;

    export class MainPageDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/VisionSDK/Studio/Services/Localization/MainPageLocalization.jsonp";
        private graphDao : VisualGraphDAO;
        private graphNodesMap : VisualGraphNode[];
        private kernelNodesMap : IKernelNode[];
        private visualGraph : VisualGraph;
        private readonly projectsList : ArrayList<VisualGraphDAO>;
        private appWizardDao : AppWizardDAO;
        private readonly installDao : InstallationRecipeDAO;
        private fileSystem : FileSystemHandlerConnector;
        private readonly settingsDao : SettingsDAO;
        private vxVersion : string;

        constructor() {
            super();
            this.graphDao = new VisualGraphDAO();
            this.projectsList = new ArrayList<VisualGraphDAO>();
            this.appWizardDao = new AppWizardDAO();
            this.fileSystem = new FileSystemHandlerConnector(true);
            this.installDao = new InstallationRecipeDAO();
            this.installDao
                .setConfigurationPath("resource/data/Io/VisionSDK/Studio/Services/Configuration/DependencyInstall.jsonp");
            this.settingsDao = new SettingsDAO();
            this.vxVersion = APIType.OVX_1_2_0;
        }

        public CorrectAttributes($node : IKernelNode, $attribute : IKernelNodeAttribute) : ICorrectAttributesStatus {
            const vxNode : VXNode = <VXNode>this.graphNodesMap[this.kernelNodesMap.indexOf($node)];
            if (!ObjectValidator.IsEmptyOrNull(vxNode)) {
                const status : ICorrectAttributesStatus = vxNode.ToModel().correctAttributes(vxNode.getAttribute($attribute.name), true);
                $node.Help(vxNode.getHelp());
                this.overwriteFrontendAttributes(vxNode, $node);
                return status;
            }
            return {
                corrected: []
            };
        }

        public Validator($node1 : IKernelNode, $node2 : IKernelNode,
                         $outputIndex : number, $inputIndex : number, $correctAttributes? : boolean,
                         $applyChanges? : boolean) : IConnectionValidatorStatus {
            const vxNode1 : VXNode = <VXNode>this.graphNodesMap[this.kernelNodesMap.indexOf($node1)];
            const vxNode2 : VXNode = <VXNode>this.graphNodesMap[this.kernelNodesMap.indexOf($node2)];
            const status : IConnectionValidatorStatus
                = vxNode1.ToModel().validator(<any>vxNode2, $outputIndex, $inputIndex, $correctAttributes, $applyChanges);

            status.sourceNode = $node1;
            status.sourceIndex = $outputIndex;
            status.targetNode = $node2;
            status.targetIndex = $inputIndex;

            if ($correctAttributes && $applyChanges) {
                this.overwriteFrontendAttributes(vxNode1, $node1);
                this.overwriteFrontendAttributes(vxNode2, $node2);
            }
            return status;
        }

        public getPageConfiguration() : IMainPageLocalization {
            return <IMainPageLocalization>super.getStaticConfiguration();
        }

        public getModelArgs() : EditorPageViewerArgs {
            if (!ObjectValidator.IsSet(this.modelArgs)) {
                const pageConfiguration : IMainPageLocalization = this.getPageConfiguration();
                const args : EditorPageViewerArgs = new EditorPageViewerArgs();
                this.modelArgs = args;

                const editorArgs : EditorPanelViewerArgs = args.EditorPanelViewerArgs();

                const toolbarArgs : ToolbarPanelViewerArgs = editorArgs.ToolbarArgs();
                toolbarArgs.ProjectMenuText(pageConfiguration.toolBarPanel.projectMenu);
                toolbarArgs.SaveButtonText(pageConfiguration.toolBarPanel.save);
                toolbarArgs.UndoButtonText(pageConfiguration.toolBarPanel.undo);
                toolbarArgs.RedoButtonText(pageConfiguration.toolBarPanel.redo);
                toolbarArgs.UpdateButtonText(pageConfiguration.toolBarPanel.update);
                toolbarArgs.UpdateButtonHint(pageConfiguration.toolBarPanel.updateHint);
                toolbarArgs.RunButtonText(pageConfiguration.toolBarPanel.buildBar.run);
                toolbarArgs.OpenButtonText(pageConfiguration.toolBarPanel.open);
                toolbarArgs.ConsoleButtonText(pageConfiguration.toolBarPanel.console);
                toolbarArgs.HelpButtonText(pageConfiguration.toolBarPanel.help);
                toolbarArgs.ConfigureBuildText(pageConfiguration.toolBarPanel.buildBar.configureBuild);

                const examplesProjectPickerArgs : ProjectPickerPanelViewerArgs = args.ProjectPickerDialogArgs();
                examplesProjectPickerArgs.HeaderText(pageConfiguration.examplesProjectPickerDialog.header);
                examplesProjectPickerArgs.LoadButtonText(pageConfiguration.examplesProjectPickerDialog.load);

                const defaultPage : IDefaultPageDescriptor = new IDefaultPageDescriptor();
                defaultPage.header = pageConfiguration.examplesProjectPickerDialog.defaultPageHeader;
                defaultPage.description = pageConfiguration.examplesProjectPickerDialog.defaultPageDescription;

                const exportArgs : ExportPanelViewerArgs = args.ExportDialogArgs();
                exportArgs.HeaderText(pageConfiguration.exportDialog.header);
                exportArgs.ExportButtonText(pageConfiguration.exportDialog.exportButton);
                exportArgs.CloseButtonText(pageConfiguration.exportDialog.close);
                exportArgs.PlatformText(pageConfiguration.exportDialog.targetPlatform);
                exportArgs.ExportPathText(pageConfiguration.exportDialog.exportPath);
                exportArgs.AddPlatform(pageConfiguration.exportDialog.platforms.winGCC, PlatformType.WIN_GCC);
                exportArgs.AddPlatform(pageConfiguration.exportDialog.platforms.linux, PlatformType.LINUX);
                exportArgs.AddPlatform(pageConfiguration.exportDialog.platforms.imx, PlatformType.IMX);

                const projectCreationArgs : ProjectCreationPanelViewerArgs = args.ProjectCreationDialogArgs();
                const projectSettingsArgs : ProjectSettingsPanelViewerArgs = projectCreationArgs.SettingsArgs();
                projectSettingsArgs.HeaderText(pageConfiguration.projectCreationPanel.header);
                projectSettingsArgs.VxVersionText(pageConfiguration.projectCreationPanel.vxVersion);
                projectSettingsArgs.DescriptionText(pageConfiguration.projectCreationPanel.description);
                projectSettingsArgs.PathText(pageConfiguration.projectCreationPanel.path);
                projectSettingsArgs.NameText(pageConfiguration.projectCreationPanel.name);

                pageConfiguration.projectCreationPanel.vxVersionOptions.forEach(($version : string) : void => {
                    projectSettingsArgs.AddVxVersion($version);
                });
                projectCreationArgs.CreateText(pageConfiguration.projectCreationPanel.create);
                args.SettingsPanelArgs(this.settingsDao.getModelArgs());

                const platforms : IBuildPlatform[] = [];
                [
                    args.SettingsPanelArgs().ConnectionSettingsPanelArgs().Value().options,
                    args.SettingsPanelArgs().ToolchainSettingsPanelArgs().Value().options
                ].forEach(($options : IConnectionSettingsValue[]) : void => {
                    $options.forEach(($value : IConnectionSettingsValue) : void => {
                        if (!ObjectValidator.IsSet($value.enabled) || $value.enabled) {
                            platforms.push({text: $value.name, value: $value.name});
                        }
                    });
                });
                toolbarArgs.BuildPlatforms(platforms);

                editorArgs.VisualGraphPanelArgs(this.getVisualGraphArgs());
                editorArgs.VisualGraphPanelArgs().NodeNotifications(pageConfiguration.kernelNodeNotifications);

                const settingsPanelArgs : ProjectSettingsPanelViewerArgs
                    = this.getModelArgs().SettingsPanelArgs(this.settingsDao.getModelArgs()).ProjectSettingsPanelArgs();

                settingsPanelArgs.Value({
                    description: this.visualGraph.Description(),
                    name       : this.visualGraph.Name(),
                    path       : this.getVisualGraphPath(),
                    vxVersion  : this.getTemplates().version
                });

                const propertiesArgs : PropertiesPanelViewerArgs = editorArgs.DetailsPanelArgs().PropertiesPanelArgs();
                propertiesArgs.NameText(pageConfiguration.propertiesPanel.name);
                propertiesArgs.TypeText(pageConfiguration.propertiesPanel.type);

                this.vxVersion = this.getTemplates().version;
                this.loadNodes();

                const browserDialogArgs : DirectoryBrowserPanelViewerArgs = args.DirectoryBrowserDialogArgs();
                browserDialogArgs.CloseButtonText(pageConfiguration.directoryBrowserDialog.close);
                browserDialogArgs.SaveButtonText(pageConfiguration.directoryBrowserDialog.save);

                const kernelNodeHelpDialogArgs : KernelNodeHelpPanelViewerArgs = args.KernelNodeHelpDialogArgs();
                kernelNodeHelpDialogArgs.HeaderText(pageConfiguration.kernelNodeHelpDialog.header);
                kernelNodeHelpDialogArgs.DescriptionText(pageConfiguration.kernelNodeHelpDialog.description);
                kernelNodeHelpDialogArgs.LinkText(pageConfiguration.kernelNodeHelpDialog.link);
            }
            return <EditorPageViewerArgs>this.modelArgs;
        }

        public FromGraphRoot($graphRoot : IKernelNode, $modelChanged : boolean = true) : void {
            if ($modelChanged) {
                this.kernelNodesMap = [];
                this.graphNodesMap = [];
                this.visualGraph.getIoComs().Clear();
                this.visualGraph.getVxContexts().Clear();
                const index : any = {
                    context: 0,
                    graph  : 0,
                    ioCom  : 0,
                    kernel : 0,
                    node   : 0
                };

                this.visualGraph.GridDimX($graphRoot.GridSize().Width());
                this.visualGraph.GridDimY($graphRoot.GridSize().Height());

                const createNode : any = ($kernelNode : KernelNode, $parent : VXGraph) : void => {
                    const model : VXNode = new VXNode();
                    model.Type($kernelNode.Type());
                    model.Configuration(this.visualGraph.Configuration());
                    this.graphNodesMap.push(model);
                    if (this.getTemplates().vxIoComs.hasOwnProperty(model.Type())) {
                        model.Index(index.ioCom);
                        model.Parent($parent.Parent().Parent());
                        this.visualGraph.getIoComs().Add(model);
                        if (!model.getOwners().Contains($parent)) {
                            model.getOwners().Add($parent);
                        }
                        index.ioCom++;
                    } else if (this.getTemplates().vxNodes.hasOwnProperty(model.Type())) {
                        model.Index(index.node);
                        model.Parent($parent);
                        $parent.getVxNodes().Add(model);
                        if (!model.getOwners().Contains($parent)) {
                            model.getOwners().Add($parent);
                        }
                        index.node++;
                    }
                    const nodeData : IVXNodeModel = model.ToModel();
                    nodeData.attributes();

                    nodeData.inputs().forEach(($input : IVXNode) : void => {
                        (<any>$input).Parent($parent);
                        model.getInputs().Add((<any>$input));
                    });

                    nodeData.outputs().forEach(($output : IVXNode) : void => {
                        (<any>$output).Parent($parent);
                        model.getOutputs().Add((<any>$output));
                    });

                    nodeData.params().forEach(($param : IVXNode) : void => {
                        model.getParams().Add($param);
                    });
                };
                const createGraph : any = ($parent : VXContext) : VXGraph => {
                    const model : VXGraph = new VXGraph();
                    model.Index(index.graph);
                    model.Parent($parent);
                    model.Configuration(this.visualGraph.Configuration());
                    $parent.getVxGraphs().Add(model);
                    index.graph++;
                    index.node = 0;
                    return model;
                };
                const createContext : any = () : VXContext => {
                    const model : VXContext = new VXContext();
                    model.Index(index.context);
                    model.Parent(this.visualGraph);
                    model.Configuration(this.visualGraph.Configuration());
                    index.context++;
                    index.graph = 0;
                    return model;
                };

                const contextMapping : ArrayList<VXGraph> = new ArrayList<VXGraph>();

                $graphRoot.ChildNodes().foreach(($context : KernelNode) : void => {
                    const context : VXContext = createContext();
                    context.Name($context.Name());
                    context.UniqueId($context.UniqueId());
                    context.Row($context.Row());
                    context.Column($context.Column());
                    context.GridDimX($context.GridSize().Width());
                    context.GridDimY($context.GridSize().Height());
                    this.visualGraph.getVxContexts().Add(context);
                    $context.ChildNodes().foreach(($graph : KernelNode) : void => {
                        const graph : VXGraph = createGraph(context);
                        graph.Name($graph.Name());
                        graph.UniqueId($graph.UniqueId());
                        graph.Row($graph.Row());
                        graph.Column($graph.Column());
                        graph.GridDimX($graph.GridSize().Width());
                        graph.GridDimY($graph.GridSize().Height());
                        $graph.ChildNodes().foreach(($node : KernelNode) : void => {
                            contextMapping.Add(graph, $node.Id());
                            createNode($node, graph);
                            this.kernelNodesMap.push($node);
                        });
                    });
                });

                const dataStorage : VXGraph = createGraph(createContext("IOComContext"));
                dataStorage.UniqueId("DataStorage");

                for (index.kernel = 0; index.kernel < this.kernelNodesMap.length; index.kernel++) {
                    const kernelNode : IKernelNode = this.kernelNodesMap[index.kernel];
                    const vxNode : VXNode = <VXNode>this.graphNodesMap[index.kernel];

                    if (!kernelNode.getOutputs().IsEmpty()) {
                        kernelNode.getOutputs().foreach(($output : Output) : void => {
                            $output.inputs.foreach(($input : Input, $index : number) : void => {
                                const inputVxNode : VXNode = <VXNode>this.graphNodesMap[this.kernelNodesMap.indexOf($input.node)];
                                const inputVxData : VXNode = <VXNode>inputVxNode.getInputs().getItem($input.position);
                                const outputIndex : number = $output.position;
                                if ($index === 0) {
                                    vxNode.getOutputs().Add(inputVxData, outputIndex);
                                    inputVxData.getInputs().Clear();
                                    inputVxData.getInputs().Add(vxNode);
                                } else {
                                    const swappedOutput : any = vxNode.getOutputs().getItem(outputIndex);
                                    swappedOutput.getOutputs().Add(inputVxNode);
                                    inputVxNode.getInputs().Add(swappedOutput, $input.position);
                                }
                            });
                        });
                    }
                }

                index.kernel = 0;
                this.graphNodesMap.forEach(($node : VXNode) : void => {
                    const kernelNode : IKernelNode = this.kernelNodesMap[index.kernel];
                    const context : VXGraph = contextMapping.getItem(kernelNode.Id());

                    const registerData : any = ($data : VXNode) : void => {
                        if (this.getTemplates().vxData.hasOwnProperty($data.Type())) {
                            if (!context.getVxData().Contains($data)) {
                                $data.Index(context.getVxData().Length());
                                $data.Parent(context);
                                context.getVxData().Add($data);
                            }
                        }
                    };
                    $node.getInputs().foreach(registerData);
                    $node.getOutputs().foreach(registerData);

                    index.kernel++;
                });

                if (!dataStorage.getVxData().IsEmpty()) {
                    this.visualGraph.getVxContexts().Add(dataStorage.Parent());
                }
            }

            let index : number = 0;
            this.kernelNodesMap.forEach(($kernelNode : KernelNode) : void => {
                const vxNode : VXNode = <VXNode>this.graphNodesMap[index];
                vxNode.FromKernelNode($kernelNode);
                const nodeData : IVXNodeModel = vxNode.ToModel();
                nodeData.params().forEach(($param : IVXNode, $index : number) : void => {
                    vxNode.getParams().Add($param, $index);
                });
                index++;
            });
        }

        public ToJsonp() : string {
            return this.graphDao.ToJsonp();
        }

        public getVisualGraph() : VisualGraph {
            return this.visualGraph;
        }

        public getVisualGraphDAO() : VisualGraphDAO {
            return this.graphDao;
        }

        public getVxNodeNode($kernelNode : IKernelNode) : VXNode {
            const nodeIndex : number = this.kernelNodesMap.indexOf($kernelNode);
            if (nodeIndex !== -1) {
                const node : VXNode = <VXNode>this.graphNodesMap[nodeIndex];
                if (!ObjectValidator.IsEmptyOrNull(node) && node.IsTypeOf(VXNode)) {
                    return node;
                }
            }
            return null;
        }

        public getVisualGraphEvents() : IVisualGraphDAOEvents {
            return this.graphDao.getEvents();
        }

        public getTemplates() : IVXNodeTemplates {
            return this.graphDao.getConfigurationInstance().vxAPI;
        }

        public getAppWizardDao() : IAppWizardLocalization {
            return this.appWizardDao.getStaticConfiguration();
        }

        public getWizardItem($index : number = 0) : IAppWizardItem {
            const configuration : IAppWizardLocalization = this.appWizardDao.getStaticConfiguration();
            if ($index >= 0 && $index < configuration.items.length) {
                const args : AppWizardPanelViewerArgs = this.getModelArgs().AppWizardPanelViewerArgs();
                const itemConfig : IAppWizardItem = configuration.items[$index];
                const item : IAppWizardItem = <IAppWizardItem>{
                    after     : ($instance : IGuiCommons, $dao : BasePageDAO, $callback : () => void) : void => {
                        $callback();
                    },
                    before    : ($instance : IGuiCommons, $dao : BasePageDAO, $callback : ($instance : IGuiCommons) => void) : void => {
                        $callback(null);
                    },
                    dimensions: null,
                    header    : itemConfig.header,
                    text      : itemConfig.text
                };
                args.HeaderText(item.header);
                args.DescriptionText(item.text);
                if (!ObjectValidator.IsEmptyOrNull(itemConfig.dimensions)) {
                    item.dimensions = <IBoundingRectangle>{};
                    const generateNumber : any = ($source : any) : PropagableNumber => {
                        if (ObjectValidator.IsInteger($source)) {
                            return new PropagableNumber({number: $source, unitType: UnitType.PX});
                        } else if (ObjectValidator.IsString($source)) {
                            const args : IPropagableNumberValue[] = [];
                            const values : string[] = $source.split(";");
                            values.forEach(($value : string) => {
                                args.push({
                                    number  : StringUtils.ToInteger($value),
                                    unitType: StringUtils.Contains($value, "px") ? UnitType.PX : UnitType.PCT
                                });
                            });
                            return new PropagableNumber(args);
                        } else {
                            return undefined;
                        }
                    };
                    if (!ObjectValidator.IsEmptyOrNull(itemConfig.dimensions.width)) {
                        item.dimensions.width = generateNumber(itemConfig.dimensions.width);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(itemConfig.dimensions.height)) {
                        item.dimensions.height = generateNumber(itemConfig.dimensions.height);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(itemConfig.dimensions.offsetBottom)) {
                        item.dimensions.offsetBottom = generateNumber(itemConfig.dimensions.offsetBottom);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(itemConfig.dimensions.offsetLeft)) {
                        item.dimensions.offsetLeft = generateNumber(itemConfig.dimensions.offsetLeft);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(itemConfig.dimensions.offsetRight)) {
                        item.dimensions.offsetRight = generateNumber(itemConfig.dimensions.offsetRight);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(itemConfig.dimensions.offsetTop)) {
                        item.dimensions.offsetTop = generateNumber(itemConfig.dimensions.offsetTop);
                    }
                }
                if (!ObjectValidator.IsEmptyOrNull(itemConfig.before)) {
                    item.before = itemConfig.before;
                }
                if (!ObjectValidator.IsEmptyOrNull(itemConfig.after)) {
                    item.after = itemConfig.after;
                }
                return item;
            } else {
                return null;
            }
        }

        public getSettingsDao() : SettingsDAO {
            return this.settingsDao;
        }

        public getInstallDao() : InstallationRecipeDAO {
            return this.installDao;
        }

        public Load($language : LanguageType, $asyncHandler : () => void, $force : boolean = false) : void {
            super.Load($language, () : void => {
                const loadProject : any = ($path? : string) : void => {
                    const path : string = ObjectValidator.IsEmptyOrNull($path) ?
                        this.getPageConfiguration().globalSettings.templates[APIType.OVX_1_2_0] : $path + "/VisualGraph.jsonp";
                    this.settingsDao.Load($language, () : void => {
                        this.LoadProject(path, () : void => {
                            this.appWizardDao.Load($language, () : void => {
                                this.installDao.Load($language, $asyncHandler);
                            });
                        }, $force);
                    }, $force);
                };
                if ($force) {
                    loadProject();
                } else {
                    this.getPersistedProjectPath(loadProject);
                }
            }, $force);
        }

        public LoadProject($path : string, $asyncHandler : () => void, $force : boolean = true) : void {
            if (!ObjectValidator.IsEmptyOrNull($path)) {
                this.graphDao = new VisualGraphDAO();
                this.graphDao.setConfigurationPath($path);
                this.graphDao.Load(LanguageType.EN, () : void => {
                    if ($force) {
                        this.getVisualGraphArgs();
                    }
                    if (!ObjectValidator.IsEmptyOrNull(this.visualGraph)) {
                        this.getModelArgs().EditorPanelViewerArgs().VisualGraphPanelArgs(this.getVisualGraphArgs());
                        if (this.vxVersion !== this.getTemplates().version) {
                            this.vxVersion = this.getTemplates().version;
                            this.loadNodes();
                        }
                    }
                    $asyncHandler();
                }, true);
            } else {
                $asyncHandler();
            }
        }

        public PersistProjectPath($path : string) : void {
            PersistenceFactory.getPersistence(this.getClassName()).Variable("ProjectPath", $path);
        }

        public getPersistedProjectPath($callback : ($path : string) => void) : void {
            const path : string = PersistenceFactory.getPersistence(this.getClassName()).Variable("ProjectPath");
            if (!ObjectValidator.IsEmptyOrNull(path) && ObjectValidator.IsString(path)) {
                this.fileSystem.Exists(path + "/VisualGraph.jsonp")
                    .Then(($success : boolean) : void => {
                        $callback($success ? path : null);
                    });
            } else {
                $callback(null);
            }
        }

        public getVisualGraphPath() : string {
            return this.graphDao.getDaoDataSource();
        }

        public getProjects($callback : ($list : ArrayList<VisualGraphDAO>) => void) : void {
            const paths : string[] = this.getPageConfiguration().projectsList;
            if (this.projectsList.IsEmpty() && paths.length > 0) {
                const getNextProject : any = ($index : number) : void => {
                    if ($index < paths.length) {
                        const dao : VisualGraphDAO = new VisualGraphDAO();
                        dao.setConfigurationPath(paths[$index]);
                        dao.Load(LanguageType.EN, () : void => {
                            this.projectsList.Add(dao, paths[$index]);
                            getNextProject($index + 1);
                        });
                    } else {
                        $callback(this.projectsList);
                    }
                };
                getNextProject(0);
            } else {
                $callback(this.projectsList);
            }
        }

        public Clear() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.graphDao)) {
                this.graphDao.getVisualGraph().getIoComs().Clear();
                this.graphDao.getVisualGraph().getVxContexts().Clear();
                this.getModelArgs().EditorPanelViewerArgs().VisualGraphPanelArgs(this.getVisualGraphArgs());
            }
        }

        public getGraphRestrictedNodes($platformName : string) : INodeRestrictions {
            const restrictedTypes : string[] = this.getPageConfiguration().globalSettings.nodeRestrictions[$platformName];
            const names : ArrayList<string> = new ArrayList<string>();
            const types : ArrayList<string> = new ArrayList<string>();
            const nodes : ArrayList<IKernelNode> = new ArrayList<IKernelNode>();

            if (!ObjectValidator.IsEmptyOrNull(restrictedTypes)) {
                const handler : any = ($node : VisualGraphNode) : void => {
                    const node : IKernelNode = this.kernelNodesMap[this.graphNodesMap.indexOf($node)];
                    if (restrictedTypes.indexOf($node.Type()) !== -1) {
                        nodes.Add(node, node.Name());
                        names.Add(node.Name(), node.Name());
                        if (!types.Contains($node.Type())) {
                            types.Add(node.Type(), node.Type());
                        }
                    }
                };
                const graph : VisualGraph = this.getVisualGraph();
                graph.getVxContexts().foreach(($context : VXContext) : void => {
                    $context.getVxGraphs().foreach(($graph : VXGraph) : void => {
                        $graph.getVxNodes().foreach(($node : VXNode) : void => {
                            handler($node);
                        });
                    });
                });
                graph.getIoComs().foreach(($node : VXNode) : void => {
                    handler($node);
                });
            }
            names.SortByKeyUp();
            types.SortByKeyUp();
            nodes.SortByKeyUp();
            return {types: types.ToArray(), names: names.ToArray(), nodes: nodes.ToArray()};
        }

        public getInvalidNodeConnections() : IConnectionValidatorStatus[] {
            const returnVal : IConnectionValidatorStatus[] = [];
            this.kernelNodesMap.forEach(($node : IKernelNode) : void => {
                $node.getOutputs().foreach(($output : Output) : void => {
                    $output.inputs.foreach(($input : Input) : void => {
                        const status : IConnectionValidatorStatus
                            = this.Validator($output.node, $input.node, $output.position, $input.position);
                        if (!status.status) {
                            returnVal.push(status);
                        }
                    });
                });
            });
            return returnVal;
        }

        private overwriteFrontendAttributes($source : VXNode, $target : IKernelNode) : void {
            $source.getAttributes().foreach(($attribute : IVXNodeAttribute) : void => {
                $target.setAttribute(<IKernelNodeAttribute>{
                    format: $attribute.format,
                    items : $attribute.options,
                    name  : $attribute.varName,
                    text  : $attribute.name,
                    type  : $attribute.type,
                    value : $attribute.value
                });
            });
        }

        private getVisualGraphArgs() : VisualGraphPanelViewerArgs {
            const args : VisualGraphPanelViewerArgs = new VisualGraphPanelViewerArgs();
            this.graphNodesMap = [];
            this.kernelNodesMap = [];
            const connectionsRegister : any[] = [];
            const registerConnection : any = ($handler : () => void) : void => {
                connectionsRegister.push($handler);
            };
            const registerNode : any = ($node : VXNode, $parentGraph? : KernelNode) : void => {
                this.graphNodesMap.push($node);
                const configuration : IVisualGraph = $node.Configuration();
                if (!configuration.vxAPI.vxData.hasOwnProperty($node.Type())) {
                    const kernelNode : KernelNode = $node.ToKernelNode();
                    this.kernelNodesMap.push(kernelNode);
                    if (ObjectValidator.IsSet($parentGraph)) {
                        $parentGraph.ChildNodes().Add(kernelNode);
                        kernelNode.ParentNode($parentGraph);
                    }

                    const linkInput : any = ($inputVxData : VXNode, $index : number) => {
                        const input : Input = kernelNode.getInput($index);
                        const outputVxNode : VXNode = $inputVxData.getInputs().getItem(0);
                        if (!ObjectValidator.IsEmptyOrNull(outputVxNode)) {
                            const outputNode : IKernelNode = this.kernelNodesMap[this.graphNodesMap.indexOf(outputVxNode)];
                            if (!ObjectValidator.IsEmptyOrNull(outputNode)) {
                                const output : Output = outputNode.getOutput(outputVxNode.getOutputs().IndexOf($inputVxData));
                                output.inputs.Add(input);
                                input.output = output;
                            }
                        }
                    };

                    $node.getInputs().foreach(($input : VXNode, $index : number) : void => {
                        registerConnection(() : void => {
                            if (configuration.vxAPI.vxData.hasOwnProperty($input.Type())) {
                                linkInput($input, $index);
                            }
                        });
                    });
                }
            };

            this.visualGraph = this.graphDao.getVisualGraph();
            const visualGraphNode : KernelNode = this.visualGraph.ToKernelNode(IconType.VISUAL_GRAPH);
            visualGraphNode.setAttribute(<IKernelNodeAttribute>{
                name : "path",
                text : "Project path",
                type : "JsonpFile",
                value: this.getVisualGraphPath()
            });

            const graphs : ArrayList<KernelNode> = new ArrayList<KernelNode>();
            this.visualGraph.getVxContexts().foreach(($context : VXContext) : void => {
                const contextNode : KernelNode = $context.ToKernelNode(IconType.VXCONTEXT);
                visualGraphNode.ChildNodes().Add(contextNode);
                contextNode.ParentNode(visualGraphNode);
                $context.getVxGraphs().foreach(($graph : VXGraph) : void => {
                    const graphNode : KernelNode = $graph.ToKernelNode(IconType.VXGRAPH);
                    graphs.Add(graphNode, graphNode.Name());
                    contextNode.ChildNodes().Add(graphNode);
                    graphNode.ParentNode(contextNode);
                    $graph.getVxNodes().foreach(($node : VXNode) : void => {
                        registerNode($node, graphNode);
                    });
                });
            });

            this.visualGraph.getIoComs().foreach(($ioCom : VXNode) : void => {
                let parent : KernelNode = null;
                $ioCom.getParams().foreach(($param : any) : boolean => {
                    if ($param instanceof VXNode) {
                        if (this.getTemplates().vxData.hasOwnProperty($param.Type())) {
                            parent = graphs.getItem($param.Parent().Name());
                            return false;
                        }
                    }
                });
                registerNode($ioCom, parent);
            });

            connectionsRegister.forEach(($handler : () => void) : void => {
                $handler();
            });

            args.GraphRoot(visualGraphNode);

            return args;
        }

        private loadNodes() : void {
            const pickerArgs : KernelNodesPickerPanelViewerArgs = this.getModelArgs().EditorPanelViewerArgs().KernelNodePickerPanelArgs();
            pickerArgs.getKernelNodes().Clear();
            const getSupportedNodes : any = ($nodes : IVXNodeTemplate[]) : void => {
                let nodeName : string;
                const types = this.getTemplates().vxTypes;
                const typeExists : any = ($typeName : string) : boolean => {
                    let retVal : boolean = false;
                    if (!ObjectValidator.IsEmptyOrNull(types) && ObjectValidator.IsObject(types)) {
                        let typeName : string;
                        for (typeName in types) {
                            if (types.hasOwnProperty(typeName) && $typeName === types[typeName]) {
                                retVal = true;
                                break;
                            }
                        }
                    }
                    return retVal;
                };

                for (nodeName in $nodes) {
                    if ($nodes.hasOwnProperty(nodeName) && typeExists(nodeName)) {
                        const node : KernelNode = new KernelNode($nodes[nodeName].icon, -1, -1,
                            <string>(ObjectValidator.IsEmptyOrNull($nodes[nodeName].groupId) ?
                                VxNodesGroupType.DEFAULT : $nodes[nodeName].groupId));
                        node.Type(nodeName);
                        const model : VXNode = new VXNode();
                        model.Configuration(this.visualGraph.Configuration());
                        model.FromKernelNode(node);
                        node.Name(model.Name());
                        node.UniqueIdPrefix(model.Name());
                        const modelConfig : IVXNodeModel = model.ToModel();
                        modelConfig.outputs().forEach(($node : IVXNode) => {
                            node.AddOutput((<any>$node).Type());
                        });
                        modelConfig.inputs().forEach(($node : IVXNode) => {
                            node.AddInput((<any>$node).Type());
                        });
                        modelConfig.attributes().forEach(($attribute : IVXNodeAttribute) : void => {
                            node.setAttribute(<IKernelNodeAttribute>{
                                format: $attribute.format,
                                items : $attribute.options,
                                name  : $attribute.varName,
                                text  : $attribute.name,
                                type  : $attribute.type,
                                value : $attribute.value
                            });
                        });
                        model.getAttributes().foreach(($attribute : IVXNodeAttribute) : void => {
                            modelConfig.correctAttributes($attribute, true);
                        });
                        node.Help(model.getHelp());
                        pickerArgs.AddKernelNode(node);
                    }
                }
            };
            getSupportedNodes(this.getTemplates().vxIoComs);
            getSupportedNodes(this.getTemplates().vxNodes);

            const context : KernelNode = new KernelNode(KernelNodeType.VXCONTEXT, -1, -1, "Contexts");
            context.Name("Context");
            context.UniqueIdPrefix("vxContext");
            context.Type(KernelNodeType.VXCONTEXT);
            context.Help(this.getTemplates().vxContext.help);
            pickerArgs.AddKernelNode(context);

            const graph : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, -1, -1, "Graphs");
            graph.Name("Graph");
            graph.UniqueIdPrefix("vxGraph");
            graph.Type(KernelNodeType.VXGRAPH);
            graph.Help(this.getTemplates().vxGraph.help);
            pickerArgs.AddKernelNode(graph);
        }
    }
}
