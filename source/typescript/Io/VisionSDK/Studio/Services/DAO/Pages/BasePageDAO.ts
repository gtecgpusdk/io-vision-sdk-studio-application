/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.DAO.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class BasePageDAO extends Com.Wui.Framework.Services.DAO.BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/VisionSDK/Studio/Services/Localization/BasePageLocalization.jsonp";

        protected static getDaoInterfaceClassName($interfaceName : string) : any {
            switch ($interfaceName) {
            case "IBasePageConfiguration":
                return Pages.BasePageDAO;
            case "IMainPageConfiguration":
                return Pages.MainPageDAO;
            case "IKernelGeneratorConfiguration":
                return Pages.KernelGeneratorDAO;
            case "ISettingLocalization":
                return Pages.SettingsDAO;
            case "IBrowserErrorLocalization":
                return Pages.BrowserErrorPageDAO;
            default:
                break;
            }
            return BasePageDAO;
        }

        protected getResourcesHandler() : any {
            return Resources;
        }
    }
}
