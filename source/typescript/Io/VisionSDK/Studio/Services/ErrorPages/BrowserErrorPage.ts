/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.ErrorPages {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BrowserErrorPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.BrowserErrorPageDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class BrowserErrorPage extends Com.Wui.Framework.Gui.ErrorPages.BaseErrorPage {
        private dao : BrowserErrorPageDAO;

        constructor() {
            super();
            this.dao = new BrowserErrorPageDAO();
        }

        protected getFaviconSource() : string {
            return "resource/graphics/VisionSdk.ico";
        }

        protected getPageTitle() : string {
            return this.dao.getStaticConfiguration().title;
        }

        protected getMessageHeader() : string {
            return this.dao.getStaticConfiguration().header;
        }

        protected getMessageBody() : string {
            return StringUtils.Format(this.dao.getStaticConfiguration().message,
                StringUtils.NewLine() +
                "<a href=\"http://www.google.com/chrome/\" target=\"_blank\">Google Chrome</a>" + StringUtils.NewLine() +
                "<a href=\"http://www.opera.com/\" target=\"_blank\">Opera</a>" + StringUtils.NewLine() +
                "<a href=\"https://www.apple.com/safari/\" target=\"_blank\">Safari</a>");
        }

        protected getPageBody() : string {
            const EOL : string = StringUtils.NewLine(false);
            let output : string = "";
            output +=
                "<div class=\"BrowserErrorPage\">" + EOL +
                "   <div class=\"Image\"></div>" + EOL +
                "   <h1>" + this.getMessageHeader() + "</h1>" + EOL +
                "   <h2>" + this.getMessageBody() + "</h2>" + EOL +
                "</div>";

            return output;
        }

        protected resolver() : void {
            this.dao.Load(LanguageType.EN, () : void => {
                super.resolver();
            }, true);
        }
    }
}
