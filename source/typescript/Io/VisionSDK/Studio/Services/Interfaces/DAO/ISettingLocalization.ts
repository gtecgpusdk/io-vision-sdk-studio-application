/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces.DAO {
    "use strict";
    import IBasePageLocalization = Com.Wui.Framework.Services.Interfaces.DAO.IBasePageLocalization;
    import IConnectionSettings = Io.VisionSDK.Studio.Gui.Interfaces.IConnectionSettings;
    import IToolchainSettings = Io.VisionSDK.Studio.Gui.Interfaces.IToolchainSettings;
    import ISettings = Io.VisionSDK.Studio.Gui.Interfaces.ISettings;

    export abstract class IAboutSettingsPanelLocalization {
        public readonly header : string;
        public readonly versionLabel : string;
        public readonly versionValue : string;
        public readonly aboutText : string;
        public readonly panelDescription : string;
    }

    export abstract class IConnectionSettingsPanelLocalization {
        public readonly header : string;
        public readonly name : string;
        public readonly address : string;
        public readonly username : string;
        public readonly password : string;
        public readonly platform : string;
        public readonly agentName : string;
        public readonly settings : IConnectionSettings;
        public readonly panelDescription : string;
    }

    export abstract class IToolchainSettingsPanelLocalization {
        public readonly header : string;
        public readonly name : string;
        public readonly cmakePath : string;
        public readonly cppPath : string;
        public readonly linker : string;
        public readonly settings : IToolchainSettings;
        public readonly validateButton : string;
        public readonly panelDescription : string;
    }

    export abstract class IProjectSettingsPanelLocalization {
        public readonly header : string;
        public readonly name : string;
        public readonly vxVersion : string;
        public readonly vxVersionOptions : string[];
        public readonly description : string;
        public readonly path : string;
        public readonly create? : string;
        public readonly panelDescription : string;
    }

    export abstract class ISettingLocalization extends IBasePageLocalization {
        public readonly save : string;
        public readonly cancel : string;
        public readonly about : string;
        public readonly connection : string;
        public readonly toolchain : string;
        public readonly project : string;
        public readonly back : string;
        public readonly header : string;
        public readonly menuHeader : string;
        public readonly descriptionHeader : string;
        public readonly projectSettingsPanel : IProjectSettingsPanelLocalization;
        public readonly connectionSettingsPanel : IConnectionSettingsPanelLocalization;
        public readonly toolchainSettingsPanel : IToolchainSettingsPanelLocalization;
        public readonly aboutPanel : IAboutSettingsPanelLocalization;
        public readonly values : ISettings;
        public readonly projectId : string;
    }
}
