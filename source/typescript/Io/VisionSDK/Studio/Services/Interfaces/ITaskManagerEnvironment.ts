/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces {
    "use strict";
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import IMainPageNotifications = Io.VisionSDK.Studio.Services.Interfaces.DAO.IMainPageNotifications;

    export abstract class ITaskManagerEnvironment {
        public notifications : IMainPageNotifications;
        public sdkVersion : string;
        public runtimeVersion : string;
        public compilerPath? : string;
        public cmakePath? : string;
        public sdkPath? : string;
        public platform? : PlatformType;
        public agentName? : string;
        public serverUrl? : string;
        public hubUrl? : string;
    }
}
