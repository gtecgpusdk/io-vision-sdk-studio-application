#!/bin/sh
# * ********************************************************************************************************* *
# *
# * Copyright (c) 2019 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

mkdir -p /var/visionsdk/io-vision-sdk-studio-services/resource/libs/WuiConnector
/var/wui/com-wui-framework-connector/WuiConnector --config-locations=/var/visionsdk/io-vision-sdk-studio-services/resource/libs/WuiConnector &

while [ ! -f /var/visionsdk/io-vision-sdk-studio-services/resource/libs/WuiConnector/connector.config.jsonp ]
do
  sleep 1
done

/var/wui/com-wui-framework-localhost/WuiLocalhost start --target=/var/visionsdk/io-vision-sdk-studio-services
