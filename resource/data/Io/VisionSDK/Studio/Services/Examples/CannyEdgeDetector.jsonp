/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.VisionSDK.Studio.Services.DAO.Resources.Data({
    $interface: "IVisualGraph",
    imports: {
        vxAPI: "resource/data/Io/VisionSDK/Studio/Services/Configuration/OpenVX_1_1_0/CodeGeneratorTemplates.jsonp"
    },
    id: "9ba99caf16bdf1fd17623b6dce429472a1c6bfc3",
    name: "Canny Edge Detector",
    description: "This example shows basic usage ov Canny Edge Detector applied on camera stream.<br><br>" +
        "<img src=\"resource/data/Io/VisionSDK/Studio/Services/Examples/Snapshots/CannyEdgeDetector.png\">",
    gridDimX: 4,
    gridDimY: 4,
    ioComs: [
        {
            type() {
                return this.vxAPI.vxTypes.CAMERA;
            },
            name: "ioCom0",
            id: "ioCom0",
            column: 0,
            row: 0,
            deviceIndex: 0,
            height: 480,
            width: 640,
            outputType: "VX_DF_IMAGE_RGB",
            fpsType: "VX_TYPE_FLOAT32",
            fps: 10.000,
            params() {
                return [
                    this.ioComs[0].deviceIndex,
                    this.vxContexts[0].vxGraphs[0].vxData[0],
                    this.vxContexts[0].vxGraphs[0].vxData[5]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[0],
                    this.vxContexts[0].vxGraphs[0].vxData[5]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.DISPLAY;
            },
            name: "ioCom1",
            id: "ioCom1",
            column: 4,
            row: 0,
            inputType: "VX_DF_IMAGE_U8",
            windowTitle: "Detected edges",
            params() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[4],
                    this.ioComs[1].windowTitle
                ];
            }
        }
    ],
    vxContexts: [
        {
            name: "Context",
            id: "Context0",
            column: 0,
            row: 0,
            gridDimX: 4,
            gridDimY: 4,
            vxGraphs: [
                {
                    name: "Graph",
                    id: "Graph0",
                    column: 0,
                    row: 0,
                    gridDimX: 5,
                    gridDimY: 4,
                    vxNodes: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_COLOR_CONVERT;
                            },
                            name: "vxNode0",
                            id: "vxNode0",
                            column: 1,
                            row: 0,
                            inputImageFormat: "VX_DF_IMAGE_RGB",
                            outputImageFormat: "VX_DF_IMAGE_YUV4",
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[1]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_CANNY_EDGE_DETECTOR;
                            },
                            name: "vxNode1",
                            id: "vxNode1",
                            column: 3,
                            row: 0,
                            inputType: "VX_DF_IMAGE_U8",
                            threshType: "VX_THRESHOLD_TYPE_RANGE",
                            dataType: "VX_TYPE_UINT8",
                            threshValue: [50,150],
                            gradientSize: 3,
                            normType: "VX_NORM_L1",
                            outputType: "VX_DF_IMAGE_U8",
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[2],
                                    this.vxContexts[0].vxGraphs[0].vxData[3],
                                    this.vxContexts[0].vxGraphs[0].vxData[4]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[4]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_CHANNEL_EXTRACT;
                            },
                            name: "vxNode2",
                            id: "vxNode2",
                            column: 2,
                            row: 0,
                            inputType: "VX_DF_IMAGE_YUV4",
                            channel: "VX_CHANNEL_Y",
                            outputType: "VX_DF_IMAGE_U8",
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[1],
                                    this.vxContexts[0].vxGraphs[0].vxData[2]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[2]
                                ];
                            }
                        }
                    ],
                    vxData: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData0",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[0].width,
                                    this.ioComs[0].height,
                                    this.ioComs[0].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData1",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[0],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0].outputImageFormat
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData2",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[1],
                                    this.vxContexts[0].vxGraphs[0].vxData[1],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_THRESHOLD_OBJ;
                            },
                            id: "vxData3",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxAPI.vxTypes.VX_THRESHOLD_OBJ,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1].threshType,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1].dataType,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1].threshValue
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData4",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[2],
                                    this.vxContexts[0].vxGraphs[0].vxData[2],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.ioComs[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_SCALAR;
                            },
                            id: "vxData5",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[0].fpsType,
                                    this.ioComs[0].fps
                                ];
                            }
                        }
                    ]
                }
            ]
        }
    ]
});
