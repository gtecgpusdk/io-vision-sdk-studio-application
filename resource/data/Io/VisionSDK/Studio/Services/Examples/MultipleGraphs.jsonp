/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.VisionSDK.Studio.Services.DAO.Resources.Data({
    $interface: "IVisualGraph",
    imports: {
        vxAPI: "resource/data/Io/VisionSDK/Studio/Services/Configuration/OpenVX_1_1_0/CodeGeneratorTemplates.jsonp"
    },
    id: "7e778ac19645556cb9715b28b95d7b03842b6722",
    name: "Multiple graphs",
    description: "This example shows usage of project hierarchy divided into multiple graphs in multiple contexts.<br><br>" +
        "<ul><li><b>First context</b></li><br>" +
        "<ul><li>First graph</li><img src=\"resource/data/Io/VisionSDK/Studio/Services/Examples/Snapshots/MultiGraph_c0g0.png\"><br>" +
        "<li>Second graph</li><img src=\"resource/data/Io/VisionSDK/Studio/Services/Examples/Snapshots/MultiGraph_c0g1.png\"><br>" +
        "</ul><li><b>Second context</b></li><br>" +
        "<ul><li>Third graph</li><img src=\"resource/data/Io/VisionSDK/Studio/Services/Examples/Snapshots/MultiGraph_c1g3.png\"><br>" +
        "<li>Fourth graph</li><img src=\"resource/data/Io/VisionSDK/Studio/Services/Examples/Snapshots/MultiGraph_c1g4.png\"><br>" +
        "</ul></ul>",
    gridDimX: 4,
    gridDimY: 4,
    ioComs: [
        {
            type() {
                return this.vxAPI.vxTypes.IMAGE_INPUT;
            },
            name: "ioCom5",
            id: "ioCom5",
            column: 0,
            row: 0,
            path: "{appPath}/resource/data/VisionSDK.png",
            height: 310,
            width: 310,
            outputType: "VX_DF_IMAGE_RGBX",
            params() {
                return [
                    this.ioComs[0].path,
                    this.vxContexts[0].vxGraphs[0].vxData[0]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[0]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.IMAGE_OUTPUT;
            },
            name: "ioCom6",
            id: "ioCom6",
            column: 2,
            row: 0,
            path: "data/output/c0g0.png",
            inputType: "VX_DF_IMAGE_U8",
            params() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[1],
                    this.ioComs[1].path
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.IMAGE_INPUT;
            },
            name: "ioCom7",
            id: "ioCom7",
            column: 0,
            row: 0,
            path: "{appPath}/resource/data/VisionSDK.png",
            height: 310,
            width: 310,
            outputType: "VX_DF_IMAGE_RGBX",
            params() {
                return [
                    this.ioComs[2].path,
                    this.vxContexts[0].vxGraphs[1].vxData[0]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[0].vxGraphs[1].vxData[0]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.IMAGE_OUTPUT;
            },
            name: "ioCom8",
            id: "ioCom8",
            column: 3,
            row: 0,
            path: "data/output/c0g1.png",
            inputType: "VX_DF_IMAGE_U8",
            params() {
                return [
                    this.vxContexts[0].vxGraphs[1].vxData[2],
                    this.ioComs[3].path
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.IMAGE_INPUT;
            },
            name: "ioCom9",
            id: "ioCom9",
            column: 0,
            row: 0,
            path: "{appPath}/resource/data/VisionSDK.png",
            height: 310,
            width: 310,
            outputType: "VX_DF_IMAGE_RGBX",
            params() {
                return [
                    this.ioComs[4].path,
                    this.vxContexts[1].vxGraphs[0].vxData[0]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[1].vxGraphs[0].vxData[0]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.IMAGE_OUTPUT;
            },
            name: "ioCom10",
            id: "ioCom10",
            column: 4,
            row: 0,
            path: "data/output/c1g2.png",
            inputType: "VX_DF_IMAGE_U8",
            params() {
                return [
                    this.vxContexts[1].vxGraphs[0].vxData[4],
                    this.ioComs[5].path
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.IMAGE_INPUT;
            },
            name: "ioCom11",
            id: "ioCom11",
            column: 0,
            row: 0,
            path: "{appPath}/resource/data/VisionSDK.png",
            height: 310,
            width: 310,
            outputType: "VX_DF_IMAGE_RGBX",
            params() {
                return [
                    this.ioComs[6].path,
                    this.vxContexts[1].vxGraphs[1].vxData[0]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[1].vxGraphs[1].vxData[0]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.IMAGE_OUTPUT;
            },
            name: "ioCom12",
            id: "ioCom12",
            column: 3,
            row: 0,
            path: "data/output/c1g3.png",
            inputType: "VX_DF_IMAGE_U8",
            params() {
                return [
                    this.vxContexts[1].vxGraphs[1].vxData[3],
                    this.ioComs[7].path
                ];
            }
        }
    ],
    vxContexts: [
        {
            name: "FirstContext",
            id: "Context0",
            column: 0,
            row: 0,
            gridDimX: 4,
            gridDimY: 4,
            vxGraphs: [
                {
                    name: "FirstGraph",
                    id: "Graph0",
                    column: 0,
                    row: 0,
                    gridDimX: 4,
                    gridDimY: 4,
                    vxNodes: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_CHANNEL_EXTRACT;
                            },
                            name: "vxNode0",
                            id: "vxNode0",
                            column: 1,
                            row: 0,
                            inputType: "VX_DF_IMAGE_RGBX",
                            channel: "VX_CHANNEL_R",
                            outputType: "VX_DF_IMAGE_U8",
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[1]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[1]
                                ];
                            }
                        }
                    ],
                    vxData: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData0",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[0].width,
                                    this.ioComs[0].height,
                                    this.ioComs[0].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData1",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[0],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.ioComs[1]
                                ];
                            }
                        }
                    ]
                },
                {
                    name: "SecondGraph",
                    id: "Graph1",
                    column: 0,
                    row: 1,
                    gridDimX: 4,
                    gridDimY: 4,
                    vxNodes: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_CHANNEL_EXTRACT;
                            },
                            name: "vxNode1",
                            id: "vxNode1",
                            column: 1,
                            row: 0,
                            inputType: "VX_DF_IMAGE_RGBX",
                            channel: "VX_CHANNEL_R",
                            outputType: "VX_DF_IMAGE_U8",
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[1],
                                    this.vxContexts[0].vxGraphs[1].vxData[0],
                                    this.vxContexts[0].vxGraphs[1].vxData[1]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[1].vxData[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_BITWISE_NOT;
                            },
                            name: "vxNode2",
                            id: "vxNode2",
                            column: 2,
                            row: 0,
                            imageType: "VX_DF_IMAGE_U8",
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[1],
                                    this.vxContexts[0].vxGraphs[1].vxData[1],
                                    this.vxContexts[0].vxGraphs[1].vxData[2]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[1].vxData[2]
                                ];
                            }
                        }
                    ],
                    vxData: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData0",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[2].width,
                                    this.ioComs[2].height,
                                    this.ioComs[2].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[1].vxNodes[0]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData1",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[1].vxData[0],
                                    this.vxContexts[0].vxGraphs[1].vxData[0],
                                    this.vxContexts[0].vxGraphs[1].vxNodes[0].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[1].vxNodes[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData2",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[1].vxData[1],
                                    this.vxContexts[0].vxGraphs[1].vxData[1],
                                    this.vxContexts[0].vxGraphs[1].vxNodes[1].imageType
                                ];
                            },
                            outputs() {
                                return [
                                    this.ioComs[3]
                                ];
                            }
                        }
                    ]
                }
            ]
        },
        {
            name: "SecondContext",
            id: "Context1",
            column: 0,
            row: 1,
            gridDimX: 4,
            gridDimY: 4,
            vxGraphs: [
                {
                    name: "ThirdGraph",
                    id: "Graph2",
                    column: 0,
                    row: 0,
                    gridDimX: 5,
                    gridDimY: 4,
                    vxNodes: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_COLOR_CONVERT;
                            },
                            name: "vxNode3",
                            id: "vxNode3",
                            column: 1,
                            row: 0,
                            inputImageFormat: "VX_DF_IMAGE_RGBX",
                            outputImageFormat: "VX_DF_IMAGE_YUV4",
                            params() {
                                return [
                                    this.vxContexts[1].vxGraphs[0],
                                    this.vxContexts[1].vxGraphs[0].vxData[0],
                                    this.vxContexts[1].vxGraphs[0].vxData[1]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[0].vxData[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_CHANNEL_EXTRACT;
                            },
                            name: "vxNode4",
                            id: "vxNode4",
                            column: 2,
                            row: 0,
                            inputType: "VX_DF_IMAGE_YUV4",
                            channel: "VX_CHANNEL_Y",
                            outputType: "VX_DF_IMAGE_U8",
                            params() {
                                return [
                                    this.vxContexts[1].vxGraphs[0],
                                    this.vxContexts[1].vxGraphs[0].vxData[1],
                                    this.vxContexts[1].vxGraphs[0].vxData[2]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[0].vxData[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_THRESHOLD;
                            },
                            name: "vxNode5",
                            id: "vxNode5",
                            column: 3,
                            row: 0,
                            imageType: "VX_DF_IMAGE_U8",
                            threshType: "VX_THRESHOLD_TYPE_BINARY",
                            dataType: "VX_TYPE_UINT8",
                            threshValue: 200,
                            threshValueRange: [100,150],
                            params() {
                                return [
                                    this.vxContexts[1].vxGraphs[0],
                                    this.vxContexts[1].vxGraphs[0].vxData[2],
                                    this.vxContexts[1].vxGraphs[0].vxData[3],
                                    this.vxContexts[1].vxGraphs[0].vxData[4]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[0].vxData[4]
                                ];
                            }
                        }
                    ],
                    vxData: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData0",
                            params() {
                                return [
                                    this.vxContexts[1],
                                    this.ioComs[4].width,
                                    this.ioComs[4].height,
                                    this.ioComs[4].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[0].vxNodes[0]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData1",
                            params() {
                                return [
                                    this.vxContexts[1],
                                    this.vxContexts[1].vxGraphs[0].vxData[0],
                                    this.vxContexts[1].vxGraphs[0].vxData[0],
                                    this.vxContexts[1].vxGraphs[0].vxNodes[0].outputImageFormat
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[0].vxNodes[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData2",
                            params() {
                                return [
                                    this.vxContexts[1],
                                    this.vxContexts[1].vxGraphs[0].vxData[1],
                                    this.vxContexts[1].vxGraphs[0].vxData[1],
                                    this.vxContexts[1].vxGraphs[0].vxNodes[1].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[0].vxNodes[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_THRESHOLD_OBJ;
                            },
                            id: "vxData3",
                            params() {
                                return [
                                    this.vxContexts[1],
                                    this.vxAPI.vxTypes.VX_THRESHOLD_OBJ,
                                    this.vxContexts[1].vxGraphs[0].vxNodes[2].threshType,
                                    this.vxContexts[1].vxGraphs[0].vxNodes[2].dataType,
                                    this.vxContexts[1].vxGraphs[0].vxNodes[2].threshValue
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[0].vxNodes[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData4",
                            params() {
                                return [
                                    this.vxContexts[1],
                                    this.vxContexts[1].vxGraphs[0].vxData[2],
                                    this.vxContexts[1].vxGraphs[0].vxData[2],
                                    this.vxContexts[1].vxGraphs[0].vxNodes[2].imageType
                                ];
                            },
                            outputs() {
                                return [
                                    this.ioComs[5]
                                ];
                            }
                        }
                    ]
                },
                {
                    name: "FourthGraph",
                    id: "Graph3",
                    column: 0,
                    row: 1,
                    gridDimX: 4,
                    gridDimY: 4,
                    vxNodes: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_CHANNEL_EXTRACT;
                            },
                            name: "vxNode6",
                            id: "vxNode6",
                            column: 1,
                            row: 0,
                            inputType: "VX_DF_IMAGE_RGBX",
                            channel: "VX_CHANNEL_R",
                            outputType: "VX_DF_IMAGE_U8",
                            params() {
                                return [
                                    this.vxContexts[1].vxGraphs[1],
                                    this.vxContexts[1].vxGraphs[1].vxData[0],
                                    this.vxContexts[1].vxGraphs[1].vxData[1]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[1].vxData[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_CHANNEL_EXTRACT;
                            },
                            name: "vxNode7",
                            id: "vxNode7",
                            column: 1,
                            row: 1,
                            inputType: "VX_DF_IMAGE_RGBX",
                            channel: "VX_CHANNEL_G",
                            outputType: "VX_DF_IMAGE_U8",
                            params() {
                                return [
                                    this.vxContexts[1].vxGraphs[1],
                                    this.vxContexts[1].vxGraphs[1].vxData[0],
                                    this.vxContexts[1].vxGraphs[1].vxData[2]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[1].vxData[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_ARITHMETIC_SUBTRACTION;
                            },
                            name: "vxNode8",
                            id: "vxNode8",
                            column: 2,
                            row: 0,
                            inputType1: "VX_DF_IMAGE_U8",
                            inputType2: "VX_DF_IMAGE_U8",
                            policy: "VX_CONVERT_POLICY_SATURATE",
                            outputType: "VX_DF_IMAGE_U8",
                            params() {
                                return [
                                    this.vxContexts[1].vxGraphs[1],
                                    this.vxContexts[1].vxGraphs[1].vxData[1],
                                    this.vxContexts[1].vxGraphs[1].vxData[2],
                                    this.vxContexts[1].vxGraphs[1].vxData[3]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[1].vxData[3]
                                ];
                            }
                        }
                    ],
                    vxData: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData0",
                            params() {
                                return [
                                    this.vxContexts[1],
                                    this.ioComs[6].width,
                                    this.ioComs[6].height,
                                    this.ioComs[6].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[1].vxNodes[0],
                                    this.vxContexts[1].vxGraphs[1].vxNodes[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData1",
                            params() {
                                return [
                                    this.vxContexts[1],
                                    this.vxContexts[1].vxGraphs[1].vxData[0],
                                    this.vxContexts[1].vxGraphs[1].vxData[0],
                                    this.vxContexts[1].vxGraphs[1].vxNodes[0].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[1].vxNodes[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData2",
                            params() {
                                return [
                                    this.vxContexts[1],
                                    this.vxContexts[1].vxGraphs[1].vxData[0],
                                    this.vxContexts[1].vxGraphs[1].vxData[0],
                                    this.vxContexts[1].vxGraphs[1].vxNodes[1].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[1].vxGraphs[1].vxNodes[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData3",
                            params() {
                                return [
                                    this.vxContexts[1],
                                    this.vxContexts[1].vxGraphs[1].vxData[1],
                                    this.vxContexts[1].vxGraphs[1].vxData[1],
                                    this.vxContexts[1].vxGraphs[1].vxNodes[2].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.ioComs[7]
                                ];
                            }
                        }
                    ]
                }
            ]
        }
    ]
});
