/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.VisionSDK.Studio.Services.DAO.Resources.Data({
    $interface: "ISettingLocalization",
    projectName: "Target project",
    save: "Save",
    cancel: "Cancel",
    about: "About",
    connection: "Remote toolchain",
    toolchain: "Local toolchain",
    project: "Project",
    back: "Back",
    header: "Settings Panel",
    menuHeader: "Menu",
    descriptionHeader: "Description",
    projectSettingsPanel: {
        header: "Project settings",
        name: "Project name",
        description: "Description",
        path: "Project path",
        vxVersion: "OpenVX version",
        vxVersionOptions: [
            "1.2.0",
            "1.1.0",
            "1.0.1"
        ],
        panelDescription: "Project settings holds general information about currently loaded project, allowing its modification."
    },
    toolchainSettingsPanel: {
        header: "Local toolchain settings",
        name: "Name",
        cmakePath: "CMAKE path",
        cppPath: "C++ compiler path",
        linker: "Linker info",
        validateButton: "Validate",
        panelDescription: "Local toolchain settings enable setup of local build toolchain paths. Settings are used globally - for every project."
    },
    connectionSettingsPanel: {
        header: "Remote toolchain settings",
        name: "Name",
        address: "Address",
        username: "Username",
        password: "Password",
        platform: "Platform",
        agentName: "Target name",
        panelDescription: "Remote toolchain settings enable setup of build connections. Settings are used globally - for every project."
    },
    aboutPanel: {
        header: "About",
        versionLabel: "Version: ",
        versionValue: "<? @var project.version ?>",
        aboutText: "Vision SDK Studio is a drag-and-drop tool focused on prototyping applications for GPU with usage of OpenVX API.<br>" +
            "This tool provides a graphical interface that is useful also for less experienced users. <br>" +
            "Prototyping is simplified by code generator, precompiled libraries and dedicated toolchain for compilation of C++ code.<br><br>" +
            "Licensed under BSD-3-Clause License<br>" +
            "Copyright (c) 2017-2019 NXP",
        panelDescription: "About contains description and current version of IDE."
    },
    values: {
        selectedPlatform: "Linux Cloud",
        toolchainSettings: {
            options: [
                {
                    name: "Local GCC",
                    platform: "WinGCC",
                    cmake: "%localappdata%/VisionSDK/ExternalModules/cmake/bin/cmake.exe",
                    compiler: "%localappdata%/VisionSDK/ExternalModules/msys2/mingw64/bin/g++.exe",
                    linker: ""
                },
                {
                    enabled: false,
                    name: "Local MSVC",
                    platform: "MSVC",
                    cmake: "%localappdata%/VisionSDK/ExternalModules/cmake/bin/cmake.exe",
                    compiler: "C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/MSBuild/15.0/Bin/MSBuild.exe",
                    linker: ""
                }
            ]
        },
        connectionSettings: {
            options: [
                {
                    name: "Linux Cloud",
                    platform: "Linux",
                    address: "<? @var project.target.agentsHub ?>",
                    agentName: "gcp-agent",
                    cmake: "/var/lib/com-wui-framework-builder/appdata/external_modules/cmake/bin"
                },
                {
                    name: "Windows Cloud",
                    platform: "WinGCC",
                    address: "<? @var project.target.agentsHub ?>",
                    agentName: "win-agent",
                    cmake   : "c:/wui/com-wui-framework-builder/appdata/cmake/bin",
                    compiler: "c:/wui/com-wui-framework-builder/appdata/msys2/mingw64/bin"
                },
                {
                    name: "i.MX Cloud",
                    platform: "iMX",
                    address: "<? @var project.target.agentsHub ?>",
                    agentName: "imx-agent"
                }
            ]
        }
    }
});