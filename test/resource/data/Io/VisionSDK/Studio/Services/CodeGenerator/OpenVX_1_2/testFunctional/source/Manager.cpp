/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "Manager.hpp"
#include "VisualGraph.hpp"

typedef int (*OnStartCallback)(char *);

typedef int (*OnStopCallback)(char *, int);

typedef int (*OnErrorCallback)(char *, char *);

namespace TestCase_1_1 {
    Manager::Manager() {
        this->setVisualGraph(new VisualGraph());
    }
}

extern "C" {
// interface should be published as extern "C" function to produce undecorated link symbols
bool disposed = false;

TestCase_1_1::Manager *manager = nullptr;

API_EXPORT Io::VisionSDK::Studio::Libs::Interfaces::IManager *API_CALL getInstance() {
    if (manager == nullptr) {
        manager = new TestCase_1_1::Manager();
    }
    return manager;
}

API_EXPORT int API_CALL Create() {
    return getInstance()->Create();
}

API_EXPORT int API_CALL Validate() {
    return getInstance()->Validate();
}

API_EXPORT int API_CALL Process(const Io::VisionSDK::Studio::Libs::Utils::Manager::ProcessOptions &$options) {
    return getInstance()->Process($options);
}

API_EXPORT void API_CALL Release() {
    disposed = true;
    getInstance()->Release();
}

API_EXPORT void API_CALL StopGraph() {
    getInstance()->Stop();
}

API_EXPORT bool API_CALL IsGraphRunning() {
    return getInstance()->IsRunning();
}

API_EXPORT void API_CALL Translate(int $code, char *$buffer, int *$size) {
    return getInstance()->Translate($code, $buffer, $size);
}

API_EXPORT void API_CALL ReadData(const char *$path, char *$data, int *$size) {
    return getInstance()->ReadData($path, $data, $size);
}

API_EXPORT int API_CALL WriteData(const char *$path, const char *$data) {
    return getInstance()->WriteData($path, $data);
}

API_EXPORT void API_CALL ReadImage(const char *$path, char *$buffer, int *$size) {
    return getInstance()->ReadImage($path, $buffer, $size);
}

API_EXPORT int API_CALL WriteImage(const char *$path, char *$buffer, int $size) {
    return getInstance()->WriteImage($path, $buffer, $size);
}

API_EXPORT void API_CALL ReadStats(char *$data, int *$size) {
    return getInstance()->ReadStats($data, $size);
}

API_EXPORT void API_CALL RegisterOnStart(OnStartCallback $callback) {
    getInstance()->RegisterOnStart([&](const std::string &$entity) {
        $callback(const_cast<char *>($entity.c_str()));
    });
    while (!disposed) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

API_EXPORT void API_CALL RegisterOnStop(OnStopCallback $callback) {
    getInstance()->RegisterOnStop([&](const std::string &$entity, int $elapsed) {
        $callback(const_cast<char *>($entity.c_str()), $elapsed);
    });
    while (!disposed) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

API_EXPORT void API_CALL RegisterOnError(OnErrorCallback $callback) {
    getInstance()->RegisterOnError([&](const std::string &$entity, const std::string &$error) {
        $callback(const_cast<char *>($entity.c_str()), const_cast<char *>($error.c_str()));
    });
    while (!disposed) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}
}
