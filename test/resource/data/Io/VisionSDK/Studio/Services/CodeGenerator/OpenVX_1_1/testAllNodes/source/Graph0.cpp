/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "Graph0.hpp"
#include "VisualGraph.hpp"

namespace NewVisualProject {
    using Io::VisionSDK::Studio::Libs::Primitives::BaseContext;
    using Io::VisionSDK::Studio::Libs::Utils::StopWatch;

    Graph0::Graph0(BaseContext *parent)
            : BaseGraph(parent) {}

    vx_status Graph0::create() {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData0"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData0"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData1"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData1"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData2"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData0"]), BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData2"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData3"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData3"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData4"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData3"]), BaseGraph::getImageHeight(this->vxDataMap["vxData3"]), VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData4"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData5"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData5"]);
        }
        if (status == VX_SUCCESS) {
            vx_uint32 vxData6Value = 0;
            this->vxDataMap["vxData6"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData6Value);
            status = this->getParent()->Check(this->vxDataMap["vxData6"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData7"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData5"]), BaseGraph::getImageHeight(this->vxDataMap["vxData5"]), VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData7"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData8"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData8"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData9Value = 0.0f;
            this->vxDataMap["vxData9"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData9Value);
            status = this->getParent()->Check(this->vxDataMap["vxData9"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData10"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData8"]), BaseGraph::getImageHeight(this->vxDataMap["vxData8"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData10"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData11"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData11"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData12"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData12"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData13"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData11"]), BaseGraph::getImageHeight(this->vxDataMap["vxData11"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData13"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData14"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData14"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData15"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData15"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData16"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData14"]), BaseGraph::getImageHeight(this->vxDataMap["vxData14"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData16"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData17"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData17"]);
        }
        if (status == VX_SUCCESS) {
            vx_int32 vxData18Value = 0;
            this->vxDataMap["vxData18"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_INT32, &vxData18Value);
            status = this->getParent()->Check(this->vxDataMap["vxData18"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData19"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData17"]), BaseGraph::getImageHeight(this->vxDataMap["vxData17"]), VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData19"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData20"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData20"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData21"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData21"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData22"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData20"]), BaseGraph::getImageHeight(this->vxDataMap["vxData20"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData22"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData23"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData23"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData24"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData24"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData25"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData23"]), BaseGraph::getImageHeight(this->vxDataMap["vxData23"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData25"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData26"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData26"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData27"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData27"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData28"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData26"]), BaseGraph::getImageHeight(this->vxDataMap["vxData26"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData28"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData29"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData29"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData30"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData29"]), BaseGraph::getImageHeight(this->vxDataMap["vxData29"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData30"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData31"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData31"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData32"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData31"]), BaseGraph::getImageHeight(this->vxDataMap["vxData31"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData32"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData33"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData33"]);
        }
        if (status == VX_SUCCESS) {
            vx_int32 vxData34Value = 80;
            auto vxData34Ref = vxCreateThreshold(this->getParent()->getVxContext(), VX_THRESHOLD_TYPE_RANGE, VX_TYPE_UINT8);
            this->vxDataMap["vxData34"] = (vx_reference)vxData34Ref;
            vxSetThresholdAttribute(vxData34Ref, VX_THRESHOLD_THRESHOLD_LOWER, &vxData34Value, sizeof(vxData34Value));
            vxData34Value = 160;
            vxSetThresholdAttribute(vxData34Ref, VX_THRESHOLD_THRESHOLD_UPPER, &vxData34Value, sizeof(vxData34Value));
            status = this->getParent()->Check(this->vxDataMap["vxData34"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData35"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData33"]), BaseGraph::getImageHeight(this->vxDataMap["vxData33"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData35"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData36"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData36"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData37"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData37"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData38"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData38"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData39"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData39"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData40"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData36"]), BaseGraph::getImageHeight(this->vxDataMap["vxData36"]), VX_DF_IMAGE_NV12);
            status = this->getParent()->Check(this->vxDataMap["vxData40"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData41"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_RGB);
            status = this->getParent()->Check(this->vxDataMap["vxData41"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData42"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData41"]), BaseGraph::getImageHeight(this->vxDataMap["vxData41"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData42"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData43"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_RGB);
            status = this->getParent()->Check(this->vxDataMap["vxData43"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData44"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData43"]), BaseGraph::getImageHeight(this->vxDataMap["vxData43"]), VX_DF_IMAGE_RGB);
            status = this->getParent()->Check(this->vxDataMap["vxData44"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData45"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData45"]);
        }
        if (status == VX_SUCCESS) {
            vx_int16 vxData46Value[3][3] = {
                    {1, 0, -1},
                    {3, 0, -3},
                    {1, 0, -1}
            };
            this->vxDataMap["vxData46"] = (vx_reference)vxCreateConvolution(this->getParent()->getVxContext(), 3, 3);
            vxCopyConvolutionCoefficients((vx_convolution)this->vxDataMap["vxData46"], &vxData46Value[0][0], VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
            status = this->getParent()->Check(this->vxDataMap["vxData46"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData47"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData45"]), BaseGraph::getImageHeight(this->vxDataMap["vxData45"]), VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData47"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData48"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData48"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData49"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData48"]), BaseGraph::getImageHeight(this->vxDataMap["vxData48"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData49"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData50"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData50"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData51"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData50"]), BaseGraph::getImageHeight(this->vxDataMap["vxData50"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData51"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData52"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData52"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData53"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData52"]), BaseGraph::getImageHeight(this->vxDataMap["vxData52"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData53"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData54"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData54"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData55Value = 10000.000f;
            this->vxDataMap["vxData55"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData55Value);
            status = this->getParent()->Check(this->vxDataMap["vxData55"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData56"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_KEYPOINT, 1000);
            status = this->getParent()->Check(this->vxDataMap["vxData56"]);
        }
        if (status == VX_SUCCESS) {
            vx_size vxData57Value = 0;
            this->vxDataMap["vxData57"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_SIZE, &vxData57Value);
            status = this->getParent()->Check(this->vxDataMap["vxData57"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData58"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData58"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData59"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData58"]), BaseGraph::getImageHeight(this->vxDataMap["vxData58"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData59"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData60"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData60"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData61"] = (vx_reference)vxCreatePyramid(this->getParent()->getVxContext(), 4, VX_SCALE_PYRAMID_HALF, 310, 310, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData61"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData62"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData62"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData63Value = 10000.000f;
            this->vxDataMap["vxData63"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData63Value);
            status = this->getParent()->Check(this->vxDataMap["vxData63"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData64Value = 2.000f;
            this->vxDataMap["vxData64"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData64Value);
            status = this->getParent()->Check(this->vxDataMap["vxData64"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData65Value = 0.150f;
            this->vxDataMap["vxData65"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData65Value);
            status = this->getParent()->Check(this->vxDataMap["vxData65"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData66"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_KEYPOINT, 1000);
            status = this->getParent()->Check(this->vxDataMap["vxData66"]);
        }
        if (status == VX_SUCCESS) {
            vx_size vxData67Value = 0;
            this->vxDataMap["vxData67"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_SIZE, &vxData67Value);
            status = this->getParent()->Check(this->vxDataMap["vxData67"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData68"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData68"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData69"] = (vx_reference)vxCreateDistribution(this->getParent()->getVxContext(), 16, 0, 256);
            status = this->getParent()->Check(this->vxDataMap["vxData69"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData70"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData70"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData71"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData70"]), BaseGraph::getImageHeight(this->vxDataMap["vxData70"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData71"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData72"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData72"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData73"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData72"]), BaseGraph::getImageHeight(this->vxDataMap["vxData72"]), VX_DF_IMAGE_U32);
            status = this->getParent()->Check(this->vxDataMap["vxData73"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData74"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData74"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData75"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData75"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData76"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData74"]), BaseGraph::getImageHeight(this->vxDataMap["vxData74"]), VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData76"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData77"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData77"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData78Value = 0.0f;
            this->vxDataMap["vxData78"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData78Value);
            status = this->getParent()->Check(this->vxDataMap["vxData78"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData79Value = 0.0f;
            this->vxDataMap["vxData79"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData79Value);
            status = this->getParent()->Check(this->vxDataMap["vxData79"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData80"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData80"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData81"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData80"]), BaseGraph::getImageHeight(this->vxDataMap["vxData80"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData81"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData82"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData82"]);
        }
        if (status == VX_SUCCESS) {
            vx_uint8 vxData83Value = 0;
            this->vxDataMap["vxData83"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT8, &vxData83Value);
            status = this->getParent()->Check(this->vxDataMap["vxData83"]);
        }
        if (status == VX_SUCCESS) {
            vx_uint8 vxData84Value = 0;
            this->vxDataMap["vxData84"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT8, &vxData84Value);
            status = this->getParent()->Check(this->vxDataMap["vxData84"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData85"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_COORDINATES2D, 1000);
            status = this->getParent()->Check(this->vxDataMap["vxData85"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData86"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_COORDINATES2D, 1000);
            status = this->getParent()->Check(this->vxDataMap["vxData86"]);
        }
        if (status == VX_SUCCESS) {
            vx_uint32 vxData87Value = 0;
            this->vxDataMap["vxData87"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData87Value);
            status = this->getParent()->Check(this->vxDataMap["vxData87"]);
        }
        if (status == VX_SUCCESS) {
            vx_uint32 vxData88Value = 0;
            this->vxDataMap["vxData88"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData88Value);
            status = this->getParent()->Check(this->vxDataMap["vxData88"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData89"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData89"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData90"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData90"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData91"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData89"]), BaseGraph::getImageHeight(this->vxDataMap["vxData89"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData91"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData92"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData92"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData93"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData93"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData94Value = 0.0f;
            this->vxDataMap["vxData94"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData94Value);
            status = this->getParent()->Check(this->vxDataMap["vxData94"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData95"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData92"]), BaseGraph::getImageHeight(this->vxDataMap["vxData92"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData95"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData96"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData96"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData97"] = (vx_reference)vxCreateRemap(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData96"]), BaseGraph::getImageHeight(this->vxDataMap["vxData96"]), 310, 310);
            status = this->getParent()->Check(this->vxDataMap["vxData97"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData98"] = this->createImage(this->getParent()->getVxContext(), 310, 310, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData98"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData99"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData99"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData100"] = this->createImage(this->getParent()->getVxContext(), 310, 310, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData100"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData101"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData101"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData102"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData101"]), BaseGraph::getImageHeight(this->vxDataMap["vxData101"]), VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData102"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData103"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData101"]), BaseGraph::getImageHeight(this->vxDataMap["vxData101"]), VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData103"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData104"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData104"]);
        }
        if (status == VX_SUCCESS) {
            vx_uint8 *vxData105Value = nullptr;
            std::vector<vx_uint8> vxData105Data = {
                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                    21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
                    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                    81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
                    101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
                    121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140,
                    141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160,
                    161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180,
                    181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200,
                    201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220,
                    221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240,
                    241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255
            };
            this->vxDataMap["vxData105"] = (vx_reference)vxCreateLUT(this->getParent()->getVxContext(), VX_TYPE_UINT8, 256);
            vxCopyLUT((vx_lut)this->vxDataMap["vxData105"], (void **)&vxData105Value, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
            vxTableLookupNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"], (vx_lut)this->vxDataMap["vxData105"], (vx_image)this->vxDataMap["vxData2"]);
            status = this->getParent()->Check(this->vxDataMap["vxData105"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData106"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData104"]), BaseGraph::getImageHeight(this->vxDataMap["vxData104"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData106"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData107"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData107"]);
        }
        if (status == VX_SUCCESS) {
            vx_int32 vxData108Value = 140;
            this->vxDataMap["vxData108"] = (vx_reference)vxCreateThreshold(this->getParent()->getVxContext(), VX_THRESHOLD_TYPE_BINARY, VX_TYPE_UINT8);
            vxSetThresholdAttribute((vx_threshold)this->vxDataMap["vxData108"], VX_THRESHOLD_THRESHOLD_VALUE, &vxData108Value, sizeof(vxData108Value));
            status = this->getParent()->Check(this->vxDataMap["vxData108"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData109"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData107"]), BaseGraph::getImageHeight(this->vxDataMap["vxData107"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData109"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData110"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData110"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData111Value[3][2] = {
                    {1.000f, 1.000f},
                    {2.000f, 2.000f},
                    {0.500f, 0.500f}
            };
            this->vxDataMap["vxData111"] = (vx_reference)vxCreateMatrix(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, 2, 3);
            vxCopyMatrix((vx_matrix)this->vxDataMap["vxData111"], (void **)&vxData111Value, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
            this->getParent()->Check(this->vxDataMap["vxData111"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData112"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData110"]), BaseGraph::getImageHeight(this->vxDataMap["vxData110"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData112"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData113"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData113"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData114Value[3][3] = {
                    {1.000f, 1.000f, 1.000f},
                    {2.000f, 2.000f, 2.000f},
                    {0.500f, 0.500f, 0.500f}
            };
            this->vxDataMap["vxData114"] = (vx_reference)vxCreateMatrix(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, 3, 3);
            vxCopyMatrix((vx_matrix)this->vxDataMap["vxData114"], (void **)&vxData114Value, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
            this->getParent()->Check(this->vxDataMap["vxData114"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData115"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData113"]), BaseGraph::getImageHeight(this->vxDataMap["vxData113"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData115"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData116"] = this->createImage(this->getParent()->getVxContext(), 310, 310, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData116"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData117"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData117"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData118"] = this->createImage(this->getParent()->getVxContext(), 640, 480, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData118"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData119Value = 10.000f;
            this->vxDataMap["vxData119"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData119Value);
            status = this->getParent()->Check(this->vxDataMap["vxData119"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData120"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData120"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode0"] = (vx_reference)vxAbsDiffNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"], (vx_image)this->vxDataMap["vxData1"], (vx_image)this->vxDataMap["vxData2"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode0"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode1"] = (vx_reference)vxAccumulateImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData3"], (vx_image)this->vxDataMap["vxData4"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode1"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode2"] = (vx_reference)vxAccumulateSquareImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData5"], (vx_scalar)this->vxDataMap["vxData6"], (vx_image)this->vxDataMap["vxData7"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode2"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode3"] = (vx_reference)vxAccumulateWeightedImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData8"], (vx_scalar)this->vxDataMap["vxData9"], (vx_image)this->vxDataMap["vxData10"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode3"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode4"] = (vx_reference)vxAddNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData11"], (vx_image)this->vxDataMap["vxData12"], VX_CONVERT_POLICY_SATURATE, (vx_image)this->vxDataMap["vxData13"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode4"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode5"] = (vx_reference)vxSubtractNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData14"], (vx_image)this->vxDataMap["vxData15"], VX_CONVERT_POLICY_SATURATE, (vx_image)this->vxDataMap["vxData16"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode5"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode6"] = (vx_reference)vxConvertDepthNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData17"], (vx_image)this->vxDataMap["vxData19"], VX_CONVERT_POLICY_SATURATE, (vx_scalar)this->vxDataMap["vxData18"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode6"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode7"] = (vx_reference)vxAndNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData20"], (vx_image)this->vxDataMap["vxData21"], (vx_image)this->vxDataMap["vxData22"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode7"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode8"] = (vx_reference)vxXorNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData23"], (vx_image)this->vxDataMap["vxData24"], (vx_image)this->vxDataMap["vxData25"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode8"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode9"] = (vx_reference)vxOrNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData26"], (vx_image)this->vxDataMap["vxData27"], (vx_image)this->vxDataMap["vxData28"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode9"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode10"] = (vx_reference)vxNotNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData29"], (vx_image)this->vxDataMap["vxData30"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode10"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode11"] = (vx_reference)vxBox3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData31"], (vx_image)this->vxDataMap["vxData32"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode11"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode12"] = (vx_reference)vxCannyEdgeDetectorNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData33"], (vx_threshold)this->vxDataMap["vxData34"], (vx_int32)3, VX_NORM_L1, (vx_image)this->vxDataMap["vxData35"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode12"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode13"] = (vx_reference)vxChannelCombineNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData36"], (vx_image)this->vxDataMap["vxData37"], nullptr, nullptr, (vx_image)this->vxDataMap["vxData40"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode13"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode14"] = (vx_reference)vxChannelExtractNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData41"], VX_CHANNEL_0, (vx_image)this->vxDataMap["vxData42"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode14"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode15"] = (vx_reference)vxColorConvertNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData43"], (vx_image)this->vxDataMap["vxData44"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode15"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode16"] = (vx_reference)vxConvolveNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData45"], (vx_convolution)this->vxDataMap["vxData46"], (vx_image)this->vxDataMap["vxData47"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode16"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode17"] = (vx_reference)vxDilate3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData48"], (vx_image)this->vxDataMap["vxData49"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode17"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode18"] = (vx_reference)vxEqualizeHistNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData50"], (vx_image)this->vxDataMap["vxData51"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode18"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode19"] = (vx_reference)vxErode3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData52"], (vx_image)this->vxDataMap["vxData53"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode19"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode20"] = (vx_reference)vxFastCornersNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData54"], (vx_scalar)this->vxDataMap["vxData55"],
                                            (vx_bool)false, (vx_array)this->vxDataMap["vxData56"],
                                            (vx_scalar)this->vxDataMap["vxData57"]);            status = this->getParent()->Check(this->vxNodesMap["vxNode20"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode21"] = (vx_reference)vxGaussian3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData58"], (vx_image)this->vxDataMap["vxData59"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode21"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode22"] = (vx_reference)vxGaussianPyramidNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData60"], (vx_pyramid)this->vxDataMap["vxData61"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode22"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode23"] = (vx_reference)vxHarrisCornersNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData62"], (vx_scalar)this->vxDataMap["vxData63"],
                                            (vx_scalar)this->vxDataMap["vxData64"], (vx_scalar)this->vxDataMap["vxData65"], 5, 3,
                                            (vx_array)this->vxDataMap["vxData66"], (vx_scalar)this->vxDataMap["vxData67"]);            status = this->getParent()->Check(this->vxNodesMap["vxNode23"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode24"] = (vx_reference)vxHistogramNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData68"], (vx_distribution)this->vxDataMap["vxData69"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode24"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode25"] = (vx_reference)vxHalfScaleGaussianNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData70"], (vx_image)this->vxDataMap["vxData71"], (vx_int32)3);
            status = this->getParent()->Check(this->vxNodesMap["vxNode25"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode26"] = (vx_reference)vxIntegralImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData72"], (vx_image)this->vxDataMap["vxData73"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode26"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode27"] = (vx_reference)vxMagnitudeNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData74"], (vx_image)this->vxDataMap["vxData75"], (vx_image)this->vxDataMap["vxData76"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode27"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode28"] = (vx_reference)vxMeanStdDevNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData77"], (vx_scalar)this->vxDataMap["vxData78"], (vx_scalar)this->vxDataMap["vxData79"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode28"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode29"] = (vx_reference)vxMedian3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData80"], (vx_image)this->vxDataMap["vxData81"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode29"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode30"] = (vx_reference)vxMinMaxLocNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData82"], (vx_scalar)this->vxDataMap["vxData83"], (vx_scalar)this->vxDataMap["vxData84"], (vx_array)this->vxDataMap["vxData85"], (vx_array)this->vxDataMap["vxData86"], (vx_scalar)this->vxDataMap["vxData87"], (vx_scalar)this->vxDataMap["vxData88"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode30"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode32"] = (vx_reference)vxPhaseNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData89"], (vx_image)this->vxDataMap["vxData90"], (vx_image)this->vxDataMap["vxData91"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode32"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode33"] = (vx_reference)vxMultiplyNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData92"], (vx_image)this->vxDataMap["vxData93"], (vx_scalar)this->vxDataMap["vxData94"], VX_CONVERT_POLICY_WRAP, VX_ROUND_POLICY_TO_ZERO, (vx_image)this->vxDataMap["vxData95"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode33"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode34"] = (vx_reference)vxRemapNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData96"], (vx_remap)this->vxDataMap["vxData97"], VX_INTERPOLATION_BILINEAR, (vx_image)this->vxDataMap["vxData98"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode34"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode35"] = (vx_reference)vxScaleImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData99"], (vx_image)this->vxDataMap["vxData100"], VX_INTERPOLATION_NEAREST_NEIGHBOR);
            status = this->getParent()->Check(this->vxNodesMap["vxNode35"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode36"] = (vx_reference)vxSobel3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData101"], (vx_image)this->vxDataMap["vxData102"], (vx_image)this->vxDataMap["vxData103"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode36"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode37"] = (vx_reference)vxTableLookupNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData104"], (vx_lut)this->vxDataMap["vxData105"], (vx_image)this->vxDataMap["vxData106"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode37"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode38"] = (vx_reference)vxThresholdNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData107"], (vx_threshold)this->vxDataMap["vxData108"], (vx_image)this->vxDataMap["vxData109"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode38"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode39"] = (vx_reference)vxWarpAffineNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData110"], (vx_matrix)this->vxDataMap["vxData111"], VX_INTERPOLATION_BILINEAR, (vx_image)this->vxDataMap["vxData112"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode39"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode40"] = (vx_reference)vxWarpPerspectiveNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData113"], (vx_matrix)this->vxDataMap["vxData114"], VX_INTERPOLATION_BILINEAR, (vx_image)this->vxDataMap["vxData115"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode40"]);
        }
        return status;
    }

    vx_status Graph0::process(const std::function<vx_status(int)> &$handler) {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        if (status == VX_SUCCESS) {
            status = BaseGraph::process([&](int $iteration) -> vx_status {
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom0 = this->getParent()->getParent()->getIoCom("ioCom0");
                    if ($iteration == 0) {
                        vx_image vxData116Ref = (vx_image)this->getData("vxData116");
                        status = this->getParent()->Check(ioCom0->getFrame(vxData116Ref, true));
                    }
                }
                if (status == VX_SUCCESS) {
                    auto ioCom2 = this->getParent()->getParent()->getIoCom("ioCom2");
                    vx_image vxData118Ref = (vx_image)this->getData("vxData118");
                    ioCom2->getFrame(vxData118Ref, true);
                }
                if (status == VX_SUCCESS) {
                    status = this->getParent()->Check(vxProcessGraph(this->getVxGraph()));
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom1 = this->getParent()->getParent()->getIoCom("ioCom1");
                    vx_image vxData117Ref = (vx_image)this->getData("vxData117");
                    status = this->getParent()->Check(ioCom1->setFrame(vxData117Ref));
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsHeadless()) {
                    auto ioCom3 = this->getParent()->getParent()->getIoCom("ioCom3");
                    vx_image vxData120Ref = (vx_image)this->getData("vxData120");
                    ioCom3->setFrame(vxData120Ref);
                }
                return status;
            });
        }
        return status;
    }

    bool Graph0::loopCondition(int $loopCnt) const {
        return BaseGraph::loopCondition($loopCnt);
    }
}
