/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef NEWVISUALPROJECT_MANAGER_HPP_
#define NEWVISUALPROJECT_MANAGER_HPP_

#include <io-vision-sdk-studio-libs.hpp>
#include "VisualGraph.hpp"

namespace NewVisualProject {
    /**
     * This class implements runtime manager.
     */
    class Manager : public Io::VisionSDK::Studio::Libs::Utils::Manager {
     public:
        Manager();
    };
}

#endif  // NEWVISUALPROJECT_MANAGER_HPP_
