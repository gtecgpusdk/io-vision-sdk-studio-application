/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef NEWVISUALPROJECT_GRAPH_HPP_
#define NEWVISUALPROJECT_GRAPH_HPP_

#include <VX/vx.h>

#include "Reference.hpp"
#include "BaseGraph.hpp"

namespace NewVisualProject {
    /**
     * This class overrides BaseGraph methods with generated code.
     */
    class Graph : public BaseGraph {
     public:
        /**
         * Constructs Graph object from parent Context.
         * @param context Specify parent Context.
         */
        explicit Graph(BaseContext *context);

        vx_status create() override;

        vx_status process() const override;
    };
}

#endif  // NEWVISUALPROJECT_GRAPH_HPP_
