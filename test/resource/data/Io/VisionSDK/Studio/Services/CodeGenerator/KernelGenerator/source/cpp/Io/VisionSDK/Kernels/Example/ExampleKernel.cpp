/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::VisionSDK::Kernels::Example::ExampleKernel;

static vx_param_description_t exampleKernelParams[] = {
        {vx_direction_e::VX_INPUT, vx_type_e::VX_TYPE_IMAGE, vx_parameter_state_e::VX_PARAMETER_STATE_REQUIRED},
        {vx_direction_e::VX_INPUT, vx_type_e::VX_TYPE_SCALAR, vx_parameter_state_e::VX_PARAMETER_STATE_REQUIRED},
        {vx_direction_e::VX_OUTPUT, vx_type_e::VX_TYPE_IMAGE, vx_parameter_state_e::VX_PARAMETER_STATE_REQUIRED}
};

static vx_status VX_CALLBACK exampleKernel(vx_node $node, const vx_reference $parameters[], vx_uint32 $parametersCount) {
    vx_status status = vx_status_e::VX_ERROR_INVALID_PARAMETERS;

    if ($node != nullptr && $parameters != nullptr && $parametersCount == dimof(exampleKernelParams)) {
        auto input = (vx_image)$parameters[0];
        auto shift = (vx_scalar)$parameters[1];
        auto output = (vx_image)$parameters[2];

        /// TODO: Write your implementation here.
    }

    return status;
}

static vx_status VX_CALLBACK exampleValidate(vx_node $node, const vx_reference $parameters[], vx_uint32 $parametersCount,
                                             vx_meta_format $meta[]) {
    vx_status status = vx_status_e::VX_ERROR_INVALID_PARAMETERS;

    if ($node != nullptr && $parametersCount == dimof(exampleKernelParams) && $meta != nullptr) {
        /// TODO: Write validator for your implementation here.
    }

    return status;
}

static vx_status VX_CALLBACK exampleInitialize(vx_node $node, const vx_reference $parameters[], vx_uint32 $parametersCount) {
    vx_status status = vx_status_e::VX_SUCCESS;

    /// TODO: Write initialization routine here.

    return status;
}

static vx_status VX_CALLBACK exampleDeinitialize(vx_node $node, const vx_reference $parameters[], vx_uint32 $parametersCount) {
    vx_status status = vx_status_e::VX_SUCCESS;

    /// TODO: Write cleanup routine here.

    return status;
}

vx_kernel_description_t ExampleKernel = {
        Io::VisionSDK::Kernels::Enums::KernelType::VX_KERNEL_EXAMPLE,
        "io.visionsdk.kernels.example.example",
        exampleKernel,
        exampleKernelParams, dimof(exampleKernelParams),
        exampleValidate,
        nullptr,
        nullptr,
        exampleInitialize,
        exampleDeinitialize
};
