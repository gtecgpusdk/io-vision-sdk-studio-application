/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
"use strict";
Io.VisionSDK.Studio.Services.DAO.Resources.Data({
    $interface: "IVisualGraph",
    imports: {
        vxAPI: "../../resource/data/Io/VisionSDK/Studio/Services/Configuration/OpenVX_1_2_0/CodeGeneratorTemplates.jsonp"
    },
    id: "55a6e53187f22a58a9e9069cbe918366350e3c33",
    name: "TestCase_1_1",
    description: "",
    gridDimX: 4,
    gridDimY: 4,
    ioComs: [
        {
            type() {
                return this.vxAPI.vxTypes.DATA_INPUT;
            },
            name: "input",
            id: "ioCom0",
            column: 1,
            row: 3,
            path: "../data/input/xorNNInput.txt",
            numberOfDims: 1,
            dims: [2],
            dataType: "VX_TYPE_INT16",
            fixedPointPosition: 8,
            params() {
                return [
                    this.ioComs[0].path,
                    this.vxContexts[0].vxGraphs[0].vxData[0]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[0]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.DATA_INPUT;
            },
            name: "weights0",
            id: "ioCom1",
            column: 2,
            row: 4,
            path: "../data/input/xorWeight0Data.txt",
            numberOfDims: 2,
            dims: [2,4],
            dataType: "VX_TYPE_INT16",
            fixedPointPosition: 8,
            params() {
                return [
                    this.ioComs[1].path,
                    this.vxContexts[0].vxGraphs[0].vxData[1]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[1]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.DATA_INPUT;
            },
            name: "biases0",
            id: "ioCom2",
            column: 2,
            row: 5,
            path: "../data/input/xorBias0Data.txt",
            numberOfDims: 1,
            dims: [4],
            dataType: "VX_TYPE_INT16",
            fixedPointPosition: 8,
            params() {
                return [
                    this.ioComs[2].path,
                    this.vxContexts[0].vxGraphs[0].vxData[2]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[2]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.DATA_INPUT;
            },
            name: "weights1",
            id: "ioCom3",
            column: 4,
            row: 4,
            path: "../data/input/xorWeight1Data.txt",
            numberOfDims: 2,
            dims: [4,1],
            dataType: "VX_TYPE_INT16",
            fixedPointPosition: 8,
            params() {
                return [
                    this.ioComs[3].path,
                    this.vxContexts[0].vxGraphs[0].vxData[5]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[5]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.DATA_INPUT;
            },
            name: "biases1",
            id: "ioCom4",
            column: 4,
            row: 5,
            path: "../data/input/xorBias1Data.txt",
            numberOfDims: 1,
            dims: [1],
            dataType: "VX_TYPE_INT16",
            fixedPointPosition: 8,
            params() {
                return [
                    this.ioComs[4].path,
                    this.vxContexts[0].vxGraphs[0].vxData[6]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[6]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.DATA_OUTPUT;
            },
            name: "output",
            id: "ioCom5",
            column: 7,
            row: 3,
            path: "data/output/xorNNOutput.txt",
            dataType: "VX_TYPE_INT16",
            numberOfDims: 1,
            dims: [1],
            fixedPointPosition: 8,
            params() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[8],
                    this.ioComs[5].path
                ];
            }
        }
    ],
    vxContexts: [
        {
            name: "Context",
            id: "vxContext0",
            column: 3,
            row: 3,
            gridDimX: 4,
            gridDimY: 4,
            vxGraphs: [
                {
                    name: "Graph",
                    id: "vxGraph0",
                    column: 3,
                    row: 3,
                    gridDimX: 4,
                    gridDimY: 4,
                    vxNodes: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_FULLY_CONNECTED_LAYER;
                            },
                            name: "vxNode0",
                            id: "vxNode1",
                            column: 3,
                            row: 3,
                            overflowPolicy: "VX_CONVERT_POLICY_WRAP",
                            roundingPolicy: "VX_ROUND_POLICY_TO_NEAREST_EVEN",
                            dataTypeInput: "VX_TYPE_INT16",
                            numberOfDimsInput: 1,
                            dimsInput: [2],
                            fixedPointPositionInput: 8,
                            dataTypeWeights: "VX_TYPE_INT16",
                            numberOfDimsWeights: 2,
                            dimsWeights: [2,4],
                            fixedPointPositionWeights: 8,
                            dataTypeBiases: "VX_TYPE_INT16",
                            numberOfDimsBiases: 1,
                            dimsBiases: [4],
                            fixedPointPositionBiases: 8,
                            dataTypeOutput: "VX_TYPE_INT16",
                            numberOfDimsOutput: 1,
                            dimsOutput: [4],
                            fixedPointPositionOutput: 8,
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[1],
                                    this.vxContexts[0].vxGraphs[0].vxData[2],
                                    this.vxContexts[0].vxGraphs[0].vxData[3]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[3]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_ACTIVATION_LAYER;
                            },
                            name: "vxNode1",
                            id: "vxNode2",
                            column: 4,
                            row: 3,
                            function: "VX_NN_ACTIVATION_HYPERBOLIC_TAN",
                            a: 1.0,
                            b: 1.0,
                            dataTypeInput: "VX_TYPE_INT16",
                            numberOfDimsInput: 1,
                            dimsInput: [4],
                            fixedPointPositionInput: 8,
                            dataTypeOutput: "VX_TYPE_INT16",
                            numberOfDimsOutput: 1,
                            dimsOutput: [4],
                            fixedPointPositionOutput: 8,
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[3],
                                    this.vxContexts[0].vxGraphs[0].vxData[4]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[4]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_FULLY_CONNECTED_LAYER;
                            },
                            name: "vxNode2",
                            id: "vxNode3",
                            column: 5,
                            row: 3,
                            overflowPolicy: "VX_CONVERT_POLICY_WRAP",
                            roundingPolicy: "VX_ROUND_POLICY_TO_NEAREST_EVEN",
                            dataTypeInput: "VX_TYPE_INT16",
                            numberOfDimsInput: 1,
                            dimsInput: [4],
                            fixedPointPositionInput: 8,
                            dataTypeWeights: "VX_TYPE_INT16",
                            numberOfDimsWeights: 2,
                            dimsWeights: [4,1],
                            fixedPointPositionWeights: 8,
                            dataTypeBiases: "VX_TYPE_INT16",
                            numberOfDimsBiases: 1,
                            dimsBiases: [1],
                            fixedPointPositionBiases: 8,
                            dataTypeOutput: "VX_TYPE_INT16",
                            numberOfDimsOutput: 1,
                            dimsOutput: [1],
                            fixedPointPositionOutput: 8,
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[4],
                                    this.vxContexts[0].vxGraphs[0].vxData[5],
                                    this.vxContexts[0].vxGraphs[0].vxData[6],
                                    this.vxContexts[0].vxGraphs[0].vxData[7]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[7]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_ACTIVATION_LAYER;
                            },
                            name: "vxNode3",
                            id: "vxNode4",
                            column: 6,
                            row: 3,
                            function: "VX_NN_ACTIVATION_LOGISTIC",
                            a: 1.0,
                            b: 1.0,
                            dataTypeInput: "VX_TYPE_INT16",
                            numberOfDimsInput: 1,
                            dimsInput: [1],
                            fixedPointPositionInput: 8,
                            dataTypeOutput: "VX_TYPE_INT16",
                            numberOfDimsOutput: 1,
                            dimsOutput: [1],
                            fixedPointPositionOutput: 8,
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[7],
                                    this.vxContexts[0].vxGraphs[0].vxData[8]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[8]
                                ];
                            }
                        }
                    ],
                    vxData: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_TENSOR;
                            },
                            id: "vxData0",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[0].numberOfDims,
                                    this.ioComs[0].dims,
                                    this.ioComs[0].dataType,
                                    this.ioComs[0].fixedPointPosition
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_TENSOR;
                            },
                            id: "vxData1",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[1].numberOfDims,
                                    this.ioComs[1].dims,
                                    this.ioComs[1].dataType,
                                    this.ioComs[1].fixedPointPosition
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_TENSOR;
                            },
                            id: "vxData2",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[2].numberOfDims,
                                    this.ioComs[2].dims,
                                    this.ioComs[2].dataType,
                                    this.ioComs[2].fixedPointPosition
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_TENSOR;
                            },
                            id: "vxData3",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0].numberOfDimsOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0].dimsOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0].dataTypeOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0].fixedPointPositionOutput
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_TENSOR;
                            },
                            id: "vxData4",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1].numberOfDimsOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1].dimsOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1].dataTypeOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1].fixedPointPositionOutput
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_TENSOR;
                            },
                            id: "vxData5",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[3].numberOfDims,
                                    this.ioComs[3].dims,
                                    this.ioComs[3].dataType,
                                    this.ioComs[3].fixedPointPosition
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_TENSOR;
                            },
                            id: "vxData6",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[4].numberOfDims,
                                    this.ioComs[4].dims,
                                    this.ioComs[4].dataType,
                                    this.ioComs[4].fixedPointPosition
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_TENSOR;
                            },
                            id: "vxData7",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2].numberOfDimsOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2].dimsOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2].dataTypeOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2].fixedPointPositionOutput
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_TENSOR;
                            },
                            id: "vxData8",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3].numberOfDimsOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3].dimsOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3].dataTypeOutput,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3].fixedPointPositionOutput
                                ];
                            },
                            outputs() {
                                return [
                                    this.ioComs[5]
                                ];
                            }
                        }
                    ]
                }
            ]
        }
    ]
});
