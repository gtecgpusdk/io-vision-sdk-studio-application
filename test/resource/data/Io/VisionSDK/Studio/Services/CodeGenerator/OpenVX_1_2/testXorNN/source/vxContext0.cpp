/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "vxContext0.hpp"
#include "vxGraph0.hpp"

namespace TestCase_1_1 {
    using Io::VisionSDK::Studio::Libs::Primitives::BaseGraph;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseContext;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph;

    vxContext0::vxContext0(BaseVisualGraph *parent)
            : BaseContext(parent) {
        this->graphsMap["graph0"] = new vxGraph0(this);
    }

    vx_status vxContext0::process(const std::function<vx_status()> &$handler) {
        return BaseContext::process([&]() -> vx_status {
            vx_status status = vxGetStatus((vx_reference)this->getVxContext());
            auto graph0 = this->getGraph("graph0");
            if (status == VX_SUCCESS && graph0 != nullptr) {
                status = const_cast<BaseGraph *>(graph0)->Process();
                if (status != VX_SUCCESS) {
                    const_cast<vxContext0 *>(this)->removeGraph("graph0");
                }
            } else {
                status = VX_FAILURE;
            }
            return status;
        });
    }
}
