/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef NEWVISUALPROJECT_BASECONTEXT_HPP_
#define NEWVISUALPROJECT_BASECONTEXT_HPP_

#include <string>
#include <map>

#include <VX/vx.h>

#include "Reference.hpp"

namespace NewVisualProject {
    /**
     * BaseContext class holds all generalised operations on OpenVX context object domain.
     */
    class BaseContext {
     public:
        /**
         * Construct BaseContext object from parent visual graph.
         * @param parent Specify pointer to parent visual graph.
         */
        explicit BaseContext(VisualGraph *parent);

        /**
         * Destruct BaseContext object.
         */
        ~BaseContext();

        /**
         * @return Returns parent visual graph pointer.
         */
        const VisualGraph *getParent() const;

        /**
         * @return Returns reference to internal vx_context object.
         */
        vx_context getVxContext() const;

        /**
         * Generic getter for graph instances owned by this context.
         * @param name Specify name of graph to get.
         * @return Returns pointer to requested graph or nullptr if not found.
         */
        const BaseGraph *getGraph(const std::string &name) const;

        /**
         * Creates internal graphs. Has to be called before process().
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status create() = 0;

        /**
         * Run process routine.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status process() const = 0;

     protected:
        void removeGraph(const std::string &name);

        std::map<std::string, BaseGraph *> graphsMap = {};

     private:
        VisualGraph *parent;
        vx_context vxContext;
    };
}

#endif  // NEWVISUALPROJECT_BASECONTEXT_HPP_
