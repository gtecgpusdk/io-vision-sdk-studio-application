/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "Graph0.hpp"
#include "VisualGraph.hpp"

namespace TestCase_1_1 {
    using Io::VisionSDK::Studio::Libs::Primitives::BaseContext;
    using Io::VisionSDK::Studio::Libs::Utils::StopWatch;

    Graph0::Graph0(BaseContext *parent)
            : BaseGraph(parent) {}

    vx_status Graph0::create() {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData0"] = this->createImage(this->getParent()->getVxContext(), 640, 480, VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData0"]);
        }
        if (status == VX_SUCCESS) {
            vx_uint32 vxData1Value = 4;
            this->vxDataMap["vxData1"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData1Value);
            status = this->getParent()->Check(this->vxDataMap["vxData1"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData2"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData0"]), BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData2"]);
        }
        if (status == VX_SUCCESS) {
            vx_float32 vxData3Value = 0.500f;
            this->vxDataMap["vxData3"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData3Value);
            status = this->getParent()->Check(this->vxDataMap["vxData3"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData4"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData0"]), BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_U8);
            status = this->getParent()->Check(this->vxDataMap["vxData4"]);
        }
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData5"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData0"]), BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_S16);
            status = this->getParent()->Check(this->vxDataMap["vxData5"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["acc_squ1"] = (vx_reference)vxAccumulateSquareImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"], (vx_scalar)this->vxDataMap["vxData1"], (vx_image)this->vxDataMap["vxData2"]);
            status = this->getParent()->Check(this->vxNodesMap["acc_squ1"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["acc_wei1"] = (vx_reference)vxAccumulateWeightedImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"], (vx_scalar)this->vxDataMap["vxData3"], (vx_image)this->vxDataMap["vxData4"]);
            status = this->getParent()->Check(this->vxNodesMap["acc_wei1"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["acc"] = (vx_reference)vxAccumulateImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"], (vx_image)this->vxDataMap["vxData5"]);
            status = this->getParent()->Check(this->vxNodesMap["acc"]);
        }
        return status;
    }

    vx_status Graph0::process(const std::function<vx_status(int)> &$handler) {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        if (status == VX_SUCCESS) {
            status = BaseGraph::process([&](int $iteration) -> vx_status {
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom0 = this->getParent()->getParent()->getIoCom("ioCom0");
                    if ($iteration == 0) {
                        vx_image vxData0Ref = (vx_image)this->getData("vxData0");
                        status = this->getParent()->Check(ioCom0->getFrame(vxData0Ref, true));
                    }
                }
                if (status == VX_SUCCESS) {
                    status = this->getParent()->Check(vxProcessGraph(this->getVxGraph()));
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom1 = this->getParent()->getParent()->getIoCom("ioCom1");
                    vx_image vxData2Ref = (vx_image)this->getData("vxData2");
                    status = this->getParent()->Check(ioCom1->setFrame(vxData2Ref));
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom2 = this->getParent()->getParent()->getIoCom("ioCom2");
                    vx_image vxData4Ref = (vx_image)this->getData("vxData4");
                    status = this->getParent()->Check(ioCom2->setFrame(vxData4Ref));
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom3 = this->getParent()->getParent()->getIoCom("ioCom3");
                    vx_image vxData5Ref = (vx_image)this->getData("vxData5");
                    status = this->getParent()->Check(ioCom3->setFrame(vxData5Ref));
                }
                return status;
            });
        }
        return status;
    }

    bool Graph0::loopCondition(int $loopCnt) const {
        return BaseGraph::loopCondition($loopCnt);
    }
}
