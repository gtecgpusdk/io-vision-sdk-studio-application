/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <fstream>
#include <regex>

#include "VisualGraph.hpp"

namespace VXUtils = Io::VisionSDK::Studio::Libs::VXUtils;

std::vector<std::string> testCasesplit(std::string::const_iterator it, std::string::const_iterator end, std::regex e = std::regex{"\\w+"}) {
    std::smatch m{};
    std::vector<std::string> ret{};
    while (std::regex_search(it, end, m, e)) {
        ret.emplace_back(m.str());
        std::advance(it, m.position() + m.length());
    }
    return ret;
}

int main(int argc, const char *argv[]) {
    vx_status status;
    bool isHeadless = false;
    bool isLooped = false;
    for (int i = 0; i < argc - 1; ++i) {
        std::string key = argv[1 + i];
        if (key == "--headless") {
            isHeadless = true;
        } else if (key == "--looped") {
            isLooped = true;
        }
    }

    VXUtils::Utils::StopWatch stopWatch{};
    auto *visualGraph = new NewVisualProject::VisualGraph();
    visualGraph->setHeadless(isHeadless);
    int iterations = 0;
    std::string name;
    do {
        std::ifstream ifs("build\\debug\\runtime.config");
        std::string str;
        ifs >> str;
        ifs.close();

        auto items = testCasesplit(str.begin(), str.end(), std::regex("\\w+=[\\w\\d\"_-]+"));
        for (const auto &item : items) {
            auto index = item.find("run=");
            if (index == 0) {
                std::string tmp = item.substr(4);
                if (tmp != "true") {
                    isLooped = false;
                }
            }
            index = item.find("vxDataName=");
            if (index == 0) {
                name = item.substr(11);
                if (name[0] == '"') {
                    name = name.substr(1, name.size() - 2);
                }
            }
        }

        visualGraph->setStreamDataName(name);
        iterations++;
        status = visualGraph->create();
        stopWatch.Start();
        if (status == VX_SUCCESS) {
            status = visualGraph->process();
        }
        stopWatch.Stop();
        std::cout << "Processing elapsed (" << iterations << "): " << stopWatch.getElapsed() << " ms; status: " << status << std::endl;
    } while (isLooped);
    delete visualGraph;

    if (argc < 2 || !isHeadless) {
        if (status == VX_SUCCESS) {
            std::cout << "Run successfully ..." << std::endl;
        } else {
            std::cout << "Run has failed with status: " << status << std::endl;
        }

        std::cout << "Click mouse to any display and press ESC for escape ..." << std::endl;
        VXUtils::Media::Display::WaitForAllClosed();
    }
    return status;
}
