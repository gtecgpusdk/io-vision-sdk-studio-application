# * ******************************************************************************************************* *
# *
# * Copyright (c) 2018-2019 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ******************************************************************************************************* *

cmake_minimum_required(VERSION 3.6)

project(VisionGraph)

set(CMAKE_CONFIGURATION_TYPES "debug;release" CACHE STRING "Available build configurations." FORCE)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

set(CMAKE_CXX_STANDARD 17)

# Custom variables
if (NOT DEFINED VISION_SDK_PATH)
    set(VISION_SDK_PATH $ENV{VISION_SDK_PATH})
endif ()

set(PROJECT_DEPENDENCIES
        )

string(TOLOWER ${TARGET_PLATFORM} TARGET_PLATFORM)
message(STATUS "Target platform: ${TARGET_PLATFORM}")
if (${TARGET_PLATFORM} STREQUAL "imx" OR ${TARGET_PLATFORM} STREQUAL "imx6" OR ${TARGET_PLATFORM} STREQUAL "imx8")
    set(TARGET_PLATFORM arm)
endif ()

if (${TARGET_PLATFORM} STREQUAL "win" OR ${TARGET_PLATFORM} STREQUAL "linux" OR ${TARGET_PLATFORM} STREQUAL "arm")
    set(CMAKE_C_FLAGS_DEBUG "-O0 -g ")
    set(CMAKE_C_FLAGS_RELEASE "-O2 -fdata-sections -ffunction-sections -DNDEBUG ")
    set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g -Wsign-compare -Wparentheses ")
    set(CMAKE_CXX_FLAGS_RELEASE "-O2 -fdata-sections -ffunction-sections -DNDEBUG -Wsign-compare -Wparentheses ")
    set(CMAKE_EXE_LINKER_FLAGS_DEBUG " ")
    set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-Wl,-O1 -Wl,--as-needed -Wl,--gc-sections ")

    if (${TARGET_PLATFORM} STREQUAL "win")
        add_definitions(-DWIN_PLATFORM)
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static ")
        set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static")
        set(CMAKE_SHARED_LIBRARY_PREFIX "")
    elseif (${TARGET_PLATFORM} STREQUAL "linux")
        add_definitions(-DUNIX_PLATFORM)
        set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build)
    elseif (${TARGET_PLATFORM} STREQUAL "arm")
        add_definitions(-DUNIX_PLATFORM)
        set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build)
    endif ()

    add_library(${PROJECT_NAME}
            SHARED
            include/Context0.hpp
            include/Graph0.hpp
            include/VisualGraph.hpp
            include/Reference.hpp
            include/Manager.hpp
            source/Context0.cpp
            source/Graph0.cpp
            source/VisualGraph.cpp
            source/Manager.cpp
            )

    add_executable(Video_processing
            source/main.cpp)

    add_dependencies(Video_processing ${PROJECT_NAME})

    #    OpenCV
    if (${TARGET_PLATFORM} STREQUAL "win" OR ${TARGET_PLATFORM} STREQUAL "linux")
        set(VISION_SDK_OPENCV_LIBRARIES
                "opencv_videostab"
                "opencv_objdetect"
                "opencv_video"
                "opencv_calib3d"
                "opencv_features2d"
                "opencv_flann"
                "opencv_highgui"
                "opencv_photo"
                "opencv_videoio"
                "opencv_imgcodecs"
                "opencv_imgproc"
                "opencv_core"
                "libjasper"
                "libtiff"
                "libjpeg"
                "libpng"
                "libwebp"
                "zlib"
                )

        set(VISION_SDK_OPENCV_LIBRARIES_DIR
                ${VISION_SDK_PATH}/opencv/lib
                )
        set(VISION_SDK_OPENCV_INCLUDE_DIR
                ${VISION_SDK_PATH}/opencv/include
                )
    else ()
        set(VISION_SDK_OPENCV_LIBRARIES
                "opencv_videostab"
                "opencv_video"
                "opencv_calib3d"
                "opencv_features2d"
                "opencv_flann"
                "opencv_highgui"
                "opencv_imgproc"
                "opencv_imgcodecs"
                "opencv_core"
                "png"
                )
        set(VISION_SDK_OPENCV_LIBRARIES_DIR
                /usr/lib
                )
        set(VISION_SDK_OPENCV_INCLUDE_DIR
                /usr/include
                )
    endif ()

    #    OpenVX
    set(VISION_SDK_OPENVX_LIBRARIES
            "openvx"
            )
    set(VISION_SDK_OPENVX_LIBRARIES_DIR
            "${VISION_SDK_PATH}/openvx/lib"
            )
    set(VISION_SDK_OPENVX_INCLUDE_DIR
            "${VISION_SDK_PATH}/openvx/include"
            )

    #    io-vision-sdk-studio-libs
    set(VISION_SDK_LIBS_LIBRARIES
            "io-vision-sdk-studio-libs"
            )
    set(VISION_SDK_LIBS_LIBRARIES_DIR
            "${VISION_SDK_PATH}/io-vision-sdk-studio-libs/lib"
            )
    set(VISION_SDK_LIBS_INCLUDE_DIR
            "${VISION_SDK_PATH}/io-vision-sdk-studio-libs/include"
            )

    #    Other
    if (${TARGET_PLATFORM} STREQUAL "linux" OR ${TARGET_PLATFORM} STREQUAL "arm")
        set(VISION_SDK_OTHER_LIBRARIES
                "stdc++fs"
                "pthread"
                "dl"
                )
    else ()
        set(VISION_SDK_OTHER_LIBRARIES
                "stdc++fs"
                "vfw32"
                "avifil32"
                "avicap32"
                "winmm"
                "msvfw32"
                "comctl32"
                "gdi32"
                "ole32"
                "setupapi"
                )
    endif ()

    set(VISION_SDK_OTHER_INCLUDE_DIR
            "${VISION_SDK_PATH}/nlohmann/src"
            )

    target_include_directories(${PROJECT_NAME}
            PUBLIC
            ${CMAKE_SOURCE_DIR}/include)
    target_include_directories(Video_processing
            PUBLIC
            ${CMAKE_SOURCE_DIR}/include
            ${VISION_SDK_LIBS_INCLUDE_DIR}/Io/VisionSDK/Studio/Libs/Interfaces)

    target_include_directories(${PROJECT_NAME}
            SYSTEM BEFORE PUBLIC
            ${VISION_SDK_OPENVX_INCLUDE_DIR}
            ${VISION_SDK_OPENCV_INCLUDE_DIR}
            ${VISION_SDK_LIBS_INCLUDE_DIR}
            ${VISION_SDK_OTHER_INCLUDE_DIR})

    #   io-vision-sdk-studio-libs
    foreach (item ${VISION_SDK_LIBS_LIBRARIES})
        find_library(VISION_SDK_LIBS_${item}_FOUND ${item} ${VISION_SDK_LIBS_LIBRARIES_DIR} NO_DEFAULT_PATH NO_SYSTEM_ENVIRONMENT_PATH)
        if (VISION_SDK_LIBS_${item}_FOUND)
            target_link_libraries(${PROJECT_NAME} ${VISION_SDK_LIBS_${item}_FOUND})
            message(STATUS "Lib Found: ${VISION_SDK_LIBS_${item}_FOUND}")
        else ()
            message(FATAL_ERROR "io-vision-sdk-studio-libs lib NOT Found: ${item} in ${VISION_SDK_LIBS_LIBRARIES_DIR}")
        endif ()
    endforeach ()

    #   OpenVX
    if (NOT ${TARGET_PLATFORM} STREQUAL "arm")
        foreach (item ${VISION_SDK_OPENVX_LIBRARIES})
            find_library(OPENVX_LIB_${item}_FOUND ${item} ${VISION_SDK_OPENVX_LIBRARIES_DIR} ${VISION_SDK_PATH}/bins NO_DEFAULT_PATH NO_SYSTEM_ENVIRONMENT_PATH)
            if (OPENVX_LIB_${item}_FOUND)
                target_link_libraries(${PROJECT_NAME} ${OPENVX_LIB_${item}_FOUND})
                message(STATUS "Lib Found: ${OPENVX_LIB_${item}_FOUND}")
            else ()
                message(FATAL_ERROR "OpenVX lib NOT Found: ${item} in ${VISION_SDK_OPENVX_LIBRARIES_DIR}")
            endif ()
        endforeach ()
    else ()
        target_link_libraries(${PROJECT_NAME} "${VISION_SDK_PATH}/bins/libopenvx.so")
    endif ()

    #   OpenCV
    if (${TARGET_PLATFORM} STREQUAL "win")
        LIST(APPEND CMAKE_FIND_LIBRARY_SUFFIXES "2413.a" "310.a" "320.a" "330.a" "340.a" "341.a" "342.a")
    endif ()
    foreach (item ${VISION_SDK_OPENCV_LIBRARIES})
        find_library(OPENCV_LIB_${item}_FOUND ${item} ${VISION_SDK_OPENCV_LIBRARIES_DIR} NO_DEFAULT_PATH NO_SYSTEM_ENVIRONMENT_PATH)
        if (OPENCV_LIB_${item}_FOUND)
            target_link_libraries(${PROJECT_NAME} ${OPENCV_LIB_${item}_FOUND})
            message(STATUS "Lib Found: ${item}")
        else ()
            message(FATAL_ERROR "OpenCV lib NOT Found: ${item} in ${VISION_SDK_OPENCV_LIBRARIES_DIR}")
        endif ()
    endforeach ()
    if (${TARGET_PLATFORM} STREQUAL "win")
        LIST(REMOVE_ITEM CMAKE_FIND_LIBRARY_SUFFIXES "2413.a" "310.a" "320.a" "330.a" "340.a" "341.a" "342.a")
    endif ()

    #   Other
    target_link_libraries(${PROJECT_NAME} ${VISION_SDK_OTHER_LIBRARIES})
    target_link_libraries(Video_processing ${VISION_SDK_OTHER_LIBRARIES})

    if (${TARGET_PLATFORM} STREQUAL "linux")
        target_link_libraries(${PROJECT_NAME} ${GTK2_LIBRARIES})
    endif ()

    # add dependency projects
    foreach (dep ${PROJECT_DEPENDENCIES})
        message("CMAKE processing dependency: " ${dep})
        add_subdirectory(${dep})

        target_include_directories(${PROJECT_NAME} AFTER ${DEP_INCLUDES})

        target_link_libraries(${PROJECT_NAME} ${dep}_lib)
    endforeach (dep)
else ()
    message(FATAL_ERROR "Unsupported target platform: ${TARGET_PLATFORM}")
endif ()

# Print project config
message(STATUS "*** **************************** ***")
message(STATUS "*** CMAKE CONFIGURATION SETTINGS ***")
message(STATUS "*** **************************** ***")
message(STATUS "Generator:                      ${CMAKE_GENERATOR}")
message(STATUS "Platform:                       ${CMAKE_SYSTEM_NAME}")
message(STATUS "Architecture:                   ${PROJECT_ARCH}")
message(STATUS "Build type:                     ${CMAKE_BUILD_TYPE}")
message(STATUS "Binary root:                    ${CMAKE_BINARY_DIR}")
message(STATUS "Compile C defines:              ${CMAKE_C_FLAGS}")
message(STATUS "Compile C defines (DEBUG):      ${CMAKE_C_FLAGS_DEBUG}")
message(STATUS "Compile C defines (RELEASE):    ${CMAKE_C_FLAGS_RELEASE}")
message(STATUS "Compile C++ defines:            ${CMAKE_CXX_FLAGS}")
message(STATUS "Compile C++ defines (DEBUG):    ${CMAKE_CXX_FLAGS_DEBUG}")
message(STATUS "Compile C++ defines (RELEASE):  ${CMAKE_CXX_FLAGS_RELEASE}")
message(STATUS "Exe link flags:                 ${CMAKE_EXE_LINKER_FLAGS}")
message(STATUS "Exe link flags (DEBUG):         ${CMAKE_EXE_LINKER_FLAGS_DEBUG}")
message(STATUS "Exe link flags (RELEASE):       ${CMAKE_EXE_LINKER_FLAGS_RELEASE}")
